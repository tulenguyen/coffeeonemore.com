$("#mobile-show-nav").click(function () {
    function setHeight() {
        windowHeight = $(document).innerHeight();
        $('#topNav #mobileNav').css('min-height', windowHeight);
    }
    ;
    setHeight();
    $(window).resize(function () {
        setHeight();
    });
    $("#mobile-close-nav").click(function () {
        $('#topNav #mobileNav').css('min-height', 0);
    });
});
$(document).ready(function () {
    $('.layout-caption-overlay').each(function () {
        if ($(this).has(".intrinsic > a")) {
            $('.image-caption-wrapper', this).each(function () {
                $(this).parent('.intrinsic').find("a").append(this);
            });
        }
        ;
    });
});