<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\News;

class Category extends BaseModel {

    public $timestamps = false;
    protected $table = 'category';

    public function getCategory($options = [], $pageIndex = 1, $pageSize = 10) {

        $key = __FUNCTION__ . '.' . implode('.', $options) . '.' . $pageIndex . '.' . $pageSize;
        if ($result = $this->getCacheData($key)) {
            return $result;
        }

        $query = $this::query();
        //select
        $query = $query->select('*');

        //where
        if (isset($options['header_order'])) {
            $query->where('category_header_order', '>', '0');
            $query->orderBy('category_header_order', 'asc');
        } else if (isset($options['home_order'])) {
            $query->where('category_home_order', '>', '0');
            $query->orderBy('category_home_order', 'asc');
        } else if (isset($options['footer_order'])) {
            $query->where('category_footer_order', '>', '0');
            $query->orderBy('category_footer_order', 'asc');
        } else {
            $query->orderBy('category_id', 'desc');
        }

        isset($options['parent_id']) ? $query->where('parent_id', $options['parent_id']) : NULL;
        isset($options['id']) ? $query->where('category_id', $options['id']) : NULL;
        isset($options['ids']) ? $query->where('category_id', 'IN', $options['ids']) : NULL;
        isset($options['status']) ? $query->where('category_status', $options['status']) : $query->where('category_status', '1');
        //limit
        $query->offset(($pageIndex - 1) * $pageSize)
                ->limit($pageSize);
        //execute
        $result = $query->get();
        if (!empty($result)) {
            foreach ($result as &$item) {
                $item['category_link'] = route('category', [
                    'id' => $item['category_id'],
                    'slug' => isset($item['category_slug']) && !empty($item['category_slug']) ? $item['category_slug'] : str_slug($item['category_name']),
                ]);
            }
        }

        $this->setCacheData($key, $result);
        return $result;
    }

    private function __getChildIds($id, &$childIds = []) {
        if ($id > 0) {
            if ($childs = $this->getCategory(['parent_id' => $id])) {
                foreach ($childs as $child) {
                    $childIds[] = $child['category_id'];
                    $this->__getChildIds($child['category_id'], $childIds);
                }
            }
        }
    }

    public function getChildIds($id) {

        $key = __FUNCTION__ . $id;
        if ($result = $this->getCacheData($key)) {
            return $result;
        }
        $this->__getChildIds($id, $listIds);

        if (!empty($listIds)) {
            array_push($listIds, $id);
        } else {
            $listIds = [$id];
        }
        $this->setCacheData($key, $listIds);

        return $listIds;
    }

    public function getHeaderCategory() {
        $key = __FUNCTION__;
        if ($result = $this->getCacheData($key)) {
            
        }
        $result = $this->getCategory(['header_order' => 'header_order']);
        $this->setCacheData($key, $result);
        return $result;
    }

    public function getHomeCategory() {
        $key = __FUNCTION__;
        if ($result = $this->getCacheData($key)) {
            return $result;
        }
        $result = $this->getCategory(['home_order' => 'home_order']);
        $this->setCacheData($key, $result);

        return $result;
    }

    public function getFooterCategory() {
        $key = __FUNCTION__;
        if ($result = $this->getCacheData($key)) {
            return $result;
        }
        $result = $this->getCategory(['footer_order' => 'footer_order']);
        $this->setCacheData($key, $result);
        return $result;
    }

    public function getCategoryNews($options = array(), $pageIndex = 1, $pageSize = 19) {
        $key = __FUNCTION__ . implode('.', $options) . $pageIndex . $pageSize;
        if ($result = $this->getCacheData($key)) {
            return $result;
        }
        $parentId = 0;
        //options parent: slug
        if (isset($options['parent_slug']) && !empty($options['parent_slug'])) {
            $parent = $this->getCategory(array('category_slug' => $options['parent_slug']), 1);
            if (!empty($parent)) {
                $parentId = $parent['category_id'];
            }
        }
        if ($category = $this->getCategory(array('category_slug' => $options['category_slug'], 'parent_id' => $parentId), 1)) {
            $listNews = $this->getNews(array('category_id' => $category['category_id'],
                'others' => 0,
                'status' => 1), $pageIndex, $pageSize);
            $result = array(
                'category' => $category,
                'parent' => isset($parent) && !empty($parent) ? $parent : null
                ,
                'news' => $listNews
            );
        }

        $this->setCacheData($key, $result);
        return $result;
    }

}
