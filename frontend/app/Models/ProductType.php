<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ProductType extends BaseModel {

    const CREATED_AT = 'create_at';
    const UPDATED_AT = 'update_at';

    protected $table = 'product_type';

    public function getProductType($options = [], $pageIndex = 1, $pageSize = 10) {
        $key = __FUNCTION__ . '.' . implode('.', $options) . '.' . $pageIndex . '.' . $pageSize;
        if ($result = $this->getCacheData($key)) {
            return $result;
        }

        $query = $this::query();
        //select
        $query->select('*');
        if (isset($options['header_order'])) {
            $query->where('category_header_order', '>', '0');
            $query->orderBy('category_header_order', 'asc');
        } else if (isset($options['home_order'])) {
            $query->where('category_home_order', '>', '0');
            $query->orderBy('category_home_order', 'asc');
        } else if (isset($options['footer_order'])) {
            $query->where('category_footer_order', '>', '0');
            $query->orderBy('category_footer_order', 'asc');
        } else {
            $query->orderBy('id', 'desc');
        }

        //where
        isset($options['id']) ? $query->where('id', $options['id']) : NULL;
        isset($options['ids']) ? $query->whereIn('id', $options['ids']) : NULL;
        isset($listIds) && !empty($listIds) ? $query->whereIn('news_id', $listIds) : NULL;
        isset($options['product_id']) ? $query->where('id', $options['product_id']) : NULL;
        isset($options['status']) ? $query->where('status', $options['status']) : $query->where('status', '1');
        //limit
        $query->offset(($pageIndex - 1) * $pageSize)
                ->limit($pageSize);
        //execute
        $result = $query->get();
        if (!empty($result)) {
            foreach ($result as &$item) {
                $item['productType_link'] = route('productType', [
                    'id' => $item['id'],
                    'slug' => isset($item['slug']) && !empty($item['slug']) ? $item['slug'] : str_slug($item['name']),
                ]);
            }
        }
        $result = isset($options['id']) ? $result[0] : $result;
        $this->setCacheData($key, $result);
        return $result;
    }

}
