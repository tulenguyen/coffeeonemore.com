<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Cache;

class BaseModel extends Model
{
    protected $_prefix = 'coffeeonemore.com';
    protected $_key = '_KEYS_';

    protected $_cacheActive = false;

    public function __construct()
    {

    }

    public function hasCacheData($key)
    {
        return $this->_cacheActive ? Cache::has($key) : false;
    }

    public function getCacheData($key)
    {       
        //return null;
        $result = $this->_cacheActive ? Cache::get($this->_prefix . $key) : null;
        return $result;
    }

    public function setCacheData($key, $data, $ttl = 30000)
    {
        return Cache::put($this->_prefix . $key, $data, $ttl);
        return null;
    }

    public function setCacheActive($active = true)
    {
        $this->_cacheActive = $active;
        return $this;
    }
}