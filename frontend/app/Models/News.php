<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

use App\Models\Category;

class News extends BaseModel {

    const CREATED_AT = 'news_published_date';
    const UPDATED_AT = 'news_modified_date';

    protected $table = 'news';
    protected $_newsIds = [];

    public function setNewsIds($newsIds = []) {
        $this->_newsIds = $newsIds;
    }

    public function getNews($options = [], $pageIndex = 1, $pageSize = 10) {
        $key = __FUNCTION__ . '.' . implode('.', $options) . '.' . $pageIndex . '.' . $pageSize;
        if ($result = $this->getCacheData($key)) {
            return $result;
        }

        $categoryModel = new Category();
        if (isset($options['category_ids']) && !empty($options['category_ids'])) {
            $listIds = $categoryModel->getChildIds($options['category_ids']);
        }

        $query = $this::query();
        //select
        $query->select('news.news_id')
                ->addSelect('news_title', 'news_sapo', 'news_image', 'news_status', 'news_slug', 'category_id', 'visit_count', 'source_name', 'news_tag', 'news_published_date', 'news_modified_date', 'news_relation', 'news_seo_title', 'news_seo_description', 'news_seo_keyword');
        if (isset($options['get_content'])) {
            $query->addSelect('news_content');
        }

        //where
        isset($options['id']) ? $query->where('news_id', $options['id']) : NULL;
        isset($options['ids']) ? $query->whereIn('news_id', $options['ids']) : NULL;
        isset($listIds) && !empty($listIds) ? $query->whereIn('news_id', $listIds) : NULL;
        isset($options['category_id']) ? $query->where('category_id', $options['category_id']) : NULL;
        isset($options['status']) ? $query->where('news_status', $options['status']) : $query->where('news_status', '1');
        //join
        if (isset($options['position_id'])) {
            $query->join('news_position_rel', 'news.news_id', '=', 'news_position_rel.news_id')
                    ->where('news_position_rel.position_id', $options['position_id']);
        }

        //order by
        isset($options['order_by']) ? $query->orderBy($options['order_by']) : $query->orderBy('news_published_date', 'desc');
        //limit
        $query->offset(($pageIndex - 1) * $pageSize)
                ->limit($pageSize)->paginate($pageSize);
        //execute
        $result = $query->get();
        if (!empty($result)) {
            foreach ($result as &$item) {
                array_push($this->_newsIds, $item['news_id']);

                $item['news_title'] = str_replace('\'', '"', stripslashes($item['news_title']));

                $item['news_link'] = route('news', [
                    'slug' => isset($item['news_slug']) && !empty($item['news_slug']) ? $item['news_slug'] : str_slug($item['news_title']),
                    'id' => $item['news_id']
                ]);

                if (isset($options['get_related'])) {
                    if (isset($item['news_relation']) && !empty($item['news_relation'])) {
                        $item['related_news'] = $this->getNews([
                            'ids' => explode(',', $item['news_relation'])
                                ], 1, 10);
                    }
                }

                if (isset($item['category_id']) && !empty($item['category_id'])) {
                    $category = $categoryModel->getCategory(['category_id' => $item['category_id']]);
                    $category = isset($category[0]) ? $category[0] : [];
                    $item['category_link'] = isset($category['category_link']) ? $category['category_link'] : '';
                    $item['category_name'] = isset($category['category_name']) ? $category['category_name'] : '';
                }
            }
        }

        $this->setCacheData($key, $result);
        return $result;
    }

    public function getHomeNewNews($pageIndex = 1, $pageSize = 20) {
        $key = __FUNCTION__ . $pageIndex . '.' . $pageSize;
        if ($result = $this->getCacheData($key)) {
            return $result;
        }

        $result = $this->getNews(['status' => 1], $pageIndex, $pageSize);
        $this->setCacheData($key, $result);
        return $result;
    }

    public function getHomeHighlightNews() {
        $key = __FUNCTION__;
        if ($result = $this->getCacheData($key)) {
            return $result;
        }
        $result = $this->getNews([
            'position_id' => 1,
            'status' => 1
                ], 1, 5);
        $this->setCacheData($key, $result);
        return $result;
    }

    public function getNewsDetail($id) {
        $key = __FUNCTION__ . '.' . $id;
        if ($result = $this->getCacheData($key)) {
            return $result;
        }
        $news = $this->getNews([
            'id' => $id,
            'get_content' => 1,
            'get_related' => 1
        ]);

        if (isset($news[0]) && !empty($news[0])) {
            $news = $news[0];
        } else {
            return [];
        }

        $news['tags'] = preg_split('/,/', trim($news['news_tag']), null, PREG_SPLIT_NO_EMPTY);

        $this->setCacheData($key, $result);
        return $news;
    }

    public function getCategoryNews($options = array(), $pageIndex = 1, $pageSize = 10) {
        $key = __FUNCTION__ . '.' . implode('.', $options) . '.' . $pageIndex . '.' . $pageSize;
        if ($result = $this->getCacheData($key)) {
            return $result;
        }
        $parentId = 0;
        //options parent: slug
        if (isset($options['parent_id']) && !empty($options['parent_id'])) {
            $parent = $this->getCategory(array('id' => $options['parent_id']), 1);
            if (!empty($parent)) {
                $parentId = $parent['category_id'];
            }
        }
        $categoryModel = new Category();
        if ($category = $categoryModel->getCategory(array('id' => $options['category_id'], 'parent_id' => $parentId), 1)) {
            $category = isset($category[0]) ? $category[0] : [];

            $listNews = $this->getNews(array('category_id' => $category['category_id'],
                'others' => 0,
                'status' => 1), $pageIndex, $pageSize);
            $result = array(
                'category' => $category,
                'parent' => isset($parent) && !empty($parent) ? $parent : null,
                'news' => $listNews
            );
        }
        $this->setCacheData($key, $result);
        return $result;
    }

}
