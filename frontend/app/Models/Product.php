<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Product extends BaseModel {

    const CREATED_AT = 'create_at';
    const UPDATED_AT = 'update_at';

    protected $table = 'product';

    public function getProduct($options = [], $pageIndex = 1, $pageSize = 10) {

        $key = __FUNCTION__ . '.' . implode('.', $options) . '.' . $pageIndex . '.' . $pageSize;
        if ($result = $this->getCacheData($key)) {
            return $result;
        }
        $query = $this::query();
        //select
        $query->select('product.*');

        //where
        isset($options['id']) ? $query->where('id', $options['id']) : NULL;
        isset($options['ids']) ? $query->whereIn('id', $options['ids']) : NULL;
        isset($listIds) && !empty($listIds) ? $query->whereIn('id', $listIds) : NULL;
        isset($options['type_id']) ? $query->where('type_id', $options['type_id']) : NULL;
        isset($options['status']) ? $query->where('status', $options['status']) : $query->where('status', '1');
        //order by
        isset($options['order_by']) ? $query->orderBy($options['order_by']) : $query->orderBy('created_at', 'desc');
        //limit
        $query->offset(($pageIndex - 1) * $pageSize)
                ->limit($pageSize)->paginate($pageSize);
        //execute
        $result = $query->get();
        if (!empty($result)) {
            foreach ($result as &$item) {

                $item['product_link'] = route('product', [
                    'slug' => isset($item['slug']) && !empty($item['slug']) ? $item['slug'] : str_slug($item['name']),
                    'id' => $item['id']
                ]);

                if (isset($options['product_relation'])) {
                    if (isset($item['product_relation']) && !empty($item['product_relation'])) {
                        $item['related_products'] = $this->getProduct([
                            'ids' => explode(',', $item['product_relation'])
                                ], 1, 10);
                    }
                }
            }
        }
        $result = isset($options['id']) ? $result[0] : $result;
        $this->setCacheData($key, $result);
        return $result;
    }

}
