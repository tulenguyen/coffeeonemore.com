<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Cache;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Controller;
use App\Models\News;
use App\Models\Category;
use App\Models\ProductType;

class HomeController extends BaseController {

    public function index() {
        $newsModel = new News();

        $listNews = $newsModel->getHomeHighlightNews();

        $homeNews = [];

        $productTypeModel = new ProductType;
        $productTypes = $productTypeModel->getProductType();
        return view('home.index', ['data' => [
                'highLightNews' => !empty($listNews) ? $listNews : [],
                'homeNews' => !empty($homeNews) ? $homeNews : [],
                'productTypes' => !empty($productTypes) ? $productTypes : [],
        ]]);
    }

    public function ajax() {
        die('xxxxxxxxxx');
    }

}
