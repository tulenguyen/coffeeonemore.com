<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Controller;
use App\Models\News;

class NewsController extends BaseController
{
    public function index() {
        $id = Route::input('id');
        if ($id) {
            $model = new News();
            $news = $model->getNewsDetail($id);
            return view('news.index', ['news' => $news]);
        }
    }
}
