<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Controller;
use App\Models\ProductType;
use App\Models\Product;

class ProductTypeController extends BaseController {

    public function index() {
        $typeId = Route::input('id');
        $page = Route::input('page');
        $page = (!empty($page) && $page <> 1) ? $page : 1;
        $productModel = new Product();
        $productTypeModel = new ProductType();
        $paginate = ProductType::paginate(1);
        $listProduct = $productModel->getProduct(['type_id' => $typeId], $page);
        $productType = $productTypeModel->getProductType(['id'=>$typeId]);
        return view('productType.index', ['data' => [
                'productType'=>$productType,
                'listProduct' => !empty($listProduct) ? $listProduct : [],
                'page' => $page,
        ]])->with('paginate', $paginate);
    }

}
