<?php

namespace App\Http\Controllers;

use View;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Controller;
use App\Models\News;
use App\Models\Category;
use App\Models\ProductType;

class BaseController extends Controller
{
    public function __construct()
    {
        $categoryModel = new Category();
        $listCatNews = $categoryModel->getHeaderCategory();
        // Sharing is caring
        View::share('header_menu_cat', $listCatNews);
        
        $productTypeModel = new ProductType();
        $listProType = $productTypeModel->getProductType();

        View::share('header_menu_type',$listProType);
        /*
        if (in_array(Route::currentRouteName(), ['news'])) {
            $newsModel = new News();
            
            View::share('sidebarNews', [
                'news' => $newsModel->getHomeHighlightNews()
            ]);
        }
         * 
         */
    }
}