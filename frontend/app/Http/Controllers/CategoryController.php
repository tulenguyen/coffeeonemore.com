<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Controller;
use App\Models\Category;
use App\Models\News;

class CategoryController extends BaseController {

    public function index() {
        $categoryId = Route::input('id');
        $page = Route::input('page');
        $page = (!empty($page) && $page <> 1) ? $page : 1;
        $newsModel = new News();
        $paginate = Category::paginate(1);
        $listNews = $newsModel->getCategoryNews(['category_id' => $categoryId], $page);
        return view('category.index', ['data' => [
                'category'=>$listNews['category'],
                'listNews' => !empty($listNews) ? $listNews['news'] : [],
                'page' => $page,
                
        ]])->with('paginate', $paginate);
    }

}
