<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Controller;
use App\Models\ProductType;
use App\Models\Product;

class ProductController extends BaseController {

    public function index() {
        $id = Route::input('id');
        $productModel = new Product();
        $product = $productModel->getProduct(['id' => $id,'product_relation'=>1]);
        $productTypeModel = new ProductType();
        $productType = $productTypeModel->getProductType(['id' => $product['type_id']]);

        return view('product.index', ['data' => [
                        'productType' => $productType,
                        'product' => !empty($product) ? $product : [],
            ]]);
    }

}
