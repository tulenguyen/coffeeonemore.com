<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use Illuminate\Database\Eloquent\Model;
use App\Models\News;
use App\Models\Category;
use App\Models\Product;
use App\Models\ProductType;

class ManageCacheController extends Controller
{
    public function index(Request $request) {
        $refer  = $request->server('HTTP_REFERER');
        $cookie = $request->__get('_cookie_refer');

        $root   = $request->root();
       if (empty($cookie) && ((is_string($refer) && !preg_match('#://([^/]+)/#', $refer, $domain)) || false === strpos($root, $domain[1]))) {
            redirect('/notfound.html');
        } else {
            setcookie('_cookie_refer', 1, time() + 31536000);
        }
        $action = $request->input('action');
        
        switch ($action) {
            case 'request':
                $newsModel = new News();
                $categoryModel = new Category();
                $productModel = new Product();
                $productTypeModel = new ProductType();

                $newsModel->setCacheActive(false);
                $categoryModel->setCacheActive(false);
                $productModel->setCacheActive(false);
                $productTypeModel->setCacheActive(false);
                
                $data   = $request->input();
                
                if (isset($data['href']) && $href = urldecode($data['href'])){
                    if (preg_match('#/admin/([a-z0-9-]+)/?(\d+)?#is', $href, $match)){
                        switch ($match[1]){
                            case 'list-news':
                                if (isset($match[2])){
                                    // reset cache trang chi tiet
                                    $news = $newsModel->getNewsDetail($match[2]);
                                    if (isset($news['news_id'])){
                                        //Reset tin trang chuyen muc
                                        if (isset($news['category_id']) && !empty($news['category_id'])){
                                            $categoryModel->getCategoryNews(['id' => $news['category_id']], 1);
                                        }
                                    }
                                }
                                $newsModel->getHomeHighlightNews();
                                $productTypeModel->getProductType();
                                $categories = $categoryModel->getCategory();
                                foreach ($categories as $category){
                                    $categoryModel->getCategoryNews(['category_id' => $category['category_id']], 1);
                                    $categoryModel->getCategoryNews(['category_id' => $category['category_id']], 2);
                                }
                                break;

                            case 'edit-news':
                                if (isset($match[2])) {
                                    // reset cache trang chi tiet
                                    $news = $newsModel->getNewsDetail($match[2]);

                                    if (isset($news['news_id'])){
                                        //Reset tin trang chuyen muc
                                        if (isset($news['category_id']) && !empty($news['category_id'])){
                                            $category = $categoryModel->getCategoryNews(['id' => $news['category_id']], 1);
                                        }
                                    }
                                }

                                $newsModel->getHomeHighlightNews();
                                $productTypeModel->getProductType();
                                $categories = $categoryModel->getCategory();
                                foreach ($categories as $category){
                                    $categoryModel->getCategoryNews(['category_id' => $category['category_id']], 1);
                                    $categoryModel->getCategoryNews(['category_id' => $category['category_id']], 2);
                                }
                                break;

                            case 'edit-category':
                                if (isset($match[2])){
                                    $categoryModel->getCategoryNews(['category_id' => $match[2]], 1);
                                }
                                $newsModel->getHomeHighlightNews();
                                $productTypeModel->getProductType();
                                $categories = $categoryModel->getCategory();
                                break;

                            case 'list-product':
                                if (isset($match[2])){
                                    $product = $productModel->getProductDetail($match[2]);
                                    if (isset($product['type_id']) && !empty($product['type_id'])) {
                                        $productTypeModel->getProductTypeList(['id' => $product['type_id']], 1);
                                    }
                                }

                                $productModel->getHomeProductTypeData();
                                break;

                            case 'edit-product':
                                if (isset($match[2])){
                                    $product = $productModel->getProduct(['id' => $match[2],'product_relation'=>1]);
                                    if (isset($product['type_id']) && !empty($product['type_id'])) {
                                        $productTypeModel->getProductType(['id' => $product['type_id']]);
                                    }
                                }
                                
                                break;


                            case 'edit-product-type':
                                if (isset($match[2])){
                                    $productTypeModel->getProductTypeList(['id' => $match[2]], 1);
                                }

                                $productModel->getHomeProductTypeData();
                                break;
                                /*
                            case 'edit-article':
                                $articleModel->getArticle(['ids' => '1,6', 'get_content' => 1]);
                                $articleModel->getArticle(['ids' => '1,2,3,4,5', 'get_content' => 1]);
                                $articleModel->getArticle(['id' => 6, 'get_content' => 1]);
                                $articleModel->getArticle(['ids' => '7,8,9', 'get_content' => 1]);
                                break;

                            case 'list-article':
                                $articleModel->getArticle(['ids' => '1,6', 'get_content' => 1]);
                                $articleModel->getArticle(['ids' => '1,2,3,4,5', 'get_content' => 1]);
                                $articleModel->getArticle(['id' => 6, 'get_content' => 1]);
                                $articleModel->getArticle(['ids' => '7,8,9', 'get_content' => 1]);
                                break;
                                 * 
                                 */
                        }
                    }
                }
                die;
                break;
        }
    }
}