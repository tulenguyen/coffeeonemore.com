
<div class="banner">
    <img src="{{ URL::asset('images/banner_coffee.jpg') }}">
    <h1><?= $productType['name'] ?></h1>
</div>

<div class="product-grid clearfix">	
    <div class="onepcssgrid-1000">
        <div class="onerow">
            <div class="col3 first" id="product-image">
                <img src="<?= $product['image'] ?>" id="product-detail-image" border="0">
                <div id="ebeans-product-sups">
                </div>
            </div>
            <div class="col9">
                <h1><?= $product['name'] ?></h1>

                <div id="ebeans-product-sups">		
                    <div id="ebeans-profile-score">
                    </div>
                    <div id="ebeans-profile-region">
                        <h3>Sản Phẩm</h3>
                        <div class="sup-entry"><?= $product['short_description'] ?></div>
                    </div>
                    <div class="product-description">
                        <h3>Chi Tiết</h3>
                        <?= $product['description'] ?>    
                    </div>
                </div>
            </div>
           
        </div>
        @if(isset($product['related_products']) && !empty($product['related_products']))
        <div class="onerow">
            <div class="col3 first"></div>
            <div class="col9 last">
                <div class="pagebreak"></div>
                <h3 class="rec-prod-title">Sản Phẩm Liên Quan</h3>
                <div class="onerow">
                    <div class="col12">
                        @foreach($product['related_products'] as $relate)
                        <div class="reccomend-prods-cell left">
                            <a href="{{ $relate['product_link']}}?>">
                                <img src="{{ $relate['image']}}" width="120" border="0"></a><br>
                        </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
        @endif
    </div>	

</div>

<div style="clear:both"></div>		

</div>  
