
@extends('layout.ProductLayout')
@section('main')
    @include('product/Product',['product'=>$data['product'],'productType'=>$data['productType']])
@endsection