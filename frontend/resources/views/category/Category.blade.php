@section('title', $data['category']['category_name'])

<section id="page" role="main" data-content-field="main-content">
    <div class="sqs-layout sqs-grid-12 columns-12" data-type="page" >
        <div style="font-size:20px; margin-bottom: 20px">
            <span xmlns:v="http://rdf.data-vocabulary.org/#">
                <span typeof="v:Breadcrumb">
                    <a href="/" rel="v:url" property="v:title">Trang chủ</a>
                    »
                    <a href="{{ $data['category']['category_link'] }}" rel="v:url" property="v:title">
                        {{ $data['category']['category_name'] }}
                    </a>
                </span>
            </span>
        </div>
        <div class="row sqs-row">
            @if(isset($data['listNews']) && !empty($data['listNews']))
            @foreach($data['listNews'] as $news)
            <div class="col sqs-col-7 span-7">
                <div class="sqs-block image-block sqs-block-image" data-block-type="5" id="block-2f39d94bde249d9abbec">
                    <div class="sqs-block-content"> 
                        <div class="image-block-outer-wrapper layout-caption-below design-layout-inline">
                            <div class="intrinsic">
                                <div style="padding-bottom:66.66667175292969%;" class="image-block-wrapper   has-aspect-ratio" >
                                    <a href="{{ $news['news_link'] }}">
                                        <img src="{{ $news['news_image'] }}"  />
                                    </a>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
            <div class="col sqs-col-5 span-5">
                <div class="sqs-block html-block sqs-block-html">
                    <div class="sqs-block-content">
                        <a href="{{ $news['news_link'] }}">
                            <p><strong>{{ $news['news_title'] }}</strong></p>
                        </a>
                        <p>                            
                            {{ $news['news_sapo'] }}
                        </p>
                    </div>
                </div>
            </div>
            @endforeach
            @endif
        </div>

    </div>
</section>
{!! $paginate->render() !!}