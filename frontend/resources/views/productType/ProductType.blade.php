
<div class="banner">
    <img src="{{ URL::asset('images/banner_coffee.jpg') }}">
    <h3>{{$productType['name']}}</h3>
</div>


<div class="product-grid clearfix">				
    <div id="ebeans_category_text">
    </div>

    <div style="clear: both"></div>

    <ul class="sub-nav">
    </ul>

    <div id="cart-grid-table">
        @if(isset($listProduct) && !empty($listProduct))
        @for($i=0;$i<count($listProduct);$i=$i+4)
            <div class="onerow">
                <div class="col6 first">
                    <div class="prod-grid-container left">
                        <a href="{{$listProduct[$i]['product_link']}}">
                            <img src="{{$listProduct[$i]['image']}}" alt="{{$listProduct[$i]['name']}}" class="grid-image" border="0" /></a>
                        <div class="buy">
                            <div class="grid-prod-price">{{$listProduct[$i]['name']}}</div>
                        </div>
                    </div>
                    @if(isset($listProduct[$i+1]) && !empty($listProduct[$i+1]))
                    <div class="prod-grid-container right">
                        <a href="{{$listProduct[$i+1]['product_link']}}">
                            <img src="{{$listProduct[$i+1]['image']}}" alt="{{$listProduct[$i+1]['name']}}" class="grid-image" border="0" /></a>
                        <div class="buy">
                            <div class="grid-prod-price">{{$listProduct[$i+1]['name']}}</div>
                        </div>
                    </div>
                    @endif
                </div>
                <div class="col6 last">
                    @if(isset($listProduct[$i+2]) && !empty($listProduct[$i+2]))

                    <div class="prod-grid-container left">
                        <a href="{{$listProduct[$i+2]['product_link']}}">
                            <img src="{{$listProduct[$i+2]['image']}}" alt="{{$listProduct[$i+2]['name']}}" class="grid-image" border="0" /></a>
                        <div class="buy">
                            <div class="grid-prod-price">{{$listProduct[$i+2]['name']}}</div>
                        </div>
                    </div>
                    @endif
                    @if(isset($listProduct[$i+3]) && !empty($listProduct[$i+3]))
                    <div class="prod-grid-container right">
                        <a href="{{$listProduct[$i+3]['product_link']}}">
                            <img src="{{$listProduct[$i+3]['image']}}" alt="{{$listProduct[$i+3]['name']}}" class="grid-image" border="0" /></a>
                        <div class="buy">
                            <div class="grid-prod-price">{{$listProduct[$i+3]['name']}}</div>
                        </div>
                    </div>
                    @endif
                </div>
            </div>

            @endfor
            @endif

    </div><!-- End grid-table -->

    <div style="clear:both"></div>		

</div>  
