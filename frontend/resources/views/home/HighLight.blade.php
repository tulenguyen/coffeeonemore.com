<section id="page" role="main" data-content-field="main-content">
    <div class="sqs-layout sqs-grid-12 columns-12" data-type="page" data-updated-on="1489605071004" id="page-54d642a3e4b012482364b2ff">
        <div class="row sqs-row">
            <div class="col sqs-col-12 span-12">
                @if(isset($highLightNews) && !empty($highLightNews))
                <div class="flexslider carousel">               
                    <ul class="slides">        
                        @foreach($highLightNews as $news)
                        <li><a href="{{$news['news_link']}}"><img src="{{ $news['news_image'] }}"></li>
                        @endforeach
                    </ul>
                </div><!-- .flexslider -->
                @endif
                <div class="sqs-block html-block sqs-block-html" data-block-type="2" id="block-931fecbfab5322b5e8bf"><div class="sqs-block-content">
                        <h1 class="text-align-center">
                            <a href="#">Sản Phẩm Chính Hiệu</a>
                        </h1>
                    </div>
                </div>
                <div class="sqs-block code-block sqs-block-code" data-block-type="23" id="block-yui_3_17_2_2_1433471415470_12623">
                    <div class="sqs-block-content"><ul class="shop-with-us-2 sqs-row">
                            <li class="coffee"><a href="{{$productTypes[0]['productType_link']}}"><img src="{{ URL::asset('images/cf1.jpg') }}"></a></li>
                            <li class="tea"><a href="{{$productTypes[1]['productType_link']}}"><img src="{{ URL::asset('images/cf2.jpg') }}"></a></li>
                            <li class="merch"><a href="{{$productTypes[2]['productType_link']}}"><img src="{{ URL::asset('images/cf3.jpg') }}"></a></li>
                        </ul>
                    </div>
                </div>
<script type="text/javascript">

    (function () {

        // store the slider in a local variable
        var $window = $(window),
                flexslider = {vars: {}};

        // tiny helper function to add breakpoints
        function getGridSize() {
            return (window.innerWidth < 600) ? 1 :
                    (window.innerWidth < 900) ? 1 : 1;
        }

        $(function () {
            SyntaxHighlighter.all();
        });

        $window.load(function () {
            $('.flexslider').flexslider({
                animation: "slide",
                slideshowSpeed: 4000,
                animationSpeed: 600,
                animationLoop: false,
                itemWidth: 210,
                itemMargin: 5,
                minItems: getGridSize(), // use function to pull in initial value
                maxItems: getGridSize(), // use function to pull in initial value
                start: function (slider) {
                    $('body').removeClass('loading');
                    flexslider = slider;
                }
            });
        });

        // check grid size on resize event
        $window.resize(function () {
            var gridSize = getGridSize();

            flexslider.vars.minItems = gridSize;
            flexslider.vars.maxItems = gridSize;
        });
    }());

</script>

