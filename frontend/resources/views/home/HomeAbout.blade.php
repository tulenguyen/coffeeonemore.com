<section id="about_home">
    <div class="container">
        <div class="row">
            <div class="col-md-6">
                <div class="img_about">
                    <img class="img-responsive" src="images/about.png" alt="gioi thieu"/>
                </div>
            </div>
            <div class="col-md-6">
                <div class="content_about">
                    <h2>
                        {{ $about['title'] }}
                    </h2>
                    <p>
                        {!! $about['content'] !!}
                    </p>
                    <a href="{{ $about['article_link'] }}" class="xemtheme">
                        Xem thêm
                    </a>
                </div>
            </div>
        </div>
    </div>
</section>