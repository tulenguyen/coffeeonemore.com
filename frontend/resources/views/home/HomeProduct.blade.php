<div class="sqs-block html-block sqs-block-html" data-block-type="2" id="block-yui_3_17_2_2_1433253350261_16253">
    <div class="sqs-block-content">
        <h1 class="text-align-center" id="yui_3_17_2_2_1433253350261_13792">Phụ Kiện One More</h1>
    </div>
</div>

<div class="sqs-block gallery-block sqs-block-gallery">
    <div class="sqs-block-content">
        <div class=" sqs-gallery-container  sqs-gallery-block-grid sqs-gallery-aspect-ratio-square  sqs-gallery-thumbnails-per-row-4  clear"  >
            <div class="sqs-gallery">
                <div class="slide" data-type="image">
                    <div class="margin-wrapper">

                        <a href="#" class="image-slide-anchor content-fit" >
                            <img src="{{URL::asset('images/bag.jpg')}}"  alt="Equator Camp Cup" class="thumb-image loaded"/>
                        </a>

                    </div>
                </div>
                <div class="slide" data-type="image">
                    <div class="margin-wrapper">

                        <a href="#" class="image-slide-anchor content-fit" >
                            <img src="{{URL::asset('images/non_bh.jpg')}}"  alt="Equator Camp Cup"  />
                        </a>

                    </div>
                </div>
                <div class="slide" data-type="image">
                    <div class="margin-wrapper">

                        <a href="#" class="image-slide-anchor content-fit" >
                            <img src="{{URL::asset('images/gift_box.jpg')}}"  alt="Equator Camp Cup"  />
                        </a>

                    </div>
                </div>
                <div class="slide" data-type="image">
                    <div class="margin-wrapper">

                        <a href="#" class="image-slide-anchor content-fit" >
                            <img src="{{URL::asset('images/ly_su.jpg')}}"  alt="Equator Camp Cup"  />
                        </a>

                    </div>
                </div>

            </div>

        </div>

    </div>

</div>


</div>

</div>

</div>
</section>