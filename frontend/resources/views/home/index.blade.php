@extends('layout.HomeLayout')

@section('title', 'Trang chủ')

@section('main')
    @include('home/HighLight', ['highLightNews' => $data['highLightNews'],'productTypes' => $data['productTypes']])
    @include('home/HomeNews', ['homeNews' => $data['homeNews']])
    @include('home/HomeProduct', ['productTypes' => $data['productTypes']])
@endsection