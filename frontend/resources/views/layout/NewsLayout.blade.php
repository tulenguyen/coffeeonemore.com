@include('common/Top')
@include('common/Header')
<section id="page" role="main" data-content-field="main-content">
    <div class="sqs-layout sqs-grid-12 columns-12" data-type="page" data-updated-on="1491604190474" id="page-55237711e4b0ea389c39bb20">
        <div class="row sqs-row">
            <div class="col sqs-col-12 span-12">
                @yield('main')
            </div>
        </div>
    </div>
</section>
@include('common/Footer')
@include('common/Bottom')