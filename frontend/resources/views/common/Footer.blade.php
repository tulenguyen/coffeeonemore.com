

<footer id="page-footer">
    <div class="colored-footer-row">
        <div class="content-skeleton">
            <article id="about">
                <h5 class="widget_title">Follow us</h5>
                <ul class="footer_socials">
                    <li>
                        <a href="#">
                            <img src="{{ URL::asset('images/facebook-icon.png') }}" alt="" width="30px">
                        </a>
                    </li>
                    <li>
                        <a href="#">
                            <img src="{{ URL::asset('images/youtube-icon.png') }}" alt="" width="30px">
                        </a>
                    </li>
                </ul>
            </article>
            <article id="footer-recent-posts">
                <h5 class="widget_title">
                    Liên hệ
                </h5>
                <ul class="recent_posts_widget">
                    <li>
                        <h6 class="recent_post_title">
                                Công ty cổ phần <strong>One More Việt Nam</strong>
                        </h6>
                    </li>
                    <li>
                        <h6 class="recent_post_title">
                            <a href="#">
                                <br>
                                <b>ĐC:</b> 17-19 Tôn Thất Tùng, Phường Phạm Ngũ Lão, Quận 1, Tp Hồ Chí Minh.
                            </a>
                        </h6>
                    </li>
                    <li>
                        <h6 class="recent_post_title">
                            <a href="#">
                                <br>
                                <b>Điện thoại</b>: (84-8) 39250215 - <b>Fax</b>: (84-8) 39240216
                                <br>
                                <b>Mail</b>: Contact@coffeeonemore.com
                            </a>
                        </h6>
                    </li>
                </ul>
            </article>
            <article id="footer-socials">
                <h5 class="widget_title">Thương hiệu đã được chứng nhận</h5>
                <ul class="footer_socials">
                    <li>
                        <a href="#">
                            <img src="{{URL::asset('images/logo-footer.jpg')}}" width="200px">
                        </a>
                    </li>
                </ul>
            </article>
        </div>
    </div>
    <!-- /colored-footer-row -->
    
</footer>