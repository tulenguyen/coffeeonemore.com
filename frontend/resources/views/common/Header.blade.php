<header id="header">
    <div id="logo" data-content-field="site-title">

        <h1 class="logo"><a href="/"><img src="{{URL::asset('images/logo_onemore.jpg')}}" alt="One More Coffee" /></a></h1>

    </div>
    <div class="toggle mobile-nav-toggle-btn"><a href="#mobileNav" id="mobile-show-nav"></a></div>

    <div id="topNav"  data-content-field="navigation">

        <nav class="main-nav dropdown-hover">
            <ul id="siteNav">
                <li class="folder-collection folder SHOP">
                    <a aria-haspopup="true" href="/">
                        Trang Chủ 
                    </a>
                </li>
                @if (isset($header_menu_cat) && !empty($header_menu_cat))
                @foreach ($header_menu_cat as $category)
                <li class="folder-collection folder SHOP">
                    <a aria-haspopup="true" href="{{ $category['category_link'] }}">
                        {{ $category['category_name'] }}
                    </a>
                </li>
                @endforeach
                @endif
                <li class="folder-collection folder SHOP">
                    <a aria-haspopup="true" href="#">
                        Sản Phẩm 
                    </a>
                    <div class="subnav">
                        <ul>
                            @if (isset($header_menu_type) && !empty($header_menu_type))
                            @foreach ($header_menu_type as $productType)
                            <li class="page-collection" >
                                <a  href="{{ $productType['productType_link'] }}">
                                    {{ $productType['name'] }}
                                </a>
                            </li>
                            @endforeach
                            @endif
                        </ul>
                    </div>
                </li>
                 <li class="folder-collection folder SHOP">
                    <a aria-haspopup="true" href="http://coffeeonemore.com/he-thong-phan-phoi-ca-phe-one-more-viet-nam-n21">
                        Hệ Thống Phân Phối 
                    </a>
                </li>
                
            </ul>

            <ul id="mobileNav">
                <li><a href="#canvas" id="mobile-close-nav"></a></li>
                <li>
                    <a aria-haspopup="true" href="/">
                        Trang Chủ 
                    </a>
                </li>
                @if (isset($header_menu_cat) && !empty($header_menu_cat))
                @foreach ($header_menu_cat as $category)
                <li>
                    <a aria-haspopup="true" href="{{ $category['category_link'] }}">
                        {{ $category['category_name'] }}
                    </a>
                </li>
                @endforeach
                @endif
                <li>
                    <a aria-haspopup="true" href="#">
                        Sản Phẩm
                    </a>
                </li>
                @if (isset($header_menu_type) && !empty($header_menu_type))
                @foreach ($header_menu_type as $productType)
                <ul class="mobile-subnav">
                    <li class="external-link">
                        <a href="{{$productType['productType_link']}}">{{$productType['name']}}</a>
                    </li>
                </ul>
                @endforeach
                @endif
                
                <li>
                    <a aria-haspopup="true" href="#">
                        Hệ Thống Phân Phối 
                    </a>
                </li>
            </ul>

        </nav>

    </div>

</header>
