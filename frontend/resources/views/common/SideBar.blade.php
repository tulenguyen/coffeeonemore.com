<div class="col-md-4">
    <div class="box_widgets_right">
        <div class="box_ads">
            <img class="img-responsive" src="images/qc.png">
        </div>
        <div class="box_news_care">
            <h4>Tin được quan tâm</h4>
            @for ($i = 0; $i <= 1; $i++)
                @if (isset($sidebarNews['news'][$i]) && !empty($sidebarNews['news'][$i]))
                    @php
                        $item = $sidebarNews['news'][$i];
                    @endphp
                @endif
                <div class="item_news_care">
                    <a href="{{ $item['news_link'] }}">
                        <img class="img-responsive" src="{{ $item['news_image'] }}">
                    </a>
                    <h3>
                        <a href="{{ $item['news_link'] }}">
                            {{ $item['news_title'] }}
                        </a>
                    </h3>
                    <p>
                        {{ $item['news_sapo'] }}
                    </p>
                </div>
            @endfor
        </div>
    </div>
</div>