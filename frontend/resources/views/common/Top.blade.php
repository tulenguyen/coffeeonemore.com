


<!doctype html>
<html xmlns:og="http://opengraphprotocol.org/schema/" xmlns:fb="http://www.facebook.com/2008/fbml" xmlns:website="http://ogp.me/ns/website" lang="en-US" itemscope itemtype="http://schema.org/WebPage" >
    <head>
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">

        <meta name="viewport" content="width=device-width, initial-scale=1">

        <!-- This is Squarespace. --><!-- ian-levine-4s6s -->
        <base href="">
        <meta charset="utf-8" />
        <title>@yield('title')</title>
        <link rel="shortcut icon" type="image/x-icon" href="{{URL::asset('images/logo-onemore.jpg')}}"/>
        <link rel="shortcut icon" href="{{ URL::asset('images/favicon.ico') }}" />

        <link rel="stylesheet" type="text/css" href="//fonts.googleapis.com/css?family=Actor:normal|Exo+2:700,700italic|Open+Sans:800,800italic,700normal,700italic,300,300italic,400"/>

        <link rel="stylesheet" href="{{ URL::asset('css/ebeans.css?v=2') }}">
        <link rel="stylesheet" href="{{ URL::asset('css/style.css?v=1') }}">

        <link rel="stylesheet" href="{{ URL::asset('css/flexslider.css?v=7') }}">

        <link rel="stylesheet" href="{{ URL::asset('vendors/font-awesome/css/font-awesome.min.css') }}" />

        <script type="text/javascript" src="{{ URL::asset('js/jquery-1.10.2.min.js') }}"></script> 

        <script type="text/javascript" src="{{ URL::asset('js/common.js') }}"></script> 
        <script type="text/javascript" src="{{ URL::asset('js/jquery.flexslider.js') }}"></script>    


    </head>

    <body class="header-alignment-left footer-alignment-center blog-sidebar-right page-borders-thin folder--category-nav-style-fill site-drop-shadow social-icons-show    hide-page-title    show-products-category-navigation   product-list-titles-overlay product-list-alignment-center product-item-size-32-standard  product-gallery-size-11-square  show-product-price show-product-item-nav product-social-sharing gallery-design-slideshow aspect-ratio-auto lightbox-style-dark gallery-navigation-thumbnails gallery-info-overlay-always-show gallery-aspect-ratio-32-standard gallery-arrow-style-round-corners gallery-transitions-fade gallery-show-arrows gallery-auto-crop    event-show-past-events event-thumbnails event-thumbnail-size-32-standard event-date-label  event-list-show-cats event-list-date event-list-time event-list-address   event-icalgcal-links  event-excerpts      hide-opentable-icons opentable-style-dark newsletter-style-dark small-button-style-outline small-button-shape-square medium-button-style-outline medium-button-shape-square large-button-style-outline large-button-shape-square button-style-outline button-corner-style-square tweak-product-quick-view-button-style-floating tweak-product-quick-view-button-position-bottom tweak-product-quick-view-lightbox-excerpt-display-truncate tweak-product-quick-view-lightbox-show-arrows tweak-product-quick-view-lightbox-show-close-button tweak-product-quick-view-lightbox-controls-weight-light native-currency-code-usd collection-54d642a3e4b012482364b2ff collection-type-page collection-layout-default homepage mobile-style-available logo-image home" id="collection-54d642a3e4b012482364b2ff">
        <div id="canvasWrapper">
            <div id="canvas">