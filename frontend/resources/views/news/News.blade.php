@section('title', $news['news_title'])

<div class="col-md-8">
    <div class="breadcrumbs">
        <span xmlns:v="http://rdf.data-vocabulary.org/#">
            <span typeof="v:Breadcrumb">
                <a href="/" rel="v:url" property="v:title">Trang chủ</a>
                »
                <a href="{{ $news['category_link'] }}" rel="v:url" property="v:title">
                    {{ $news['category_name'] }}
                </a>
                »
                <span class="breadcrumb_last">{{ $news['news_title'] }}</span>
            </span>
        </span>
    </div>
    <article class="single_post">
        <h1>{{ $news['news_title'] }}</h1>
        <p style="font-style: oblique">
                {{ $news['news_sapo'] }}
        </p>
        {!! $news['news_content'] !!}
    </article>
</div>

