<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/',[
    'as'    => 'home',
    'uses'  => 'HomeController@index'
]);

Route::get('ajax', [
    'as'    => 'ajax',
    'uses'  => 'HomeController@ajax'
]);

Route::get('managecache', [
    'uses' => 'ManageCacheController@index'
]);

Route::get('{slug}-n{id}', [
    'as'    => 'news',
    'uses'  => 'NewsController@index'
])->where(['slug' => '[\w\d-]+', 'id' => '\d+']);

Route::get('n{id}/{slug}/trang-{page}', [
    'as'    => 'category',
    'uses'  => 'CategoryController@index'
])->where(['id' => '\d+', 'slug' => '[\w\d-]+', 'page' => '\d+']);

Route::get('n{id}/{slug}', [
    'as'    => 'category',
    'uses'  => 'CategoryController@index'
])->where(['id' => '\d+', 'slug' => '[\w\d-]+']);


Route::get('{slug}-p{id}', [
    'as'    => 'product',
    'uses'  => 'ProductController@index'
])->where(['slug' => '[\w\d-]+', 'id' => '\d+']);

Route::get('p{id}/{slug}/trang-{page}', [
    'as'    => 'productType',
    'uses'  => 'ProductTypeController@index'
])->where(['id' => '\d+', 'slug' => '[\w\d-]+', 'page' => '\d+']);

Route::get('p{id}/{slug}', [
    'as'    => 'productType',
    'uses'  => 'ProductTypeController@index'
])->where(['id' => '\d+', 'slug' => '[\w\d-]+']);