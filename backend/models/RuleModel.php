<?php
class RuleModel extends Model{
	public function getRuleById($id, $status = null)
	{
		$sql 	= 'select * from rule where rule_id = :id'.
				($status === null ? '' : ' and rule_status = :status ').' limit 1';
		$sql 	= $this->_conn->prepareSQL($sql, array( 'id' 		=> $id,
				                                        'status' 	=> $status));
		return  $this->_conn->fetchRow($sql);
	}
	
	public function GetRouteByRoleId($roleId){
        $sql = 'SELECT route_id FROM rule WHERE role_id = :role_id';
        $sql = $this->_conn->prepareSQL($sql, array('role_id' => $roleId));
        $routes = $this->_conn->fetchAll($sql);
        $listRoute = array();
        if (!empty($routes)){
            foreach ($routes as $route){
                array_push($listRoute, $route['route_id']);
            }
        }
        return implode(',', $listRoute);
	}
	
	public function getRules($status = null)
	{
		$sql = 'select r.*, ro.role_name from rule r inner join role ro on r.role_id = ro.role_id
		'. ($status === null ? '' : ' and r.rule_status = '. $status) .'
		order by r.role_id, r.rule_id desc';
		$records = $this->_conn->fetchAll($sql);
		return $records;
	}
	
	
	public function addRule($item)
	{
		$sql = 'insert into rule  	(rule_name,
									rule_status,
									role_id,
									route_id)
									values (:name,
									:status,
									:role_id,
									:route_id)';
	
		$sql = $this->_conn->prepareSQL($sql, $item);
		$this->_conn->execute($sql, true);
	
		return $this->_conn->getAffectedRows() > 0;
	}
	
	public function updateRule($item)
	{
		$sql = 'update rule set rule_name 		= :name,
								role_id 		= :role_id,
								route_id 		= :route_id,
								rule_status 	= :status
								where rule_id 	= :rule_id';
	
		$sql = $this->_conn->prepareSQL($sql, $item);
		$this->_conn->execute($sql, true);
		return $this->_conn->getAffectedRows() > 0;
	}
	
	public function UpdateRoleRoute($roleId, $routeId){
		$sql = 'delete from rule where role_id = ' . $roleId;
		$this->_conn->execute($sql, true);
		
		$sql = 'insert into rule(role_id, route_id) ' .
				'values (' . $roleId . ',' .  implode('),('. $roleId.',',explode(',', $routeId)) .')';
		$this->_conn->execute($sql, true);
	}
}
?>