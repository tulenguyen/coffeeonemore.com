<?php

class NewsModel extends Model {

    public function UpdateNewsStatus($newsIds, $status) {
        $this->_conn->connect();
        $sql = 'UPDATE news SET news_status = ' . $status . ' WHERE news_id IN (' . $newsIds . ')';
        $this->_conn->execute($sql, true);
        return $this->_conn->getAffectedRows();
    }

    public function getNews($options = array(), $pageIndex = 1, $pageSize = 10) {
        $condition = '';
        if (isset($options['title_like']) && !empty($options['title_like'])) {
            $options['title_like'] = '%' . $options['title_like'] . '%';
        }
        $sql = 'SELECT ' .
                (isset($options['field']) ? $options['field'] : '* ') .
                ' FROM news AS n ' .
                (isset($options['position_id']) && !empty($options['position_id']) ? ' INNER JOIN news_position_rel AS npr ON npr.news_id = news_id' : '') .
                ' WHERE 1=1 ' . $condition .
                (isset($options['status']) ? ' AND news_status = :status ' : '') .
                (isset($options['news_id']) ? ' AND news_id = :news_id ' : '') .
                (isset($options['news_ids']) ? ' AND news_id IN (' . $options['news_ids'] . ') ' : '') .
                (isset($options['title_like']) && !empty($options['title_like']) ? ' AND news_title LIKE :title_like ' : '') .
                (isset($options['filter_user_id']) && !empty($options['filter_user_id']) ? ' AND user_id = :filter_user_id ' : '') .
                (isset($options['approved_user']) && !empty($options['approved_user']) ? ' AND approved_user = :approved_user' : '') .
                (isset($options['position_id']) && !empty($options['position_id']) ? ' AND npr.position_id = :position_id ' : '') .
                (isset($options['start_date']) && !empty($options['start_date']) ? ' AND news_published_date >= ' . $options['start_date'] : '') .
                (isset($options['end_date']) && !empty($options['end_date']) ? ' AND news_published_date <= ' . $options['end_date'] : '') .
                ' ORDER BY ' .
                (isset($options['order_by']) ? $options['order_by'] : ' news_published_date') .
                (isset($options['order_asc']) ? '' : ' DESC') .
                ($pageIndex > 0 ? ' LIMIT ' . (($pageIndex - 1) * $pageSize) . ',' . $pageSize : '');
        $sql = $this->_conn->prepareSQL($sql, $options);
        $records = $pageSize == 1 || isset($options['news_id']) ? array($this->_conn->fetchRow($sql)) : $this->_conn->fetchAll($sql);
        if (!empty($records)) {
            foreach ($records as &$item) {
                if (isset($options['join_category'])) {
                    $sql = 'SELECT parent_id, category_name FROM category WHERE category_id = ' . $item['category_id'];
                    $result = $this->_conn->fetchRow($sql);
                    $item['category_name'] = $result['category_name'];

                    if (isset($result['parent_id']) && !empty($result['parent_id'])) {
                        $sql = 'SELECT category_name from category WHERE category_id = ' . $result['parent_id'];
                        $result = $this->_conn->fetchRow($sql);
                        $item['parent_name'] = $result['category_name'];
                    }
                }

                if (isset($options['join_user'])) {
                    if (isset($item['user_id'])) {
                        $sql = 'SELECT user_name as create_name FROM user WHERE user_id = ' . $item['user_id'];
                        $result = $this->_conn->fetchRow($sql);
                        $item['create_name'] = $result['create_name'];
                    }
                    if (isset($item['modified_user'])) {
                        $sql = 'SELECT user_name as modified_name FROM user WHERE user_id = ' . $item['modified_user'];
                        $result = $this->_conn->fetchRow($sql);
                        $item['modified_name'] = $result['modified_name'];
                    }
                    if (isset($item['approved_user'])) {
                        $sql = 'SELECT user_name as approved_name FROM user WHERE user_id = ' . $item['approved_user'];
                        $result = $this->_conn->fetchRow($sql);
                        $item['approved_name'] = $result['approved_name'];
                    }
                }

                if (isset($options['join_status'])) {
                    $sql = 'SELECT news_status, status_name FROM news_status WHERE news_status = ' . $item['news_status'];
                    $result = $this->_conn->fetchRow($sql);
                    $item['news_status'] = !empty($result['news_status']) ? $result['news_status'] : '0';
                    $item['status_name'] = !empty($result['status_name']) ? $result['status_name'] : '0';
                }

                if (isset($options['join_position_rel'])) {
                    $sql = 'SELECT position_id FROM news_position_rel WHERE news_id = ' . $item['news_id'];
                    $result = $this->_conn->fetchRow($sql);
                    $item['position_id'] = !empty($result['position_id']) ? $result['position_id'] : '0';
                }

                if (isset($item['news_slug']) && isset($item['news_status']) && isset($item['news_id'])) {
                    $newsLink = Context::getInstance()->getRoute()->createUrl('News', [
                        'slug' => isset($item['news_slug']) && !empty($item['news_slug']) ? $item['news_slug'] : Util::toUnsign($item['news_title']),
                        'id' => $item['news_id']
                    ]);
                    if (!empty($newsLink) && $item['news_status'] == 1) {
                        $item['link_web'] = Helper::getInstance()->getConfig('domain') . $newsLink;
                    }
                }
                $item['link_preview'] = Helper::getInstance()->getConfig('domain') . 'preview-' . $item['news_id'] . '.html?mode=preview';
            }
            $records = $pageSize == 1 || isset($options['news_id']) ? $records[0] : $records;
        }

        return $records;
    }

    public function getCountNews($options = array()) {
        $condition = '';
        if (isset($options['title_like']) && !empty($options['title_like']) && $options['title_like'] != 'Searching...') {
            $options['title_like'] = '%' . $options['title_like'] . '%';
        }
        $sql = 'SELECT count(*) AS count FROM news AS n ' .
                (isset($options['position_id']) && !empty($options['position_id']) ? ' INNER JOIN news_position_rel AS npr ON npr.news_id = n.news_id' : '') .
                ' WHERE 1=1 ' . $condition .
                (isset($options['status']) ? ' AND news_status = :status ' : '') .
                (isset($options['news_id']) ? ' AND news_id = :news_id ' : '') .
                (isset($options['news_ids']) ? ' AND news_id IN (' . $options['news_ids'] . ') ' : '') .
                (isset($options['title_like']) && !empty($options['title_like']) ? ' AND news_title LIKE :title_like ' : '') .
                (isset($options['filter_user_id']) && !empty($options['filter_user_id']) ? ' AND user_id = :filter_user_id ' : '') .
                (isset($options['approved_user']) && !empty($options['approved_user']) ? ' AND approved_user = :approved_user' : '') .
                (isset($options['position_id']) && !empty($options['position_id']) ? ' AND npr.position_id = :position_id ' : '') .
                (isset($options['start_date']) && !empty($options['start_date']) ? ' AND news_published_date >= ' . $options['start_date'] : '') .
                (isset($options['end_date']) && !empty($options['end_date']) ? ' AND news_published_date <= ' . $options['end_date'] : '');
        $sql = $this->_conn->prepareSQL($sql, $options);
        $records = $this->_conn->fetchRow($sql);
        return isset($records['count']) ? $records['count'] : 0;
    }

    public function insertNews($item){
        $this->_conn->connect();
        $sql    = 'INSERT INTO news (user_id' .
            (isset($item['modified_user'])          ? ',modified_user' : '') .
            (isset($item['approved_user']) 	        ? ',approved_user' : '') .
            (isset($item['news_title'])             ? ',news_title' : '') .
            (isset($item['news_sapo'])  	        ? ',news_sapo' : '') .
            (isset($item['news_relation'])          ? ',news_relation' : '') .
            (isset($item['news_content'])  	        ? ',news_content' : '') .
            (isset($item['news_published_date'])    ? ',news_published_date' : '') .
            (isset($item['news_modified_date'])     ? ',news_modified_date' : '') .
            (isset($item['category_id'])            ? ',category_id' : '') .
            (isset($item['news_image']) 	        ? ',news_image' : '') .
            (isset($item['news_status']) 	        ? ',news_status' : '') .
            (isset($item['is_video'])               ? ',is_video' : '') .
            (isset($item['news_redirect_link'])     ? ',news_redirect_link' : '') .
            (isset($item['news_slug'])              ? ',news_slug' : '') .
            (isset($item['news_tag_slug'])          ? ',news_tag_slug' : '') .
            (isset($item['news_tag'])               ? ',news_tag' : '') .
            (isset($item['news_seo_title'])         ? ',news_seo_title' : '') .
            (isset($item['news_seo_description'])   ? ',news_seo_description' : '') .
            (isset($item['news_seo_keyword'])       ? ',news_seo_keyword' : '') .
            (isset($item['source_name'])            ? ',source_name' : '') .
            ') VALUES (:user_id' .
            (isset($item['modified_user'])          ? ',:modified_user' : '') .
            (isset($item['approved_user']) 	        ? ',:approved_user' : '') .
            (isset($item['news_title'])             ? ',:news_title' : '') .
            (isset($item['news_sapo'])  	        ? ',:news_sapo' : '') .
            (isset($item['news_relation'])          ? ',:news_relation' : '') .
            (isset($item['news_content'])  	        ? ',:news_content' : '') .
            (isset($item['news_published_date'])    ? ',:news_published_date' : '') .
            (isset($item['news_modified_date'])     ? ',:news_modified_date' : '') .
            (isset($item['category_id'])            ? ',:category_id' : '') .
            (isset($item['news_image']) 	        ? ',:news_image' : '') .
            (isset($item['news_status']) 	        ? ',:news_status' : '') .
            (isset($item['is_video'])               ? ',:is_video' : '') .
            (isset($item['news_redirect_link'])     ? ',:news_redirect_link' : '') .
            (isset($item['news_slug'])              ? ',:news_slug' : '') .
            (isset($item['news_tag_slug'])          ? ',:news_tag_slug' : '') .
            (isset($item['news_tag'])               ? ',:news_tag' : '') .
            (isset($item['news_seo_title'])         ? ',:news_seo_title' : '') .
            (isset($item['news_seo_description'])   ? ',:news_seo_description' : '') .
            (isset($item['news_seo_keyword'])       ? ',:news_seo_keyword' : '') .
            (isset($item['source_name'])            ? ',:source_name' : '') .
            ')';
        $sql 	= $this->_conn->prepareSQL($sql, $item);
        $this->_conn->execute($sql);
        return $this->_conn->getLastInsertedId();
    }

    public function updateNews($item){
        $this->_conn->connect();
        $sql = 'UPDATE news SET news_id = :news_id' .
            (isset($item['modified_user']) && !empty($item['modified_user']) 	            ? ',modified_user 	    = :modified_user' : '') .
            (isset($item['approved_user']) && !empty($item['approved_user']) 	            ? ',approved_user		= :approved_user' : '') .
            (isset($item['news_title']) && !empty($item['news_title']) 	                    ? ',news_title 			= :news_title' : '') .
            (isset($item['news_sapo'])  	                                                ? ',news_sapo 			= :news_sapo' : '') .
            (isset($item['news_relation'])                                   	            ? ',news_relation 		= :news_relation' : '') .
            (isset($item['news_content'])  	                                                ? ',news_content 		= :news_content' : '') .
            (isset($item['news_published_date']) && !empty($item['news_published_date']) 	? ',news_published_date = :news_published_date' : '') .
            (isset($item['news_modified_date']) && !empty($item['news_modified_date']) 	    ? ',news_modified_date 	= :news_modified_date' : '') .
            (isset($item['category_id'])                                                    ? ',category_id 	    = :category_id' : '') .
            (isset($item['news_image']) && !empty($item['news_image']) 	                    ? ',news_image 	        = :news_image' : '') .
            (isset($item['news_status']) 	                                                ? ',news_status	        = :news_status' : '') .
            (isset($item['is_video']) 	                                                    ? ',is_video 			= :is_video' : '') .
            (isset($item['news_redirect_link'])                                             ? ',news_redirect_link  = :news_redirect_link' : '') .
            (isset($item['news_slug'])                                                      ? ',news_slug		    = :news_slug' : '') .
            (isset($item['news_tag_slug'])                                                  ? ',news_tag_slug		= :news_tag_slug' : '') .
            (isset($item['news_tag'])                                                       ? ',news_tag 			= :news_tag' : '') .
            (isset($item['news_seo_title'])                                                 ? ',news_seo_title 		= :news_seo_title' : '') .
            (isset($item['news_seo_description'])                                           ? ',news_seo_description= :news_seo_description' : '') .
            (isset($item['news_seo_keyword'])                                   	        ? ',news_seo_keyword 	= :news_seo_keyword' : '') .
            (isset($item['source_name'])                                   	                ? ',source_name 	= :source_name' : '') .
            (isset($item['origin_link'])                                   	                ? ',origin_link 	= :origin_link' : '') .
            ' WHERE news_id = :news_id';
        $sql = $this->_conn->prepareSQL($sql, $item);

        $this->_conn->execute($sql, true);
    }

    public function setNewsStatus($newsId, $status) {

        if ($status == 1) {
            $row = $this->_conn->fetchRow($this->_conn->prepareSQL('select news_published_date from news where news_id = :id', array('id' => $newsId)));
            $updateTime = isset($row['news_published_date']) && $row['news_published_date'] > time() ? false : true;
        } else
            $updateTime = false;

        $sql = 'UPDATE news set news_status = :status ' .
                ($updateTime ? ', news_published_date = ' . (time() - 120) : '') . '
				WHERE news_id IN (' . $newsId . ')';

        $sql = $this->_conn->prepareSQL($sql, array('status' => $status));
        $this->_conn->execute($sql, true);
        return $this->_conn->getAffectedRows() > 0;
    }

    public function CheckTitleExists($options = array()) {
        $sql = 'SELECT COUNT(*) AS count FROM news WHERE news_title = :news_title AND news_id != :news_id';
        $sql = $this->_conn->prepareSQL($sql, $options);
        $record = $this->_conn->fetchRow($sql);
        if (!empty($record) && $record['count'] > 0)
            return false;
        else
            return true;
    }
}

?>