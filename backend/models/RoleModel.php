<?php
class RoleModel extends Model{
	public function getRoleById($id, $status = null)
	{
		$sql 	= 'select * from role where role_id = :id'.
				($status === null ? '' : ' and role_status = :status ').' limit 1';
		$sql 	= $this->_conn->prepareSQL($sql, array('id' 		=> $id,
				'status' 	=> $status));
		return  $this->_conn->fetchRow($sql);
	}
	
	public function getRoles($status = null, $offset = 0, $limit = 10)
	{
		$sql = 'select * from role where 1=1
		'. ($status === null ? '' : ' and role_status = '. $status) .'
		order by role_id limit '. $offset. ','. $limit;
		$records = $this->_conn->fetchAll($sql);
		return $records;
	}
	
	
	public function addRole($item)
	{
		$sql = 'insert into role  	(role_name,
									role_description,
									role_status)
									values (:name,
									:description,
									:status)';
	
		$sql = $this->_conn->prepareSQL($sql, $item);
		$this->_conn->execute($sql, true);
	
		return $this->_conn->getLastInsertedId();
	}
	
	public function updateRole($item)
	{
		$sql = 'update role set role_name 			= :name,
								role_description 	= :description,
								role_status 		= :status
								where role_id 		= :id';
	
		$sql = $this->_conn->prepareSQL($sql, $item);
		$this->_conn->execute($sql, true);
		return $this->_conn->getAffectedRows() > 0;
	}
}
?>