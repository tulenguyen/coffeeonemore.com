<?php

class ProductModel extends Model
{
    public function getListProductType($options, $pageIndex = 1, $pageSize = 10) {
        if (isset($options['title_like']) && !empty($options['title_like'])){
            $options['title_like'] = '%' . $options['title_like'] . '%';
        }
        $sql = 'SELECT ' .
            (isset($options['field']) 		                                        ? $options['field'] : '*') .
            ' FROM product_type WHERE 1=1 '.
            (isset($options['name_like']) && !empty($options['name_like'])          ? ' AND name LIKE :name_like ' : '') .
            (isset($options['id']) 	                                                ? ' AND id = :id ' : '') .
            (isset($options['parent_id']) 	                                        ? ' AND parent_id = :parent_id ' : '') .
            (isset($options['user_id']) && !empty($options['user_id'])	            ? ' AND user_id = :user_id ' : '') .
            (isset($options['modified_user']) && !empty($options['modified_user'])  ? ' AND modified_user = :modified_user ' : '') .
            (isset($options['status'])	                                            ? ' AND status = :status ' : '') .
            ' ORDER BY ' .
            (isset($options['order_by'])        ? $options['order_by'] : ' id ') .
            ($pageIndex > 0 ? ' LIMIT ' . (($pageIndex - 1) * $pageSize). ','. $pageSize : '');
        $sql 		= $this->_conn->prepareSQL($sql, $options);
        $result 	= $pageSize == 1 || isset($options['id']) ? $this->_conn->fetchRow($sql) : $this->_conn->fetchAll($sql) ;
        return $result;
    }

    public function insertProductType($item){
        $this->_conn->connect();
        $sql    = 'INSERT INTO product_type (name' .
            (isset($item['user_id'])            ? ',user_id' : '') .
            (isset($item['parent_id'])          ? ',parent_id' : '') .
            (isset($item['modified_user'])      ? ',modified_user' : '') .
            (isset($item['description'])        ? ',description' : '') .
            (isset($item['extra_description'])  ? ',extra_description' : '') .
            (isset($item['image'])              ? ',image' : '') .
            (isset($item['slug'])               ? ',slug' : '') .
            (isset($item['header_order'])       ? ',header_order' : '') .
            (isset($item['home_order'])         ? ',home_order' : '') .
            (isset($item['footer_order'])       ? ',footer_order' : '') .
            (isset($item['status'])             ? ',status' : '') .
            (isset($item['is_sale'])            ? ',is_sale' : '') .
            (isset($item['redirect_link'])      ? ',redirect_link' : '') .
            (isset($item['created_at'])         ? ',created_at' : '') .
            (isset($item['updated_at'])         ? ',updated_at' : '') .
            ') VALUES (:name' .
            (isset($item['user_id'])            ? ',:user_id' : '') .
            (isset($item['parent_id'])          ? ',:parent_id' : '') .
            (isset($item['modified_user'])      ? ',:modified_user' : '') .
            (isset($item['description'])        ? ',:description' : '') .
            (isset($item['extra_description'])  ? ',:extra_description' : '') .
            (isset($item['image'])              ? ',:image' : '') .
            (isset($item['slug'])               ? ',:slug' : '') .
            (isset($item['header_order'])       ? ',:header_order' : '') .
            (isset($item['home_order'])         ? ',:home_order' : '') .
            (isset($item['footer_order'])       ? ',:footer_order' : '') .
            (isset($item['status'])             ? ',:status' : '') .
            (isset($item['is_sale'])            ? ',:is_sale' : '') .
            (isset($item['redirect_link'])      ? ',:redirect_link' : '') .
            (isset($item['created_at'])         ? ',:created_at' : '') .
            (isset($item['updated_at'])         ? ',:updated_at' : '') .
            ')';
        $sql 	= $this->_conn->prepareSQL($sql, $item);
        $this->_conn->execute($sql);
        return $this->_conn->getLastInsertedId();
    }

    public function updateProductType($item) {
        $sql = 'UPDATE product_type SET id = :id' .
            (isset($item['name']) && !empty($item['name'])      ? ',name                = :name' : '') .
            (isset($item['slug']) && !empty($item['slug'])      ? ',slug                = :slug' : '') .
            (isset($item['user_id'])                            ? ',user_id             = :user_id' : '') .
            (isset($item['parent_id'])                          ? ',parent_id           = :parent_id' : '') .
            (isset($item['modified_user'])                      ? ',modified_user       = :modified_user' : '') .
            (isset($item['description'])                        ? ',description         = :description' : '') .
            (isset($item['extra_description'])                  ? ',extra_description   = :extra_description' : '') .
            (isset($item['image']) && !empty($item['image'])    ? ',image 		        = :image' : '') .
            (isset($item['header_order'])                       ? ',header_order 	    = :header_order' : '') .
            (isset($item['home_order'])                         ? ',home_order 	        = :home_order' : '') .
            (isset($item['footer_order'])                       ? ',footer_order 	    = :footer_order' : '') .
            (isset($item['status'])                             ? ',status 	            = :status' : '') .
            (isset($item['is_sale'])                            ? ',is_sale 	        = :is_sale' : '') .
            (isset($item['redirect_link'])                      ? ',redirect_link       = :redirect_link' : '') .
            (isset($item['created_at'])                         ? ',created_at 	        = :created_at' : '') .
            (isset($item['updated_at'])                         ? ',updated_at		    = :updated_at' : '') .
            ' WHERE id 	= :id';
        $sql = $this->_conn->prepareSQL($sql, $item);
        $this->_conn->execute($sql, true);
    }

    public function deleteProductType($id) {
        $listProduct = $this->getListProduct(['type_id' => $id]);
        if (!empty($listProduct)) {
            //append all product id to array to update
            $listId = [];
            foreach ($listProduct as $product) {
                $listId[] = $product['id'];
            }
        }
        $productType = $this->getListProductType(['id' => $id]);
        if (!empty($productType)) {
            if (!empty($listId)) {
                $sql = 'UPDATE product set type_id = :type_id WHERE id IN (' . implode(',', $listId) . ')';
                $sql = $this->_conn->prepareSQL($sql, ['type_id' => $productType['parent_id']]);
                $this->_conn->execute($sql, true);
            }

            $sql = 'DELETE FROM product_type WHERE id = :id';
            $sql = $this->_conn->prepareSQL($sql, ['id' => $id]);
            $this->_conn->execute($sql, true);
        }
    }

    public function getListProduct($options, $pageIndex = 1, $pageSize = 10) {
        if (isset($options['name_like']) && !empty($options['name_like'])){
            $options['name_like'] = '%' . $options['name_like'] . '%';
        }
        $sql = 'SELECT ' .
            (isset($options['field']) 		                                        ? $options['field'] : '*') .
            ' FROM product WHERE 1=1 '.
            (isset($options['name_like']) && !empty($options['name_like'])          ? ' AND name LIKE :name_like ' : '') .
            (isset($options['id']) 	                                                ? ' AND id = :id ' : '') .
            (isset($options['ids']) 	                                            ? ' AND id IN (' . $options['ids'] . ') ' : '') .
            (isset($options['code']) 	                                            ? ' AND code = :code ' : '') .
            (isset($options['user_id']) && !empty($options['user_id'])	            ? ' AND user_id = :user_id ' : '') .
            (isset($options['type_id']) && !empty($options['type_id'])	            ? ' AND type_id = :type_id ' : '') .
            (isset($options['modified_user']) && !empty($options['modified_user'])  ? ' AND modified_user = :modified_user ' : '') .
            (isset($options['status'])	                                            ? ' AND status = :status ' : ' ') .
            (isset($options['start_date']) && !empty($options['start_date'])        ? ' AND created_at >= ' . $options['start_date'] : '') .
            (isset($options['end_date']) && !empty($options['end_date'])            ? ' AND created_at <= ' . $options['end_date'] : '') .
            ' ORDER BY ' .
            (isset($options['order_by'])        ? $options['order_by'] : ' id DESC ') .
            ($pageIndex > 0 ? ' LIMIT ' . (($pageIndex - 1) * $pageSize). ','. $pageSize : '');
        $sql 		= $this->_conn->prepareSQL($sql, $options);
        $result 	= $pageSize == 1 || isset($options['id']) ? array($this->_conn->fetchRow($sql)) : $this->_conn->fetchAll($sql) ;
        if (!empty($result)) {
            foreach ($result as &$item) {
                if (isset($options['join_user'])) {
                    if (isset($item['user_id']) && !empty($item['user_id'])) {
                        $sql = 'SELECT user_name AS create_name FROM user WHERE user_id = ' . $item['user_id'];
                        $record = $this->_conn->fetchRow($sql);
                        $item['create_name'] = isset($record['create_name']) ? $record['create_name'] : '';
                    }
                    if (isset($item['modified_user']) && !empty($item['modified_user'])) {
                        $sql = 'SELECT user_name AS modified_name FROM user WHERE user_id = ' . $item['modified_user'];
                        $record = $this->_conn->fetchRow($sql);
                        $item['modified_name'] = isset($record['modified_name']) ? $record['modified_name'] : '';
                    }
                }

                if (isset($options['join_type'])) {
                    if (isset($item['type_id']) && !empty($item['type_id'])) {
                        $sql = 'SELECT name AS product_type_name FROM product_type WHERE id = ' . $item['type_id'];
                        $record = $this->_conn->fetchRow($sql);
                        $item['product_type_name'] = isset($record['product_type_name']) ? $record['product_type_name'] : '';
                    }
                }
            }
            $result = $pageSize == 1 || isset($options['id']) ? $result[0] : $result;
        }
        return $result;
    }

    public function insertProduct($item){
        $this->_conn->connect();
        $sql    = 'INSERT INTO product (name' .
            (isset($item['user_id'])            ? ',user_id' : '') .
            (isset($item['modified_user'])      ? ',modified_user' : '') .
            (isset($item['short_description'])  ? ',short_description' : '') .
            (isset($item['code'])               ? ',code' : '') .
            (isset($item['description'])        ? ',description' : '') .
            (isset($item['image']) 	            ? ',image' : '') .
            (isset($item['slug'])               ? ',slug' : '') .
            (isset($item['old_price'])  	    ? ',old_price' : '') .
            (isset($item['price'])  	        ? ',price' : '') .
            (isset($item['material'])  	        ? ',material' : '') .
            (isset($item['size'])  	            ? ',size' : '') .
            (isset($item['source'])  	        ? ',source' : '') .
            (isset($item['support'])  	        ? ',support' : '') .
            (isset($item['type_id'])  	        ? ',type_id' : '') .
            (isset($item['status'])             ? ',status' : '') .
            (isset($item['news_relation'])      ? ',news_relation' : '') .
            (isset($item['product_relation'])   ? ',product_relation' : '') .
            (isset($item['created_at'])  	    ? ',created_at' : '') .
            (isset($item['updated_at'])         ? ',updated_at' : '') .
            ') VALUES (:name' .
            (isset($item['user_id'])            ? ',:user_id' : '') .
            (isset($item['modified_user'])      ? ',:modified_user' : '') .
            (isset($item['short_description'])  ? ',:short_description' : '') .
            (isset($item['code'])               ? ',:code' : '') .
            (isset($item['description'])        ? ',:description' : '') .
            (isset($item['image']) 	            ? ',:image' : '') .
            (isset($item['slug'])               ? ',:slug' : '') .
            (isset($item['old_price'])  	    ? ',:old_price' : '') .
            (isset($item['price'])  	        ? ',:price' : '') .
            (isset($item['material'])  	        ? ',:material' : '') .
            (isset($item['size'])  	            ? ',:size' : '') .
            (isset($item['source'])  	        ? ',:source' : '') .
            (isset($item['support'])  	        ? ',:support' : '') .
            (isset($item['type_id'])  	        ? ',:type_id' : '') .
            (isset($item['status'])             ? ',:status' : '') .
            (isset($item['news_relation'])      ? ',:news_relation' : '') .
            (isset($item['product_relation'])   ? ',:product_relation' : '') .
            (isset($item['created_at'])  	    ? ',:created_at' : '') .
            (isset($item['updated_at'])         ? ',:updated_at' : '') .
            ')';
        $sql 	= $this->_conn->prepareSQL($sql, $item);
        $this->_conn->execute($sql);
        return $this->_conn->getLastInsertedId();
    }

    public function updateProduct($item) {
        $sql = 'UPDATE product SET id = :id' .
            (isset($item['name']) && !empty($item['name'])      ? ',name                = :name' : '') .
            (isset($item['slug']) && !empty($item['slug'])      ? ',slug                = :slug' : '') .
            (isset($item['user_id'])                            ? ',user_id             = :user_id' : '') .
            (isset($item['modified_user'])                      ? ',modified_user       = :modified_user' : '') .
            (isset($item['short_description'])                  ? ',short_description   = :short_description' : '') .
            (isset($item['code'])                               ? ',code                = :code' : '') .
            (isset($item['description'])                        ? ',description         = :description' : '') .
            (isset($item['image']) && !empty($item['image'])    ? ',image 		        = :image' : '') .
            (isset($item['old_price'])                          ? ',old_price 	        = :old_price' : '') .
            (isset($item['price'])                              ? ',price 	            = :price' : '') .
            (isset($item['material'])                           ? ',material 	        = :material' : '') .
            (isset($item['size'])                               ? ',size 	            = :size' : '') .
            (isset($item['source'])                             ? ',source 	            = :source' : '') .
            (isset($item['support'])                            ? ',support 	        = :support' : '') .
            (isset($item['type_id'])                            ? ',type_id 	        = :type_id' : '') .
            (isset($item['status'])                             ? ',status 	            = :status' : '') .
            (isset($item['news_relation'])                      ? ',news_relation 		= :news_relation' : '') .
            (isset($item['product_relation'])                   ? ',product_relation 	= :product_relation' : '') .
            (isset($item['created_at'])                         ? ',created_at 	        = :created_at' : '') .
            (isset($item['updated_at'])                         ? ',updated_at		    = :updated_at' : '') .
            ' WHERE id 	= :id';
        $sql = $this->_conn->prepareSQL($sql, $item);
        $this->_conn->execute($sql, true);
    }

    public function checkNameExists($options = []){
        $sql = 'SELECT COUNT(*) AS count FROM product WHERE name = :name ' .
            (isset($options['id']) && !empty($options['id'])  ? ' AND id != :id ' : '');
        $sql = $this->_conn->prepareSQL($sql, $options);
        $record = $this->_conn->fetchRow($sql);
        if (!empty($record) && $record['count'] > 0)
            return false;
        else
            return true;
    }

    private function __getChildIds($id, &$childIds = [])
    {
        if ($id > 0){
            if ($childs = $this->getListProductType(['parent_id' => $id])){
                foreach ($childs as $child){
                    $childIds[] = $child['id'];
                    $this->__getChildIds($child['id'], $childIds);
                }
            }
        }
    }

    public function getChildIds($id)
    {
        $this->__getChildIds($id, $listIds);

        if (!empty($listIds)){
            array_push($listIds, $id);
            $listIds = implode(',', $listIds);
        }else{
            $listIds = $id;
        }
        return $listIds;
    }

    public function treeView($parentId = 0, $selectedIds = array(), $href = '', &$return = '', $classActive = '' )
    {
        $items = $this->getListProductType([
            'parent_id'             => $parentId,
            'asc'                   => 1
        ], 0);
        if (!empty($items)){
            $return .= '<ul>';
            foreach ($items as $row){
                $selectedElement = in_array($row['id'], $selectedIds) ? ' class="'.$classActive.'"' : '';
                $checked = $row['status'] == 1 ? 'checked="checked"' : '';
                $return .= '<li '. $selectedElement .'>
								<input type="checkbox"' . $checked . '" />
								<a href="'. $href . '/' . $row['id'] .'" data="' . $row['id'] . '">'. $row['name'] .'</a>';

                $this->treeView($row['id'], $selectedIds, $href, $return, $classActive);


                $return .= '</li>';
            }
            $return .= '</ul>';
        }
    }

    public function treeOption($parentId = 0, $selectedIds = array(), &$return = '', $repeat = 0, $options = array())
    {
        ++ $repeat;
        $items = $this->getListProductType([
            'parent_id'             => $parentId,
            'asc'                   => 1
        ], 0);
        if (!empty($items)){
            foreach ($items as $row){
                $selectedElement = in_array($row['id'], $selectedIds) ? ' selected="selected"' : '';

                $return .=  '<option value="'. $row['id'] .'"' . (isset($row['parent_id']) && $row['parent_id'] == '0' ? ' class="highlight"' : '') . $selectedElement .'>'.
                                str_repeat('__', $repeat). $row['name'] .
                            '</option>';
                $this->treeOption($row['id'], $selectedIds, $return , $repeat, $options);
            }
        }
    }

    public function treeInput($parentId = 0, $selectedIds = array(), &$return = '', $repeat = 0){
        ++ $repeat;
        $this->getListProductType([
            'parent_id'             => $parentId,
            'asc'                   => 1
        ], 0);
        if (!empty($items)){
            foreach ($items as $row){
                $selectedElement = in_array($row['id'], $selectedIds) ? ' checked="checked"' : '';

                $return .= '<div class="div8">' .
                    str_repeat('&nbsp;&nbsp;&nbsp;&nbsp;', $repeat) .
                    '<input type="checkbox" name="checkboxChild" value="'. $row['id'] .'" '. $selectedElement . ' />'.
                    '<span>' . $row['name'] . '</span>
						    </div>';

                $this->treeInput($row['id'], $selectedIds, $return , $repeat);
            }
        }
    }
}