<?php
class ImageModel extends Model{
	public function GetListImages($options = array(), $pageIndex = 1, $pageSize = 30)
	{
		if (isset($options['title']) && !empty($options['title'])){
			$options['title'] = '%'. $options['title'] .'%';
		}
		$sql = 'SELECT image_id, image_title, image_src, image_size, image_posted_date, user_id from image WHERE 1=1 '.
				(isset($options['user_id'])  ? ' AND user_id = :user_id ' : '').
				(isset($options['title']) && !empty($options['title'])  ? ' AND image_title like :title' : '').
				'ORDER BY ' .
                (isset($options['order_by']) ? $options['order_by'] : ' image_id ').
                (isset($options['asc']) ? $options['asc'] : 'DESC ').
				'LIMIT '. (($pageIndex - 1) * $pageSize)  .','. $pageSize;
		$sql = $this->_conn->prepareSQL($sql, $options);
		return $this->_conn->fetchAll($sql);
	}

    public function InsertImage($item)
    {
        $sql = 'SELECT image_id FROM image WHERE image_src = :image_src';
        $sql = $this->_conn->prepareSQL($sql, $item);
        $result = $this->_conn->fetchRow($sql);
        if (isset($result['image_id']) && !empty($result['image_id'])){
            $sql = 'UPDATE image SET image_posted_date = ' . strtotime(date('Y-m-d H:i:s')) . ' WHERE image_id = ' . $result['image_id'];
            $this->_conn->execute($sql);
            return $result['image_id'];
        } else {
            $sql = 'INSERT INTO image  (user_id,'.
                (isset($item['folder_id']) ? 'folder_id,' : '') .'
										image_title,
										image_src,
										image_size,
										image_status,
										image_posted_date)
								VALUES (:user_id,'.
                (isset($item['folder_id']) ? ':folder_id,' : '') .'
										:image_title,
										:image_src,
										:image_size,
										:image_status,
										:image_posted_date)';
            $this->_conn->connect();
            $sql = $this->_conn->prepareSQL($sql, $item);
            $this->_conn->execute($sql, true);
            return $this->_conn->getLastInsertedId();
        }
    }
}
?>