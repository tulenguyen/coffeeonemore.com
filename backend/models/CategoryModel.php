<?php

class CategoryModel extends Model {

    public function getListCategory($options = array(), $pageIndex = 1, $pageSize = 10) {
        //Tu quyen Chu muc role 4 tro xuong, chi duoc vao chuyen muc minh phu trach
        $categoryIds = '';
        if (isset($options['role_id']) && $options['role_id'] >= 4) {
            //$categoryIds = isset($options['category_ids']) && !empty($options['category_ids']) ? $options['category_ids'] : '';
        }

        $sql = 'SELECT ' .
                (isset($options['fields']) ? $options['fields'] : '*') .
                ' FROM category WHERE 1=1 ' .
                (isset($options['category_cms_order']) && !empty($options['category_cms_order']) ? ' AND category_cms_order > 0 ' : '') .
                (isset($options['parent_id']) ? ' AND parent_id = :parent_id ' : '') .
                (isset($options['category_id']) ? ' AND category_id = :category_id ' : '') .
                (isset($options['user_id']) ? ' AND user_id = :user_id ' : '') .
                (isset($options['category_status']) ? ' AND category_status = :category_status ' : '') .
                (isset($categoryIds) && !empty($categoryIds) ? ' AND category_id in (' . $categoryIds . ') OR parent_id in (' . $categoryIds . ') ' : '') .
                ' ORDER BY ' .
                (isset($options['order_by']) ? $options['order_by'] : ' category_id ') .
                ($pageIndex > 0 ? ' LIMIT ' . (($pageIndex - 1) * $pageSize) . ',' . $pageSize : '');
        $sql = $this->_conn->prepareSQL($sql, $options);
        $records = $pageSize == 1 || isset($options['category_id']) ? $this->_conn->fetchRow($sql) : $this->_conn->fetchAll($sql);
        return $records;
    }

    public function InsertCategory($item) {
        if (!empty($item['category_image']))
            $item['category_image'] = Helper::getInstance()->getConfig('image_url') . $item['category_image'];
        $sql = 'insert into category (user_id, category_name, category_slug, category_title, category_keyword, category_description, category_image, category_status, category_template, category_header_order, category_home_order, category_footer_order, category_cms_order, parent_id, is_outside, category_redirect_link) ' .
                'values (:user_id, :category_name, :category_slug, :category_title, :category_keyword, :category_description, :category_image, :category_status, :category_template, :category_header_order, :category_home_order, :category_footer_order, :category_cms_order, :parent_id, :is_outside, :category_redirect_link)';
        $sql = $this->_conn->prepareSQL($sql, $item);
        $this->_conn->execute($sql);
        return $this->_conn->getLastInsertedId();
    }

    public function UpdateCategory($item) {

        if (!empty($item['category_image']))
            $item['category_image'] = Helper::getInstance()->getConfig('image_url') . $item['category_image'];
        $sql = 'update category set category_id = :category_id' .
                (isset($item['user_id']) && !empty($item['user_id']) ? ',user_id           = :user_id' : '') .
                (isset($item['category_name']) && !empty($item['category_name']) ? ',category_name           = :category_name' : '') .
                (isset($item['category_slug']) && !empty($item['category_slug']) ? ',category_slug           = :category_slug' : '') .
                (isset($item['category_title']) && !empty($item['category_title']) ? ',category_title 	        = :category_title' : '') .
                (isset($item['category_keyword']) && !empty($item['category_keyword']) ? ',category_keyword 	    = :category_keyword' : '') .
                (isset($item['category_description']) && !empty($item['category_description']) ? ',category_description 	= :category_description' : '') .
                (isset($item['category_image']) && !empty($item['category_image']) ? ',category_image          = :category_image' : '') .
                (isset($item['category_status'])  ? ',category_status          = :category_status' : '') .
                (isset($item['category_template']) ? ',category_template		= :category_template' : '') .
                (isset($item['category_header_order']) ? ',category_header_order 	= :category_header_order' : '') .
                (isset($item['category_home_order']) ? ',category_home_order 	= :category_home_order' : '') .
                (isset($item['category_footer_order']) ? ',category_footer_order 	= :category_footer_order' : '') .
                (isset($item['category_cms_order']) ? ',category_cms_order 	= :category_cms_order' : '') .
                (isset($item['parent_id']) ? ',parent_id		 		= :parent_id' : '') .
                ' where category_id 	= :category_id';

        $sql = $this->_conn->prepareSQL($sql, $item);
        $this->_conn->execute($sql, true);
    }

    private function __getChildIds($id, &$childIds = array()) {
        if ($id > 0) {
            if ($childs = $this->getListCategory(array('parent_id' => $id, 'category_status' => 1))) {
                foreach ($childs as $child) {
                    $childIds[] = $child['category_id'];
                    $this->__getChildIds($child['category_id'], $childIds);
                }
            }
        }
    }

    public function getChildIds($id) {
        $this->__getChildIds($id, $listIds);

        if (!empty($listIds)) {
            array_push($listIds, $id);
            $listIds = implode(',', $listIds);
        } else {
            $listIds = $id;
        }
        return $listIds;
    }

    public function UpdateChuMuc($catId, $userId) {
        $sql = 'update category set user_id = :userId where category_id = :catId';
        $sql = $this->_conn->prepareSQL($sql, array('userId' => $userId, 'catId' => $catId));
        $this->_conn->execute($sql, true);
        return $this->_conn->getAffectedRows() > 0;
    }

    public function InsertCategoryNews($item = array()) {
        $sql = 'delete from category_news_rel where news_id = ' . $item['news_id'];
        $this->_conn->execute($sql, true);
        $sql = 'insert into category_news_rel(news_id, category_id) ' .
                'values (' . $item['news_id'] . ',' . implode('),(' . $item['news_id'] . ',', explode(',', $item['category_ids'])) . ')';
        $this->_conn->execute($sql, true);
    }

    //Get list cac cat va cat con thuoc phan quyen cua user
    public function GetListRuleCat($categoryIds) {
        $sql = 'SELECT GROUP_CONCAT(category_id) as ListCats FROM category WHERE category_id in (' . $categoryIds . ') OR parent_id in (' . $categoryIds . ')';
        return $this->_conn->fetchRow($sql);
    }

    public function treeView($parentId = 0, $selectedIds = array(), $href = '', &$return = '', $classActive = '') {
        $items = $this->getListCategory(array('parent_id' => $parentId,
            'category_cms_order' => 0,
            'order_by' => 'category_cms_order',
            'asc' => 1), 0);
        if (!empty($items)) {
            $return .= '<ul>';
            foreach ($items as $row) {
                $selectedElement = in_array($row['category_id'], $selectedIds) ? ' class="' . $classActive . '"' : '';
                $checked = $row['category_status'] == 1 ? 'checked="checked"' : '';
                $return .= '<li ' . $selectedElement . '>
								<input type="checkbox"' . $checked . ' onclick="UpdateCategoryStatus(this);" />
								<a href="' . $href . '/' . $row['category_id'] . '" data="' . $row['category_id'] . '">' . $row['category_name'] . '</a>';

                $this->treeView($row['category_id'], $selectedIds, $href, $return, $classActive);


                $return .= '</li>';
            }
            $return .= '</ul>';
        }
    }

    public function treeOption($parentId = 0, $selectedIds = array(), &$return = '', $repeat = 0, $options = array()) {
        ++$repeat;
        $items = isset($options['category_status']) ? $this->getListCategory(array('parent_id' => $parentId,
                    'category_cms_order' => 1,
                    'category_status' => $options['category_status'],
                    'order_by' => 'category_cms_order',
                    'asc' => 1), 0) :
                $this->getListCategory(array('parent_id' => $parentId,
                    'category_cms_order' => 1,
                    'order_by' => 'category_cms_order',
                    'asc' => 1), 0);
        if (!empty($items)) {
            foreach ($items as $row) {
                $selectedElement = in_array($row['category_id'], $selectedIds) ? ' selected="selected"' : '';

                $return .= '<option value="' . $row['category_id'] . '"' . (isset($row['parent_id']) && $row['parent_id'] == '0' ? ' class="highlight"' : '') . $selectedElement . '>' .
                        str_repeat('__', $repeat) . $row['category_name'] .
                        '</option>';
                $this->treeOption($row['category_id'], $selectedIds, $return, $repeat, $options);
            }
        }
    }

    public function treeInput($parentId = 0, $selectedIds = array(), &$return = '', $repeat = 0) {
        ++$repeat;
        $items = $this->getListCategory(array('parent_id' => $parentId,
            'category_cms_order' => 1,
            'order_by' => 'category_cms_order',
            'asc' => 1), 0);
        if (!empty($items)) {
            foreach ($items as $row) {
                $selectedElement = in_array($row['category_id'], $selectedIds) ? ' checked="checked"' : '';

                $return .= '<div class="div8">' .
                        str_repeat('&nbsp;&nbsp;&nbsp;&nbsp;', $repeat) .
                        '<input type="checkbox" name="checkboxChild" value="' . $row['category_id'] . '" ' . $selectedElement . ' />' .
                        '<span>' . $row['category_name'] . '</span>
						    </div>';

                $this->treeInput($row['category_id'], $selectedIds, $return, $repeat);
            }
        }
    }

}

?>