<?php
class PositionModel extends Model {
	public function getListPosition($options = array(), $pageIndex = 1, $pageSize = 10){
		$sql = 'select ' .
				(isset($option['fields']) 		? $option['fields'] : '*').
				' from news_position where 1=1 ' .
				(isset($options['role_id']) && $options['role_id'] >= 4	? ' and position_id >= 5' : '') . //Tu quyen chu muc khong duoc set vi tri trang chu
				(isset($options['position_id'])  	? ' and position_id = :position_id' : '') .
				(isset($options['position_active']) ? ' and position_active = :position_active' : '') .
				' order by position_id ' .
				($pageIndex > 0 ? ' limit ' . (($pageIndex - 1) * $pageSize). ','. $pageSize : '');

		$sql = $this->_conn->prepareSQL($sql, $options);
		$records 	= ($pageSize < 2 || isset($options['position_id']) && $options['position_id'] > 0) ? $this->_conn->fetchRow($sql) : $this->_conn->fetchAll($sql) ;
		return $records;
	}

	public function InsertPosition($item){
		$sql = 'insert into news_position (position_name, user_id, position_description, position_active) ' .
				                'values (:position_name, :user_id, :position_description, :position_active)';
		$sql = $this->_conn->prepareSQL($sql, $item);
		$this->_conn->execute($sql);
		return $this->_conn->getLastInsertedId();
	}

	public function UpdatePosition($item){
		$sql = 'update news_position set position_name 			= :position_name,' .
										'position_description 	= :position_description,' .
										'position_active 		= :position_active' .
										' where position_id 	= :position_id';

		$sql = $this->_conn->prepareSQL($sql, $item);
		$this->_conn->execute($sql, true);
	}
	
	public function GetNewsPosition($item = array()){
		$sql = 	'select np.news_id, np.position_id, n.news_title, n.category_id, n.news_published_date from news_position_rel as np ' .
				'inner join news as n on n.news_id = np.news_id ' .
				'inner join category as c on n.category_id = c.category_id ' .  
				'where np.position_id = ' . $item['position_id'] . ' order by news_published_date desc';
		$record = $this->_conn->fetchAll($sql);
		return $record;
	}

	//Update thread vao truong thread_ids trong bang news, them quan he vao bang news_thread_rel
    public function InsertNewsPosition($item = array()){
        if (isset($item['featureId'])){
            $sql = 'DELETE FROM news_position_rel WHERE position_id = ' . $item['featureId'] . ' AND news_id IN (' . trim($item['listNewsId'] . ',' . $item['listNewsDelete'], ',') . ')';
            $this->_conn->execute($sql, true);

            $sql = 'INSERT INTO news_position_rel (position_id, news_id) ' .
                'VALUES (' . $item['featureId'] . ',' .  implode('),('. $item['featureId'].',', explode(',', $item['listNewsId'])) .')';
            $this->_conn->execute($sql, true);
        } else if (isset($item['position_id']) && isset($item['news_id'])){
            $sql = 'DELETE FROM news_position_rel where news_id = ' . $item['news_id'];
            $this->_conn->execute($sql, true);
            if ($item['position_id'] > 0){
                $sql = 'INSERT INTO news_position_rel (position_id, news_id) VALUES (:position_id, :news_id)';
                $sql = $this->_conn->prepareSQL($sql, $item);
                $this->_conn->execute($sql, true);
            }
        }
        return true;
    }
}
?>