<?php
class Model {
	protected $_cache;
	protected $_conn;
	
	public function __construct($conn = null, $cache = null)
	{
		$this->_conn 	= $conn  === null ? Helper::getInstance()->getDbConnection() 		: $conn;
  		//$this->_cache	= $cache === null ? Helper::getInstance()->getMemcachedConnection() : $cache;
	}
	
	public function setConnection($conn)
	{
		$this->_conn = $conn;
	}
	
	public function setCache($cache)
	{
 		$this->_cache = $cache;
	}
	
	public function getCacheData($key)
	{
 		//return $this->_cache->get($key);
	}
	
	public function setCacheData($key, $data, $ttl = 300)
	{
		//return $this->_cache->set($key, $data, $ttl);
	}
	
	public function __destruct()
	{
		if (is_resource($this->_conn)){
			$this->_conn->close();
			unset($this->_conn);
		}
	}
}