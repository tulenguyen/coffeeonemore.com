<?php
class HistoryModel extends Model{
	public function InsertHistory($options = array()){
		$sql = 'insert into history (history_name, history_time, route_id, object_id, object_name, user_id) ' .
							'values (:history_name, :history_time, (select route_id from route where route_pattern = :route_id), :object_id, :object_name, :user_id)';
		$sql = $this->_conn->prepareSQL($sql, $options);
		$this->_conn->execute($sql);
	}
	
	public function GetListHistory($options = array(), $pageIndex = 1, $pageSize = 10) {
		$key = __FUNCTION__. count($options). '_'. implode('_', $options). $pageIndex. $pageSize;
		if ($result = $this->getCacheData($key)){
			return $result;
		}
		
		$sql =  'select ' .
				(isset($options['join_route'])		? 'r.route_pattern, ' : '') .
				(isset($options['join_user'])		? 'u.user_name, u.role_id, ro.role_name,' : '') .
				(isset($options['field']) 			? $options['field'] : 'h.* ') . ' from history as h ' .
				(isset($options['join_route'])		? 'inner join route as r on h.route_id = r.route_id ' : '') .
				(isset($options['join_user'])		? 'inner join user as u on h.user_id = u.user_id inner join role as ro on ro.role_id = u.role_id ' : '') .
				'where 1=1 ' .
				(isset($options['filter']) && $options['filter'] != ''	? ' and h.history_name = :filter' : '') .
				(isset($options['user_id']) 		? ' and h.user_id = :user_id' : '') .
				' order by h.history_id desc '.
				($pageIndex > 0 ? ' limit ' . (($pageIndex - 1) * $pageSize). ','. $pageSize : '');
		
		$sql 		= $this->_conn->prepareSQL($sql, $options);
		$records 	= $pageSize < 2 || (isset($options['history_id']) && $options['history_id'] > 0) ? $this->_conn->fetchRow($sql) : $this->_conn->fetchAll($sql);
		$this->setCacheData($key, $records);
		return $records;
	}
	
}
?>