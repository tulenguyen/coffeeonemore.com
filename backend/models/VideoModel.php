<?php
class VideoModel extends Model
{
	public function getVideoById($id)
	{
		$sql = 'select * from video where video_id = :id limit 1';
		$sql = $this->_conn->prepareSQL($sql, array('id' => $id));
		return  $this->_conn->fetchRow($sql);
	}
	
	public function getVideos($options = array(), $pageIndex = 1, $pageSize = 30)
	{
		$sql = 	'select ' .
				(isset($options['join_category'])  	? 'c.category_name, ' : '') .
				(isset($options['join_site'])		? 's.site_domain, ' : '') .
				(isset($options['join_user']) 		? 'u.user_fullname as create_name, ' : '') .
				(isset($options['field']) 			? $options['field'] : 'v.* ') . ' from video as v ' .
				(isset($options['join_category'])  	? 'left join category as c on v.category_id = c.category_id ' : '') .
				(isset($options['join_site']) 		? 'left join site as s on v.video_source = s.site_id ' : '') .
				(isset($options['join_user'])		? 'left join user as u on v.user_id = u.user_id ' : '') .
				'where 1=1 '.
				(isset($options['ids'])  			? ' and video_id in('. $options['ids'] .') ' : '').
				(isset($options['source'])  		? ' and video_source = :source ' : '').
				(isset($options['title_like']) && !empty($options['title_like'])	? ' and video_name like :title_like ' : '') .
				(isset($options['site_id']) && !empty($options['site_id'])  		? ' and n.site_id = :site_id ' : '') .
				(isset($options['category_id']) && !empty($options['category_id']) && $options['category_id'] > 0			? ' and (v.category_id = :category_id or c.parent_id = :category_id) ' : '') .
				(isset($options['start_date']) && !empty($options['start_date']) && $options['start_date'] != 'Chọn ngày'	? ' and v.video_posted_date >= ' . $options['start_date'] : '') .
				(isset($options['end_date']) && !empty($options['end_date']) && $options['end_date'] != 'Chọn ngày'			? ' and v.video_posted_date <= ' . $options['end_date'] : '') .
				'order by v.video_posted_date desc' . 
				($pageIndex > 0 ? ' limit ' . (($pageIndex - 1) * $pageSize). ','. $pageSize : '');
		
		$sql = $this->_conn->prepareSQL($sql, $options);
		return $this->_conn->fetchAll($sql);
	}
	
	public function addVideo($item)
	{
		if (!empty($item['avatar'])) 
			$item['avatar'] = Helper::getInstance()->getConfig('image_url') . $item['avatar'];
		$sql = 'insert into video  (video_name,
						'. (isset($item['folder_id']) ? 'folder_id,' : '') .' 
									video_src, 
									video_description,
									video_type,
									video_avatar,
									video_size,
									video_source,
									category_id,
									user_id,
									video_posted_date,
									video_focus,
									video_active)
							values (:name, 
							'. (isset($item['folder_id']) ? ':folder_id,' : '') .' 
									:src, 
									:description, 
									:type,
									:avatar,
									:size,
									:source,
									:category_id,
									:user_id,
									:video_posted_date,
									:video_focus,
									:active)';
		
		$sql = $this->_conn->prepareSQL($sql, $item);
		$this->_conn->execute($sql, true);
	    return $this->_conn->getLastInsertedId();
	}
	
	public function deleteVideo($src){
		$sql = 'delete from video where video_src = :src';
		$sql =  $this->_conn->prepareSQL($sql, array('src' => $src));
		$this->_conn->execute($sql, true); 
		@unlink($src);
		return $this->_conn->getAffectedRows() > 0;
	}
	
	public function updateVideo($item) {
		if (!empty($item['avatar'])) $item['avatar'] = Helper::getInstance()->getConfig('image_url') . $item['avatar'];
		$sql = 'update video set 	video_name 			= :name,
									'. (!empty($item['src']) ? 'video_src = :src,' : '') .'
									video_description 	= :description,
									'. (!empty($item['type']) ? 'video_type = :type,' : ''). ' 
									'. (!empty($item['avatar']) ? 'video_avatar	= :avatar,' : ''). ' 
									'. (!empty($item['size']) ? 'video_size = :size,' : ''). '  
									user_id	 			= :user_id, 
									video_posted_date	= :video_posted_date,
									video_focus			= :video_focus,
									category_id			= :category_id,
									video_source		= :source,
									video_active 		= :active
									where video_id 			= :id';
		$sql = $this->_conn->prepareSQL($sql, $item);
		$this->_conn->execute($sql, true);
		
		return $this->_conn->getAffectedRows() > 0;
	}
	
	public function getCategoryById($catId)
	{
		$sql = 'select * from category_video where category_id = :category_id';
		$sql 	= $this->_conn->prepareSQL($sql, array('category_id' => $catId));
		return  $this->_conn->fetchRow($sql);
	}
	
	public function getListCategory($options = array(), $pageIndex = 1, $pageSize = 10){
		$userCat = isset($options['user_cat']) && !empty($options['user_cat']) ? $options['user_cat'] : '';
		$sql = 'select ' .
				(isset($options['fields']) 		? $options['fields'] : '*') .
				' from category_video where 1=1 '.
				(isset($options['parent_id']) 		? 'and parent_id = :parent_id' : '') .
				(isset($options['category_id']) 	? 'and category_id = :category_id ' : '') .
				(isset($options['user_id'])		 	? 'and user_id = :user_id ' : '') .
				(!empty($userCat) 					? 'and category_id in ('. $userCat .') ' : '') .
				' order by category_id' .
				($pageIndex > 0 ? ' limit ' . (($pageIndex - 1) * $pageSize). ','. $pageSize : '');
		$sql 		= $this->_conn->prepareSQL($sql, $options);
		$records 	= $pageSize < 2 ? $this->_conn->fetchRow($sql) : $this->_conn->fetchAll($sql) ;
		return $records;
	}
	
	public function InsertCategory($item){
		if (!empty($item['category_image'])) $item['category_image'] = Helper::getInstance()->getConfig('image_url') . $item['category_image'];
		$sql = 'insert into category_video (category_name, category_url, category_image, category_description, category_status) ' .
				'values (:category_name, :category_url, :category_image, :category_description, :category_status)';
		$sql = $this->_conn->prepareSQL($sql, $item);
		$this->_conn->execute($sql);
		return $this->_conn->getLastInsertedId();
	}
	
	public function UpdateCategory($item){
		if (!empty($item['category_image'])) $item['category_image'] = Helper::getInstance()->getConfig('image_url') . $item['category_image'];
		$sql = 'update category_video set  category_name 			= :category_name,' .
				'category_url 			= :category_url,' .
				'category_description 	= :category_description,' .
				'category_status 		= :category_status' .
				(!empty($item['category_image']) ? ',category_image = :category_image' : '') .
				' where category_id 	= :category_id';
		$sql = $this->_conn->prepareSQL($sql, $item);
		$this->_conn->execute($sql, true);
	}
	
	public function treeView($parentId = 0, $selectedIds = array(), $href = '', &$return = '', $classActive = '' )
	{
		$items = $this->getListCategory(array('parent_id' => $parentId), 1, 50);
		if (!empty($items)){
			$return .= '<ul>';
			foreach ($items as $row){
				$selectedElement = in_array($row['category_id'], $selectedIds) ? ' class="'.$classActive.'"' : '';
				$checked = $row['category_status'] == 1 ? 'checked="checked"' : '';
				$return .= '<li '. $selectedElement .'>
								<input type="checkbox"' . $checked . ' />
								<a href="'. $href . '/' . $row['category_id'] .'">'. $row['category_name'] .'</a>';
				 
				$this->treeView($row['category_id'], $selectedIds, $href, $return, $classActive);
		
				 
				$return .= '</li>';
			}
			$return .= '</ul>';
		}
	}
	
	public function treeOption($parentId = 0, $selectedIds = array(), &$return = '' , $repeat = 0)
	{
		++ $repeat;
		$items = $this->getListCategory(array('parent_id' => $parentId), 1, 50);
		if (!empty($items)){
			foreach ($items as $row){
				$selectedElement = in_array($row['category_id'], $selectedIds) ? ' selected="selected"' : '';
	
				$return .= '<option value="'. $row['category_id'] .'" '. $selectedElement .'>'.
						str_repeat('&nbsp;&nbsp;&nbsp;&nbsp;', $repeat). $row['category_name'] .
						'</option>';
					
				$this->treeOption($row['category_id'], $selectedIds, $return , $repeat);
			}
		}
	}
	
	public function SetVideoStatus($videoId, $status){
		$sql = 'update video set video_active = ' . $status . ' where video_id = ' . $videoId;
		$this->_conn->execute($sql, true);
		return $this->_conn->getAffectedRows() > 0;
	}
}