<?php
class UserModel extends Model {

    public function getUser($options = array(), $pageIndex = 1, $pageSize = 10) {
        if (isset($options['user_like']) && !empty($options['user_like'])){
            $options['user_like'] = '%' . $options['user_like'] . '%';
        }
        if (isset($options['user_password']) && !empty($options['user_password'])){
            $options['user_password'] = md5($options['user_password']);
        }
        $sql = 'SELECT * FROM user WHERE 1=1 ' .
            (isset($options['user_id'])                                             ? ' AND user_id = :user_id ' : '') .
            (isset($options['user_like']) && !empty($options['user_like'])          ? ' AND (user_name LIKE :user_like OR user_fullname LIKE :user_like) ' : '') .
            (isset($options['user_name']) && !empty($options['user_name'])          ? ' AND user_name = :user_name ' : '') .
            (isset($options['user_password']) && !empty($options['user_password'])  ? ' AND user_password = :user_password ' : '') .
            (isset($options['user_status'])                                         ? ' AND user_status = :user_status ' : '') .
            ' ORDER BY user_fullname ' .
            ($pageIndex > 0 ? ' LIMIT ' . (($pageIndex - 1) * $pageSize). ','. $pageSize : '');
        $sql = $this->_conn->prepareSQL($sql, $options);

        $result = $pageSize == 1 || isset($options['user_id']) ? array($this->_conn->fetchRow($sql)) : $this->_conn->fetchAll($sql);
        if (!empty($result)){
            foreach ($result as &$item){
                if (isset($item['role_id']) && !empty($item['role_id'])){
                    $sql    = 'SELECT role_name FROM role WHERE role_id = ' . $item['role_id'];
                    $record = $this->_conn->fetchRow($sql);
                    $item['role_name'] = isset($record['role_name']) ? $record['role_name'] : '';
                }

                if (isset($item['user_name']) && isset($options['user_password'])){
                    $sql = 'SELECT GROUP_CONCAT(route_id) AS route_ids FROM rule WHERE role_id = ' . $item['role_id'];
                    if ($record = $this->_conn->fetchRow($sql)) {
                        $sql = 'SELECT route_pattern FROM route WHERE route_id IN (' . $record['route_ids'] . ')';
                        $routes = $this->_conn->fetchAll($sql);
                        $listRoute = array();
                        if (!empty($routes)){
                            foreach ($routes as $route){
                                array_push($listRoute, $route['route_pattern']);
                            }
                        }
                        $item['listRoute'] = ',' . implode(',', $listRoute) . ',';
                    }
                }
            }

            $result = $pageSize == 1 || isset($options['user_id']) ? $result[0] : $result;
        }
        return $result;
    }

    public function insertUser($item) {
        if (isset($item['user_password']) && !empty($item['user_password'])) {
            $item['user_password'] = md5($item['user_password']);
        } else {
            $item['user_password'] = md5('jocvietnam123');
        }
        if (!empty($item['user_avatar'])) {
            $item['user_avatar'] = Helper::getInstance()->getConfig('image_url') . $item['user_avatar'];
        } else {
            $item['user_avatar'] = '/public/images/noavatar.png';
        }
        if (!isset($item['user_created_date']) || empty($item['user_created_date'])){
            $item['user_created_date'] = strtotime(date('Y-m-d H:i:s'));
        }
        if (!isset($item['user_modified_date']) || empty($item['user_modified_date'])){
            $item['user_modified_date'] = strtotime(date('Y-m-d H:i:s'));
        }
        $sql = 'INSERT INTO user (' .
            (isset($item['user_name'])      ? 'user_name,' : '') .
            (isset($item['user_password'])  ? 'user_password,' : '') .
            (isset($item['user_avatar'])    ? 'user_avatar,' : '') .
            (isset($item['user_fullname'])  ? 'user_fullname,' : '') .
            (isset($item['user_email'])     ? 'user_email,' : '') .
            (isset($item['user_phone'])     ? 'user_phone,' : '') .
            (isset($item['user_address'])   ? 'user_address,' : '') .
            (isset($item['role_id'])        ? 'role_id,' : '') .
            (isset($item['user_status'])    ? 'user_status,' : '') .
            'user_created_date,' .
            'user_modified_date)' .
            ' VALUES (' .
            (isset($item['user_name'])      ? ':user_name,' : '') .
            (isset($item['user_password'])  ? ':user_password,' : '') .
            (isset($item['user_avatar'])    ? ':user_avatar,' : '') .
            (isset($item['user_fullname'])  ? ':user_fullname,' : '') .
            (isset($item['user_email'])     ? ':user_email,' : '') .
            (isset($item['user_phone'])     ? ':user_phone,' : '') .
            (isset($item['user_address'])   ? ':user_address,' : '') .
            (isset($item['role_id'])        ? ':role_id,' : '') .
            (isset($item['user_status'])    ? ':user_status,' : '') .
            ':user_created_date,' .
            ':user_modified_date)';

        $sql = $this->_conn->prepareSQL($sql, $item);
        $this->_conn->execute($sql, true);
        return $this->_conn->getLastInsertedId();
    }

    public function updateUser($item) {
        if (isset($item['user_password']) && !empty($item['user_password'])) {
            $item['user_password'] = md5($item['user_password']);
        }
        if (!empty($item['user_avatar'])) {
            $item['user_avatar'] = Helper::getInstance()->getConfig('image_url') . $item['user_avatar'];
        }
        $sql = 'UPDATE user SET ' .
            (isset($item['user_fullname']) && !empty($item['user_fullname'])    ? 'user_fullname  	= :user_fullname,' : '') .
            (isset($item['user_email']) && !empty($item['user_email'])          ? 'user_email 		= :user_email,' : '') .
            (isset($item['user_password']) && !empty($item['user_password'])    ? 'user_password 	= :user_password,' : '') .
            (isset($item['user_avatar']) && !empty($item['user_avatar'])        ? 'user_avatar 		= :user_avatar,' : '') .
            (isset($item['user_phone']) && !empty($item['user_phone'])          ? 'user_phone 		= :user_phone, ' : '') .
            (isset($item['user_address']) && !empty($item['user_address'])      ? 'user_address   	= :user_address, ' : '') .
            (isset($item['role_id']) && !empty($item['role_id'])                ? 'role_id			= :role_id, ' : '') .
            (isset($item['user_status']) && !empty($item['user_status'])        ? 'user_status		= :user_status, ' : '') .
            'user_id = :user_id WHERE user_id = :user_id';
        $sql = $this->_conn->prepareSQL($sql, $item);
        $this->_conn->execute($sql, true);
        return $this->_conn->getAffectedRows() > 0;
    }

}

?>
