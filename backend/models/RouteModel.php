<?php
class RouteModel extends Model{
	public function isAllow($userId, $route)
	{
		$sql = 'select rule_id
				from rule as ru
				inner join route as ro
				on ru.route_id = ro.route_id
				inner join user as u
				on ru.role_id = u.role_id
				where u.user_status = 1
				and ru.rule_status = 1
				and u.user_id = :user_id
				and ro.route_pattern = :pattern';
	
		$sql 	= $this->_conn->prepareSQL($sql, array('user_id' => $userId, 'pattern' => $route));
		$result = $this->_conn->fetchRow($sql);
		$result = isset($result['rule_id']) ? true : false;
		return $result;
	}

	public function getRoutes($options = array(), $offset = 0, $limit = 100)
	{
		$sql = 'SELECT * FROM route WHERE 1=1 ' .
                (isset($options['route_id']) ? ' AND route_id = :route_id' : '') .
		        ' ORDER BY route_pattern' .
            (!empty($limit) ? ' LIMIT '. $offset. ','. $limit : '');
		$sql = $this->_conn->prepareSQL($sql, $options);
		$records = isset($options['route_id']) ? $this->_conn->fetchRow($sql) : $this->_conn->fetchAll($sql);
		return $records;
	}
	
	
	public function addRoute($item)
	{
		$sql = 'INSERT INTO route (route_pattern) VALUES (:pattern)';
	
		$sql = $this->_conn->prepareSQL($sql, $item);
		$this->_conn->execute($sql, true);
		return $this->_conn->getLastInsertedId();
	}
	
	public function updateRoute($item)
	{
		$sql = 'update route set    route_pattern 		= :pattern,
		                    where   route_id 			= :id';
	
		$sql = $this->_conn->prepareSQL($sql, $item);
		$this->_conn->execute($sql, true);
		return $this->_conn->getAffectedRows() > 0;
	}
	
	public function deleteRoute($item){
		$sql = 'delete from route where route_id = :id';
		$sql = $this->_conn->prepareSQL($sql, $item);
		$this->_conn->execute($sql, true);
	}
}
?>