<?php
class FileModel extends Model
{
	public function getFolder($options = array(), $pageIndex = 1, $pageSize = 30)
	{
		$sql = 'SELECT folder_id, parent_id, folder_name, user_id, created_date FROM folder 
				WHERE 1 = 1'.
				(isset($options['folder_id']) 	? ' AND folder_id = :folder_id' : '').
				(isset($options['parent_id']) 	? ' AND parent_id = :parent_id' : '').
				(isset($options['user_id']) 	? ' AND user_id = :user_id' : '').
				($pageIndex > 0 ? ' limit '. (($pageIndex-1)*$pageSize). ','. $pageSize : '');
				
		$sql = $this->_conn->prepareSQL($sql, $options);
        return $this->_conn->fetchAll($sql);
	}
	
	public function countFolder($options = array())
	{
		$sql = 'SELECT count(0) as total FROM folder 
				WHERE 1 = 1'.
				(isset($options['folder_id']) 	? ' AND folder_id = :folder_id' : '').
				(isset($options['parent_id']) 	? ' AND parent_id = :parent_id' : '').
				(isset($options['user_id']) 	? ' AND user_id = :user_id' : '');
				
		$sql = $this->_conn->prepareSQL($sql, $options);
        $row = $this->_conn->fetchRow($sql);
        return isset($row['total']) ? $row['total'] : 0;
	}
	
	public function getVideo($options = array(), $pageIndex = 1, $pageSize = 30)
	{
		$sql = 'SELECT video_id, folder_id, user_id, video_src, video_name, category_id, video_description, video_posted_date, video_avatar  
				FROM video 
				WHERE 1=1'.
				(isset($options['video_id']) 	? ' AND video_id = :video_id' : '').
				(isset($options['folder_id']) 	? ' AND folder_id = :folder_id' : '').
				(isset($options['user_id']) 	? ' AND user_id = :user_id' : '').
                ' ORDER BY ' .
                (isset($options['order_by'])    ? $options['order_by'] : ' video_id ').
                (isset($options['asc'])         ? '' : ' DESC ').
				($pageIndex > 0 ? ' limit '. (($pageIndex-1)*$pageSize). ','. $pageSize : '');
				
		$sql = $this->_conn->prepareSQL($sql, $options);
        return $this->_conn->fetchAll($sql);
	}

    public function getImage($options = array(), $pageIndex = 1, $pageSize = 30)
    {
        $sql = 'SELECT image_id, folder_id, user_id, image_title, image_src, image_size, image_status, image_posted_date
				FROM image WHERE 1=1'.
            (isset($options['image_id']) 	    ? ' AND image_id = :image_id' : '').
            (isset($options['folder_id']) 	    ? ' AND folder_id = :folder_id' : '').
            (isset($options['user_id']) 	    ? ' AND user_id = :user_id' : '').
            (isset($options['image_status'])    ? ' AND image_status = :image_status ' : '').
            ' ORDER BY ' .
            (isset($options['order_by'])        ? $options['order_by'] : ' image_id ').
            (isset($options['asc'])             ? '' : ' DESC ').
            ($pageIndex > 0 ? ' limit '. (($pageIndex-1)*$pageSize). ','. $pageSize : '');
        $sql = $this->_conn->prepareSQL($sql, $options);
        return $this->_conn->fetchAll($sql);
    }
	
	public function insertFolder($items = array())
	{
		$sql = 'INSERT INTO folder(parent_id, folder_name, user_id, created_date) 
				VALUES (:parent_id, :folder_name, :user_id, :created_date)';
		
		if (!isset($items['created_date'])){
			$items['created_date'] = time();
		}
		
		$sql = $this->_conn->prepareSQL($sql, $items);
		return $this->_conn->execute($sql);
		
	}
	
    public function GetListFiles($options = array(), $pageIndex = 1, $pageSize = 30)
    {
        if (isset($options['title']) && !empty($options['title'])){
            $options['title'] = '%'. $options['title'] .'%';
        }
        $sql = 'select file_id, folder_id, file_title, file_src, file_size, file_posted_date, user_id from file where 1=1 '.
            (isset($options['user_id'])  ? ' and user_id = :user_id ' : '').
            (isset($options['title']) && !empty($options['title'])  ? ' and file_title like :title' : '').
            'order by file_id desc ' .
            'limit '. (($pageIndex - 1) * $pageSize)  .','. $pageSize;

        $sql = $this->_conn->prepareSQL($sql, $options);
        return $this->_conn->fetchAll($sql);
    }

    public function deleteFolder($pid = 0){
        $sql = 'DELETE FROM folder WHERE folder_id = ' . $pid;
        $this->_conn->execute($sql, true);
        $sql = 'DELETE FROM image WHERE folder_id = ' . $pid;
        $this->_conn->execute($sql, true);
        $sql = 'DELETE FROM video WHERE folder_id = ' . $pid;
        $this->_conn->execute($sql, true);
    }

    public function InsertFile($item)
    {
        $sql = 'select file_id from file where file_src = :file_src';
        $result = $this->_conn->fetchRow($sql);
        if (isset($result['file_id']) && intval($result['file_id']) > 0){
            return $result['file_id'];
        } else {
            $sql = 'insert into file  (user_id,
										file_title,
										file_src,
										file_size,
										file_status,
										file_posted_date)
								values (:user_id,
										:file_title,
										:file_src,
										:file_size,
										:file_status,
										:file_posted_date)';

            $sql = $this->_conn->prepareSQL($sql, $item);
            $this->_conn->execute($sql, true);
            return $this->_conn->getLastInsertedId();
        }
    }
}
?>