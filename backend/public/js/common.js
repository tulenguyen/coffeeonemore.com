var sendData;

var navi = {
	init: function(){
		pathname = document.location.pathname;

		$('.sub li').each(function() {
			var listRoute = $('#listRoute').val();
			var href = $(this).children("a").attr("href");

			if (a = listRoute.indexOf(',' + href.substring(1, href.length) + ',') == -1){
				$(this).addClass('hide');
			}
		});

		$('.sub').each(function() {
			var reg = new RegExp('admin/[^/]+', 'gi');
			var match = reg.exec(window.location.href);
			var url = match[0];

		    if ($(this).html().indexOf(url + '"') == -1) {
		        $(this).addClass('hide');
		    }
		    var flag = false;
		    $(this).children('li').each(function() {
		    	if (!$(this).hasClass('hide'))
		    		flag = true;
		    });
		    if (!flag){
		    	$(this).parent().addClass('hide');
		    }
		});

		$('.mainnav>ul>li').click(function(){
			$('.sub').each(function(){
				$('.sub').addClass('hide');
			});
			$(this).find('.sub').removeClass('hide');
		});
	}
};

var eventChange = {
	bindEvent : function(id){
        var me = this;

		/* Tìm tin liên quan */
        $('#btnFindNewsRelation').click(function(){
            var title = $('.input_data input').val();
            if(!title) {
                $('#show_data').hide();
            }
            $.ajax({
                type: 'GET',
                url: '/ajax',
                data: { title : title, page: 0, action : 'find-news-relation' },
                dataType: 'json',
                beforeSend: function(){
                    $('.input_data input').val('Đang tìm tin liên quan...');
                },
                success: function(data) {
                    $('.input_data input').val(title);
                    $('#show_data').html('');
                    $('#show_data').html(data['news']);
                    $('#show_data li a').click(function(){
                        var $this = $(this);
                        var newsId = $this.attr('rel');
                        var newsTitle = $this.text();
                        newsTitle = newsTitle.replace(',', '');
                        var ids = $('#newsRelation').val().split(',');
                        ids = $.grep(ids, function(value) {
                            return value != '';
                        });
                        if ($.inArray(newsId, ids) < 0)
                            ids.push(newsId);
                        if ($.inArray(newsTitle, titles) < 0)
                            titles.push(newsTitle);
                        newsRelates[newsId] = newsTitle;
                        $('#newsRelation').val(ids.join(','));
                        //$('.input_data').val(titles.join(','));
                        var ulData = $('.input_data ul');
                        if (ulData.html().indexOf('<span>' + newsTitle + '</span>') < 0){
                            ulData.append(
                                $('<li class="select2-search-choice"><span>' + newsTitle + '</span><span class="icon12 entypo-icon-cancel" rel="' + newsId + '"></span></li>').click(function(){
                                    var $this = $(this);
                                    $newsTitle = $this.parent().find('div').text();
                                    for (i in newsRelates){
                                        if (newsRelates[i] == $newsTitle){
                                            ids = $.grep(ids, function(value) {
                                                return value != i;
                                            });
                                        }
                                    }
                                    $('#newsRelation').val(ids.join(','));
                                    newsRelates = $.grep($newsTitle, function(value){
                                        return value != $newsTitle;
                                    });
                                    me.bindEvent('#show_data');
                                })
                            );
                        }
                    });
                    $('#show_data').show();
                }
            });
        });

        $('.input_data .entypo-icon-cancel').click(function(){
            var id = $(this).attr('rel');
            $(this).parent().remove();
            var ids = $('#newsRelation').val().split(',');
            ids = $.grep(ids, function(value) {
                return value != id;
            });
            $('#newsRelation').val(ids.join(','));
        });

        $('.count-content').keyup(function(e){
            $this = $(this);
            var count = $this.val().length;
            $this.parent().parent().find('.count-label').html(count + '/170');
        });
	},

	change : function() {
		var me = this;
        me.bindEvent('');

		$('tr', '#checkAll').each(function(){
			var tr = this;
			$('select', tr).change(function(){
				if ($(this).val()){
					$('input', tr).prop('checked', true);
					$('input', tr).parent().addClass('checked');
				}
			});
		});

        $('.cbChange').change(function(){
            var target = $(this).attr('target');
            if($(this).is(":checked")) {
                $('#' + target).removeClass("hide");
            } else {
                $('#' + target).addClass("hide");
            }
        });

        $('#newsTitle').focusout(function(){
            $.ajax({
                type: 'GET',
                url: '/ajax',
                data: { newsId: $('#newsId').val(), newsTitle : $('#newsTitle').val(), action : 'check-title-exist' },
                dataType: 'text',
                success: function(data) {
                    if (data == 'false'){
                        $('.alert-error').removeClass('hide');
                        $('#edit-news-error').html('<strong>Có lỗi!</strong> Title đã tồn tại');
                        $('html, body').animate({ scrollTop: $("#edit-news-error").scrollTop() }, 1000);
                        return false;
                    } else {
                        $('.alert-error').addClass('hide');
                    }
                }
            });
        });

        //Upload avatar edit-news
        $('.input-image').change(function () {
            var root        = $(this).closest('.upload-image');
            var divError    = root.find('.alert-error');
            var errorNotice = root.find('.upload-error');
            var imgSrc      = root.find('.img-src');
            var avatar      = root.find('.avatar');

            var error       = 'Lỗi gòy :(';
            var uploading   = 'Upload...';

            //Thong bao dang upload
            divError.removeClass('hide');
            errorNotice.text(uploading);

            var file = this.files[0];
            /* Create a FormData instance */
            var formData = new FormData();
            /* Add the file */
            formData.append('data', file);
            formData.append('action', 'upload-avatar');
            formData.append('type', 'avatar');

            $.ajax({
                type: 'POST',
                dataType: 'json',
                cache: false,
                data: formData,
                url: '/admin/upload-file',
                processData: false,
                contentType: false,
                success:function(data){
                    if (data.image) {
                        avatar.attr('src', data.image);
                        imgSrc.val(data.image);
                        divError.addClass('hide');
                    } else {
                        divError.removeClass('hide');
                        errorNotice.text(error);
                    }
                },
                error: function() {
                    divError.removeClass('hide');
                    errorNotice.text(error);
                }
            });
        });
	}
};

var btnClick = {
	click : function() {
        $('.btnNews').click(function(){
            var status  = $(this).attr('act');
            var newsId  = $(this).parent().find('.newsId').val();
            var note    = $(this).parent().find('textarea').val();

            if (status != undefined){
                $.ajax({
                    type: 'POST',
                    url: '/ajax',
                    data: { newsId: newsId, note: note, route: window.location.href, status: status, action: 'set-news-status' },
                    dataType: 'json',
                    success: function(data) {
                        if (data == 'false'){
                            alert('Oops, có lỗi, zzZ!!!');
                            return false;
                        }
                        location.reload();
                    }
                });
            }
        });

        $('.btnPosition').click(function(){
            var pos     = $(this).attr('act');
            var newsId  = $(this).parent().find('.newsId').val();

            if (newsId > 0){
                $.ajax({
                    type: 'GET',
                    url: '/ajax',
                    data: { newsId: newsId, position: pos, route: window.location.href, action: 'set-news-position' },
                    dataType: 'json',
                    success: function(data) {
                        if (data == 'false'){
                            alert('Oops, có lỗi, zzZ!!!');
                            return false;
                        }
                        location.reload();
                    }
                });
            }
        });

        $('.btnTime').click(function(){
            var time    = $(this).attr('act');
            var newsId  = $(this).parent().find('.newsId').val();
            var value   = $(this).parent().find('textarea').val();

            if (newsId > 0){
                $.ajax({
                    type: 'GET',
                    url: '/ajax',
                    data: { newsId: newsId, time: time, value: value, action: 'set-news-timer' },
                    dataType: 'json',
                    success: function(data) {
                        if (document.location.href.indexOf('list-news-publish') >= 0) {
                            //Reset cache trên trang chủ
                            sendData = {href: '/admin/list-news/' + newsId, action: 'request'};
                            SendRequestResetCache(function () {
                                location.reload();
                            });
                        } else {
                            location.reload();
                        }

                        if (data == 'false'){
                            alert('Oops, có lỗi!!!');
                            return false;
                        }
                    }
                });
            }
        });

        $('.btnReturnNews').click(function(){
            var reasonBox = $(this).parent().find('.reason-box');
            $('.reason-box').each(function(){
                if (!$(this).hasClass('hide') && $(this).attr('data') != reasonBox.attr('data')){
                    $(this).addClass('hide');
                }
            });
            if (reasonBox.hasClass('hide')){
                reasonBox.removeClass('hide');
                reasonBox.find('textarea').focus();
            } else {
                reasonBox.addClass('hide');
            }
        });

        $('.btnUser').click(function(){
            var status = $(this).attr('act');
            var userId = $(this).parent().find('.userId').val();

            $.ajax({
                type: 'POST',
                url: '/ajax',
                data: { userId: userId, status: status, action: 'update-user' },
                dataType: 'json',
                success: function(data) {
                    if (data){
                        location.reload();
                    }
                    else {
                        alert('Oops, có lỗi rùi!!!');
                        return false;
                    }
                }
            });
        });

		$('#btnInsertNews').click(function(){
            var result = ValidateNews();
            if (result.length <= 0) {
                $('.alert-error').addClass('hide');
                GetListCheckboxes();
                $('form[name=news]').attr('target', '_self');
                news.action.value='add';
            } else {
                $('.alert-error').removeClass('hide');
                $('#edit-news-error').html('<strong>Có lỗi!</strong> ' + result);
                $("html, body").animate({ scrollTop: $("#edit-news-error").scrollTop() }, 1000);
                return false;
            }
		});

        $('#btnSaveNews').click(function(){
            var result = ValidateNews();
            if (result.length <= 0) {
                $('.alert-error').addClass('hide');
                GetListCheckboxes();
                $('form[name=news]').attr('target', '_self');
                news.action.value='update';
            } else {
                $('.alert-error').removeClass('hide');
                $('#edit-news-error').html('<strong>Có lỗi!</strong> ' + result);
                $("html, body").animate({ scrollTop: $("#edit-news-error").scrollTop() }, 1000);
                return false;
            }
        });

        $('#btnCrawlNews').click(function(){
            var url = $('#newsURL').val();
            if (url){
                $.ajax({
                    type: 'GET',
                    url: '/ajax',
                    data: { url: url, action: 'crawler-news' },
                    dataType: 'json',
                    success: function(data) {
                        if (data){
                            $('input[name=newsTitle]').val(data.news_title);
                            $('textarea[name=newsSapo]').val(data.sapo);
                            $('textarea[name=newsSeoTitle]').val(data.seo_title);
                            $('textarea[name=newsSeoDescription]').val(data.seo_desc);
                            $('textarea[name=newsSeoKeyword]').val(data.seo_keywords);
                            $('#news_image').attr('src', data.photo);
                            $('input[name=image]').val(data.photo);
                            CKEDITOR.instances['news_content'].setData(data.content);
                            $('input[name=sourceName]').val('Nguồn ' + data.domain);
                            $('input[name=domain]').val(data.domain);
                            $('input[name=newsPublishDate]').val(data.date);
                        }
                        return false;
                    }
                });
            }
            return false;
        });
	}
	//end event click
};

function toggleCheckbox(obj){
    var html = $('<div>').append($(obj).clone()).html();
    $(obj).closest('.noMar').find('.result').val(html.indexOf('checked="checked"') >= 0 ? '1' : '0');
}

function toggleCheckboxChange(obj){
    var html = $('<div>').append($(obj).clone()).html();
    $(obj).closest('.noMar').find('.isChange').val(html.indexOf('checked="checked"') >= 0 ? '1' : '0');
    $('.change-wrap').toggle();
}

function checkForm(){
	var values = [];
	$('input[name=cbUserCat]:checked').each(function(){
		values.push($(this).val());
	});
	$('input[name=cbUserCats]').val(values);
}

function GetListCheckboxes(){
	var values = [];
	$('input[name=checkboxChild]:checked').each(function(){
		values.push($(this).val());
	});
	$('input[name=checkboxes]').val(values);
}

function ValidateNews(){
    var str = '';
    if ($('#newsTitle').val().length <= 0) {
        str = 'Title không được để trống';
    }
    return str;
}

function SendRequestResetCache(callback){
	setTimeout(function(){
		//data = sendData;
        if (sendData != undefined && typeof sendData.href != undefined){
			data = sendData;
            if (document.location.href.indexOf('list-news') == -1 && document.location.href.indexOf('list-candidate') == -1){
                data.href 	= encodeURIComponent(document.location.href);
            }
            data.action = 'request';
		}else{
			var data = {href : encodeURIComponent(document.location.href), action : 'request'};
			$('input[name!=""],select[name!=""]').each(function(){
				if (name = $(this).attr('name')){
					if (name != 'action'){
						data[name] = $(this).val();
					}
				}
			});
		}
        $.ajax({
            url: 'http://coffeeonemore.com/managecache',
            dataType: 'jsonp',
            type: 'post',
            data: data,
            beforeSend: function(){
            	$('<div id="loading" style="position:absolute;top:80px;left:40%;width:20%;z-index:1000;height:40px;padding:10px 0;background:#000;color:#fff;text-align:center">').
            	 html('Hệ thống đang chạy tiến trình reset cache, bạn vui lòng chờ trong giây lát!!! Thanks!!!').
            	 appendTo('body');
            },
            success: function(dataReturn){
                $('#loading').hide();
                if (typeof callback == 'function'){
                    callback();
                }
            },
            error: function(){
                $('#loading').hide();
                if (typeof callback == 'function'){
                    callback();
                }
            }
        });

	}, 100);
};

$(document).ready(function(){
	navi.init();
	btnClick.click();
	eventChange.change();

    if ($('.alert-error').length > 0 && $('.alert-error').html().indexOf('thành công') > 0){
        SendRequestResetCache(function(){ });
    }
});