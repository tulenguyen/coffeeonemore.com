<?php
class UploadManagerLayout extends Layout
{
	public function setup()
	{
		$this->setTemplatePath('view/templates/')
			 ->setTemplateFile('UploadManager')
			 ->setPageTitle('Trang quản lý files');
		
		$this->registerElement('UploadManager', 'view/elements/admin/');
	}	
}