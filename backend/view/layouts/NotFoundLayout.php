<?php
class NotFoundLayout  extends Layout
{
    public function setup()
    {
    	$this->setTemplatePath('NotFound')
    		 ->setTemplateFile('view/templates/')
    		 ->setPageTitle('Error 404 Not found - Không tìm thấy yêu cầu của bạn');
    	
        $this->registerElement('Head', 'view/elements/common/');
        $this->registerElement('Header', 'view/elements/common/');
        $this->registerElement('Footer', 'view/elements/common/');
        $this->registerElement('Bottom', 'view/elements/common/');
        
        Context::getInstance()->getFront()->getResponse()->setRawHeader("HTTP/1.0 404 Not Found");
        Context::getInstance()->getFront()->getResponse()->sendHeaders();
	}	
}

?>
