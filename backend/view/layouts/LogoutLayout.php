<?php
class LogoutLayout extends Layout{
	public function setup()
	{
		HttpAuth::getInstance()->clear();
		Context::getInstance()->getFront()->getResponse()->setRedirect('/dang-nhap');
		Context::getInstance()->getFront()->getResponse()->sendHeaders();
	}
}