<?php
class AdminLayout extends Layout
{
	public function setup()
	{
		$this->setTemplatePath('view/templates/')
			 ->setTemplateFile('Admin')
			 ->setPageTitle('Trang quản trị nội dung');

		$request 	= Context::getInstance()->getFront()->getRequest();
		$parts		= $request->getParts();
		$route		= implode('/', array_slice($parts, 0, 2));
        $identity = HttpAuth::getInstance()->getIdentity();

        if (isset($identity['listRoute']) && !empty($identity['listRoute']) && stripos($identity['listRoute'], $route) !== false){
			$module		= Context::getInstance()->getRoute()->getParam('module');
			if (null != $module){
				if (null != ($arr = explode('-', $module))){
					$element = '';
					foreach ($arr as $val){
						$element .= ucfirst($val);
					}
					$this->assign('elementName', $element);
					$this->registerElement($element, 'view/elements/admin/');
				}
			}
		}
		
		$this->registerElement('Head', 'view/elements/common/');
		$this->registerElement('Header', 'view/elements/common/');
		$this->registerElement('LeftBar', 'view/elements/common/');
		$this->registerElement('Footer', 'view/elements/common/');
		$this->registerElement('Bottom', 'view/elements/common/');
	}	
}