<?php

class AjaxLayout extends Layout {

    public function setup() {
        $this->handleAction();
    }

    //Bắt sự kiện AJAX GET json
    public function handleAction() {
        $front = Context::getInstance()->getFront();
        $request = $front->getRequest();
        $action = $request->getParam('action');
        $newsID = $request->getParam('newsId');
        $identity = HttpAuth::getInstance()->getIdentity();
        $userId = $identity['user_id'];

        if ($action == 'set-news-status') {
            $status = $request->getParam('status');
            if ($newsID > 0) {
                $model = Context::getInstance()->getFront()->getModel('NewsModel');
                $identity = HttpAuth::getInstance()->getIdentity();
                $route = $request->getParam('route');
                if (stripos($route, 'list-news-publish') === false && $status == 1) {
                    $model->setNewsStatus($newsID, 1);
                }
                $model->updateNews([
                    'news_id' => $newsID,
                    'approved_user' => $identity['user_id'],
                    'news_status' => $status
                ]);

                $url = Helper::getInstance()->getConfig('domain') . 'managecache?action=request' . '&href=/admin/list-news/' . $newsID;
                Transfer::getInstance()->getContent($url, 'http://cms.coffeeonemore.com', true, array('_cookie_refer' => 'cúc cu'));
                echo json_encode('true');
                die;
            }
        } elseif ($action == 'find-news-relation') {
            //Bắt sự kiện tìm tin theo title
            $title = $request->getParam('title');
            $model = $front->getModel('NewsModel');
            $identity = HttpAuth::getInstance()->getIdentity();
            $listNews = $model->getNews([
                'field' => 'news_id, news_title, news_image, news_published_date, news_status, visit_count',
                'status' => 1,
                'role_id' => $identity['role_id'],
                'title_like' => $title] , 1, 20
            );
            $data = ['news' => ''];
            if (!empty($listNews) && $action == 'find-news-relation') {
                foreach ($listNews as $news) {
                    if ($news['news_published_date'] < time() && $news['news_status'] == 1) {
                        $data['news'] .=    '<li>' .
                                                '<a href="/admin/edit-news/' . $news['news_id'] . '" onclick="return false;" rel="' . $news['news_id'] . '">' .
                                                    $news['news_title'] .
                                                '</a>' .
                                            '</li>';
                    }
                }
            }
            echo json_encode($data);
            die;
        } else if ($action == 'find-event') {
            $data = array('listEvent' => '');
            $model = Context::getInstance()->getFront()->getModel('EventModel');
            $eventName = $request->getParam('eventName');
            $listEvent = $model->getListEvent(array('event_name' => '%' . $eventName . '%', 'event_active' => 1), 0);
            if ($listEvent) {
                foreach ($listEvent as $event) {
                    $data['listEvent'] .=   '<li>' .
                                                '<a href="/admin/edit-event/' . $event['event_id'] . '" onclick="return false;" rel="' . $event['event_id'] . '">' .
                                                    $event['event_name'] .
                                                '</a>' .
                                            '</li>';
                }
            }
            echo json_encode($data);
            die;
        } else if ($action == 'check-title-exist') {
            $newsTitle = $request->getParam("newsTitle");
            $newsId = $request->getParam("newsId");
            if (!empty($newsTitle)) {
                $model = $front->getModel('NewsModel');
                if ($model->checkTitleExists(array('news_title' => $newsTitle, 'news_id' => $newsId))) {
                    echo 'true';
                } else {
                    echo 'false';
                }
            }
            die;
        } else if ($action == 'set-news-position') {
            $positionId = $request->getParam('position');
            if (!empty($newsID)) {
                $model = Context::getInstance()->getFront()->getModel('PositionModel');
                $model->InsertNewsPosition(array('news_id' => $newsID, 'position_id' => $positionId));
                $model = Context::getInstance()->getFront()->getModel('NewsModel');
                $identity = HttpAuth::getInstance()->getIdentity();
                $route = $request->getParam('route');
                if (stripos($route, 'list-news-publish') === false) {
                    //If news is not published, update current time
                    $model->setNewsStatus($newsID, 1);
                }
                $model->updateNews([
                    'news_id' => $newsID,
                    'news_note' => '',
                    'approved_user' => stripos($route, 'list-news-publish') === false ? $identity['user_id'] : '',
                    'news_status' => 1
                ]);
                $url = Helper::getInstance()->getConfig('domain') . 'managecache?action=request' . '&href=/admin/list-news/' . $newsID;
                Transfer::getInstance()->getContent($url, 'http://cms.coffeeonemore.com', true, array('_cookie_refer' => 'cúc cu'));
                echo json_encode('true');
                die;
            }
        } else if ($action == 'set-news-timer') {
            $time = $request->getParam('time');
            $value = $request->getParam('value');
            $publishedDate = $time == '1' ? strtotime(date('Y-m-d H:i:s')) : strtotime($value);
            $model = Context::getInstance()->getFront()->getModel('NewsModel');
            if ($newsID > 0 && $publishedDate > 0) {
                $model->updateNews(array('news_id' => $newsID, 'news_published_date' => $publishedDate));
                echo json_encode('true');
            } else {
                echo json_encode('false');
            }
            die;
        } else if ($action == 'crawler-news') {
            $url = $request->getParam('url');
            $news = $this->detectNews($url);

            $news['date'] = isset($news['date']) && !empty($news['date']) ? date('Y-m-d H:i:s', intval($news['date'])) : date('Y-m-d H:i:s');
            echo json_encode($news);
        }
        /* End if; */
        die;
    }

    public function detectNews($url = '') {
        if (empty($url) || !$html = Transfer::getInstance()->getContent($url)) {
            return null;
        }

        $data = array('news_title' => null,
            'date' => null,
            'seo_title' => null,
            'seo_desc' => null,
            'seo_keywords' => null,
            'sapo' => null,
            'content' => null,
            'tags' => null,
            'photo' => null,
            'url' => $url);
        // get title
        if (empty($data['seo_title']) && preg_match('#<title>(.*?)(?:[\-\|].*?)?</title>#is', $html, $match)) {
            $data['seo_title'] = trim(html_entity_decode($match[1], ENT_QUOTES, 'UTF-8'));
        }
        $data['seo_title'] = preg_replace('#["><]#s', '', strip_tags($data['seo_title']));
        if (empty($data['news_title'])) {
            $data['news_title'] = $data['seo_title'];
        }
        // get keywords
        if (empty($data['seo_keywords']) && preg_match('#<meta[^>]*name=[\'"]keyword(?:s)?[\'"][^>]*content=[\'"]([^>]+)[\'"][^>]*>#is', $html, $match)) {
            $data['seo_keywords'] = trim(html_entity_decode($match[1], ENT_QUOTES, 'UTF-8'));
        }
        $data['seo_keywords'] = !empty($data['seo_keywords']) ? preg_replace('#["><]#s', '', strip_tags($data['seo_keywords'])) : (isset($data['tags']) ? $data['tags'] : '');
        // get description
        if (empty($data['seo_desc']) && preg_match('#<meta[^>]*name=[\'"](?:og:)?description[\'"][^>]*content=[\'"]([^>]+)[\'"][^>]*>#is', $html, $match)) {
            $data['seo_desc'] = trim(html_entity_decode($match[1], ENT_QUOTES, 'UTF-8'));
        }
        $data['seo_desc'] = preg_replace('#["><]#s', '', strip_tags($data['seo_desc']));

        //get domain
        if ($domain = $this->getDomain($url)) {
            $data['domain'] = $domain;
        }
        /* if(preg_match('#(?:\d{1,2}\s*:\s*)?\d{1,2}\s*:\s*\d{1,2}[^\d]{0,20}\d{1,4}(\s*[\/\.\,\-\|]\s*)\d{1,2}\\1\d{1,4}#is', $html, $match)){
          $data['date'] = Util::getTimeFromString($match[0], true);
          } else if(preg_match('#\d{1,4}(\s*[\/\.\,\-\|]\s*)\d{1,2}\\1\d{1,4}[^\d]{0,20}\d{1,2}\s*:\s*\d{1,2}#is', $html, $match)){
          $data['date'] = Util::getTimeFromString($match[0], true);
          } else if(preg_match('#\d{1,4}(\s*[\/\.\,\-\|]\s*)\d{1,2}\\1\d{1,4}#is', $html, $match)){
          $data['date'] = Util::getTimeFromString($match[0], true);
          }

          if (empty($data['date']) || $data['date'] > time()){
          $date['date'] = time();
          } */

        $date['date'] = strtotime(date('Y-m-d H:i:s'));

        /*         * *Get news content*************************** */
        $html = str_replace('&nbsp;', '', $html);
        $html = preg_replace('#<(style|script|noscript)[^>]*>.*?</\\1>|<!--.*?-->|</?font[^>]*>|</(strong|span|b|i|em)>\s*<\\2[^>]*>|<([^\s>]+)[^>]+display\s*\:\s*none[^>]+>.*?</\\3>|<[^>]+display\s*\:\s*none[^>]+/?>#is', '', $html);
        $html = preg_replace('#<(div|p|a|span|b\s+|br|strong|table|tr|th|td|tbody|thead|button|label|input|form|textarea|em|font|hr|script|stype|ul|li|ol)[^>]*>(\s*|\s*<br[^>]*>\s*)</\\1>|\s\s+|\s+(id|onclick|onmouse\w+|on|border|class|cell\w+)\s*=\s*".*?"#is', ' ', $html);
        $html = preg_replace('#(\s*</?(br[^>]*|p)>\s*)+#is', '<br>', $html);
        $html = preg_replace('#</?(b|i|b\s+[^>]*|i\s+[^>]*)>#is', '', $html);
        $html = preg_replace('#<([^\s>]+)[^>]*(?:bold[^>]*italic|italic[^>]*bold)[^>]*>(.*?)</\\1>#is', '<strong><em>\\2</em></strong>', $html);
        $html = preg_replace('#<([^\s>]+)[^>]*bold[^>]*>(.*?)</\\1>#is', '<strong>\\2</strong>', $html);
        $html = preg_replace('#<([^\s>]+)[^>]*italic[^>]*>(.*?)</\\1>#is', '<em>\\2</em>', $html);
        $html = preg_replace('#<([^\s>]+)[^>]*>\s*,\s*</\\1>#is', ',', $html);
        $html = preg_replace('#<(span|font)[^>]*>\s*<\\1[^>]*>([^<]+)</\\1>\s*</\\1>#is', '\\2', $html);
        $html = preg_replace('#(<([^\s>]+)[^>]*>)([^<]*)<br\s*/?>([^<]*)(</\\2>)#is', '\\1\\3\\5<br/>\\1\\4\\5', $html);
        $html = preg_replace('#<br[^>]*>\s*(<img[^>]+>)\s*<br[^>]*>#is', '<p>\\1', $html);
        $html = preg_replace('#<(span|font|a)([^>]*)>([^<]*)<img([^>]+)>([^<]*)</\\1>#is', '<p\\2>\\3<img\\4>\\5</p>', $html);
        $html = preg_replace('#<(/?p|b|i)>#is', "<\\1 >", $html);
        $html = preg_replace_callback('#<iframe[^>]+>(?:\s*</iframe>)?#is', function($m) {
            return !preg_match("#youtube.com#is", $m[0]) ? "" : $m[0];
        }, $html);

        $html = preg_replace_callback('#</?([^\s>]+)[^>]*>#is', function($m) {
            return !preg_match("#^(div|ul|li|ol|iframe|br|hr|a|img|em|strong|object|embed|param|table|tr|td|tbody|trow|thead|span|p|h\d)$#i", $m[1]) ? "<div>" : $m[0];
        }, $html);
        $paragraphs = preg_split('#</?(br|p\s+|div|hr|h\d|ol|input|textarea|select|form)[^>]*>|<table[^>]*>(\s*<[^>]+>\s*)+</table>|<ul[^>]*>.*?</ul>#is', $html);

        $content = null;
        $count = count($paragraphs);
        $marked = array();
        $markIndex = 0;

        $noTagParagraphs = array();

        foreach ($paragraphs as $paragraph) {
            $noTagParagraphs[] = trim(strip_tags($paragraph));
        }

        for ($i = 0; $i < $count; $i++) {
            $paragraph = trim($paragraphs[$i]);
            if (preg_match('#</?table[^>]*>#is', $paragraph)) {
                $flag = true;
            } else if (empty($paragraph)) {
                $flag = false;
            } else
                $flag = true;

            if ($flag == false) {
                if (isset($marked[$markIndex])) {
                    $marked[$markIndex]['mark'] ++;
                } else {
                    $marked[$markIndex] = array('mark' => 0, 'paragraph' => $paragraph);
                }
            } else {
                $markIndex = $i;
                if (!isset($marked[$markIndex])) {
                    $marked[$markIndex] = array('mark' => 0, 'paragraph' => $paragraph);
                }
            }

            $noTagParagraph = trim(strip_tags($paragraphs[$i]));
            if (strlen($noTagParagraph) > 80) {
                if (($key = array_search($noTagParagraph, $noTagParagraphs)) && $key != $i) {
                    unset($paragraphs[$key]);
                    unset($marked[$key]);
                }
            }
        }

        $begin = 0;
        $repeat = 0;

        foreach ($marked as $i => $item) {
            $length = strlen(trim(strip_tags($item['paragraph'])));
            $percent = $length / (strlen($item['paragraph']) + 1);
            if ($i > 10 && (($begin == 0 && ($item['mark'] <= 6 && $length > 100 && $percent > 0.7) || ($length > 150 && ($percent > 0.7))))) {
                $begin = 1;
            } else if ($repeat == 0 && $begin == 1 && ($item['mark'] <= 3 || $length > 200)) {
                $begin ++;
                $repeat = 1;
            } else if ($repeat == 1 && ($item['mark'] > 3 && $length < 200 && $length > 0)) {
                if (strlen($content) > 1500) {
                    $begin ++;
                }
            }

            if (($begin > 2 || ($begin > 1 && $item['mark'] > 6))) {
                break;
            } else if ($begin > 0 && $item['paragraph'] && !preg_match('#^\d+$|^/?([^/]+/){3,}/?$|^[\w\P{Common}\d-]+(/[\w\d-\.]+)+/?$#uis', $item['paragraph'])) {
                if (!preg_match('#^\s*<[^>]+>|<[^>]+>\s*$#is', $item['paragraph'])) {
                    $content .= strlen($item['paragraph']) > 10 ? '<p>' . $item['paragraph'] . '</p>' : $item['paragraph'];
                } else {
                    $content .= $item['paragraph'];
                }
            }
        }

        if ($contents = preg_split('#<table#is', $content)) {
            //fix table
            $last = array_pop($contents);
            $last = preg_replace_callback('#.*#is', function($m) {
                return strpos($m[0], "</table>") === false ? $m[0] . "</td></tr></table>" : $m[0];
            }, $last);
            array_push($contents, $last);
            $content = implode('<table', $contents);
        }

        /*         * *Get Tag, Sapo************************ */
        if (strlen($content) > 500) {
            $data['content'] = preg_replace('#(<[^>]+>)(\s|&nbsp;)*\([^\)]+\)(\s*-)?#s', '$1', $data['content']);
            $data['content'] = preg_replace('#\s\s+#s', ' ', $data['content']);
            $data['content'] = preg_replace('#(\s*,)(\s*,)+#s', ', ', $data['content']);
            $data['content'] = html_entity_decode($data['content'], ENT_QUOTES, 'UTF-8');
            $data['content'] = $this->repairContent($content, $url, $data['news_title']);
            $data['link'] = Util::toUnsign($data['news_title']);

            $paragraphs = preg_split('#</?p>#is', $content);
            if (count($paragraphs) >= 3) {
                foreach ($paragraphs as $paragraph) {
                    $paragraph = strip_tags($paragraph);
                    if (count(preg_split('#\s+#is', $paragraph)) >= 10) {
                        $data['sapo'] = $paragraph;
                        break;
                    }
                }
            }

            if (empty($data['sapo']) && preg_match('#^\s*((?:\S+\s+){30})#is', strip_tags($content), $match)) {
                $data['sapo'] = $match[1];
            }

            if (empty($data['photo']) && preg_match('#<img[^>]+src=[\'"]([^\'"]+.(?:jpg|jpeg|png))[\'"][^>]*>#is', $data['content'], $photo)) {
                $data['photo'] = $photo[1];
            }

            if (isset($data['seo_keywords']) && !empty($data['seo_keywords'])) {
                $data['tags'] = $data['seo_keywords'];
            } else {
                $data['tags'] = $this->getTags($data['news_title'], $data['content']);
            }
        }
        return $data;
    }

    private function getDomain($url) {
        $domain = null;
        if (preg_match('#^(?:https?://)?([^/]+)#i', $url, $match)) {
            $domain = $match[1];
        }
        return $domain;
    }

    private function repairContent($content, $url, $title = null) {
        $title = str_replace('"', "'", $title);
        $domain = $this->getDomain($url);
        $content = preg_replace_callback('#<a[^>]+>(.*?)</a>#is', function($match) {
            if (preg_match('#javascript|print|void#is', $match[0])) {
                return null;
            } else {
                return $match[1];
            }
        }, $content);

        $content = preg_replace_callback('#<img[^>]+>#is', function($match) use ($domain, $title) {
            if (!preg_match('#src\s*=\s*["\'](.*?)["\']#is', $match[0], $src)) {
                return null;
            }
            $src = trim($src[1]);
            if (empty($src) || !preg_match('#\.(jpg|jpeg|gif|png|bmp)#is', $src)) {
                return null;
            }

            $check = true;
            if (preg_match('#^//#is', $src)) {
                $src = 'http:' . $src;
            }if (preg_match('#^/#is', $src)) {
                $src = 'http://' . $domain . $src;
            } else if (preg_match('#^/?\.+/#is', $src)) {
                $src = 'http://' . $domain . '/' . preg_replace('#^/?(\.+/)+#is', '', $src);
            } else if (preg_match('#^http#is', $src)) {
                $src = $src;
            } else if (preg_match('#^data:#is', $src)) {
                $src = $src;
                $check = false;
            } else if (preg_match('#^[\w\d-]+#is', $src)) {
                $src = 'http://' . $domain . '/' . $src;
            }

            if ($check == true) {
                if ($size = @getimagesize($src)) {
                    if (preg_match('#\d+#', $size[3], $width)) {
                        if ($width[0] < 200) {
                            return null;
                        }
                    }
                }
            }

            return "<p style=\"text-align:center\"><img src=\"" . $src . "\" alt=\"$title\" title=\"$title\" /></p>";
        }, $content);

        $content = preg_replace('#(<p[^>]*>\s*<img[^>]+>\s*</p>\s*)<p[^>]*>([^<]{0,100})</p>#is', '$1<p style="text-align:center"><em>$2</em></p>', $content);

        return $content;
    }

    private function getTags($title, $content, $threshold = 5) {
        $keyword = preg_replace('#[^\P{Common}]+#uis', '  ', html_entity_decode($title, ENT_QUOTES, 'UTF-8'));
        $reg = preg_replace('#\s([^\s]*?)\s#uis', '(?:\\1\s*)?', ' ' . preg_quote($keyword) . ' ');
        if (preg_match_all('#\s+' . $reg . '#uis', $content, $matches)) {

            $temp = array();
            foreach ($matches[0] as $item) {
                if ($item = trim($item)) {
                    $len = mb_strlen($item, 'UTF-8');
                    if ($len > 8 && $len < 25) {
                        $temp[$item] = isset($temp[$item]) ? $temp[$item] + 1 : 1;
                    }
                }
            }

            if (!empty($temp)) {
                $tags = array();
                foreach ($temp as $item => $val) {
                    $first = mb_substr($item, 0, 1, 'UTF-8');
                    if ($val >= $threshold || ($first && ctype_upper($first))) {
                        array_push($tags, $item);
                    }
                }
                return $tags;
            }
        }
        return null;
    }

}
