<?php
class LoginLayout extends Layout
{
	public function setup()
	{
		$this->setTemplatePath('view/templates/')
			 ->setTemplateFile('Login');
		if (HttpAuth::getInstance()->isAuthenticated()){
            Context::getInstance()->getFront()->getResponse()->setRedirect('/admin/list-news-store');
		}else{
			$this->login();
		}
		
		$this->registerElement('Head', 'view/elements/common/');
		$this->registerElement('Header', 'view/elements/common/');
		$this->registerElement('Footer', 'view/elements/common/');
		$this->registerElement('Bottom', 'view/elements/common/');
		$this->setPageTitle('Đăng nhập');
	}	
	
	public function login()
	{
        if ($identity = HttpAuth::getInstance()->getIdentity()) {
            Context::getInstance()->getFront()->getResponse()->setRedirect('/admin/list-news-store');
        }
		$request	= Context::getInstance()->getFront()->getRequest();
		if (null == $request->getPost()){
			return;
		}
		
		$uname 			= $request->getPost('username');
		$upass 			= $request->getPost('password');
		$response 		= null;
		
		if (null === ($uname = trim($uname))){
			$response .= 'Bạn chưa điền tên đăng nhập<br/>';
		}
		
		if (null === $upass){
			$response .= 'Bạn chưa điền mật khẩu<br/>';
		}

		if (null === $response){
			$model = Context::getInstance()->getFront()->getModel('UserModel');
			if (null != ($user = $model->getUser(array('user_name' => $uname, 'user_password' => $upass), 1, 1))) {
				HttpAuth::getInstance()->setIdentity($user);
				$response = 'Đăng nhập thành công! Bạn vui lòng chờ giây lát ...<br/>
							<a href="/">Click vào đây để về trang chủ</a><br/>
							<a href="/">Click vào đây để về trang cá nhân</a>
							<script type="text/javascript">
								setTimeout(function(){
									document.location.href="/admin/list-news-store"
								}, 1000);
							</script>';
			}else{
				$response = 'Tài khoản này không tồn tại! Bạn vui lòng thử lại';
			}
		}
		
		$this->assign('response', $response);
	}
}