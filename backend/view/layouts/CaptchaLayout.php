<?php
class CaptchaLayout  extends Layout
{
	public function setup()
	{
		Context::getInstance()->load( 'lib/Captcha.php' );
		$captcha = new Captcha();
		$captcha->testLimit = 2;
		$captcha->backColor = 0xf5f5f5;
		$captcha->fontFile = Context::getInstance()->getBasePath(). 'public/css/fonts/UTM_Colossalis.ttf';
		$captcha->run();
		die;
	}	
}
 