<?php
class ListNewsStore extends Element{

    public function setup()
    {
        $this->_templateFile = 'ListNewsStore';
        $this->_templatePath = 'view/templates/admin/';
        $this->registerElement('ListNavigation', 'view/elements/common/');
        $front 		= Context::getInstance()->getFront();
        $request 	= $front->getRequest();

        //Lấy thông tin user từ session
        $identity	= HttpAuth::getInstance()->getIdentity();
        $this->assign('user', $identity);
        $pageUrl = '/admin/list-news-store/';

        if (HttpAuth::getInstance()->isAuthenticated()){
            if (null != $request->getPost()){
                $action	= $request->getPost('action');
                if ($action == 'filter'){
                    $param = array();
                    $titleLike	    = $request->getPost('titlelike');
                    $startDate	    = $request->getPost('startdate');
                    $startDate      = strtotime($startDate);
                    $endDate	    = $request->getPost('enddate');
                    $endDate        = strtotime($endDate);
                    if (!empty($endDate) && date('H', $endDate) == 0 && date('i', $endDate) == 0 && date('s', $endDate) == 0){
                        $endDate = mktime(23, 59, 59, date('m', $endDate), date('d', $endDate), date('Y', $endDate));
                    }
                    !empty($titleLike)      ? array_push($param, 'titlelike=' . $titleLike) : '';
                    !empty($startDate)      ? array_push($param, 'startDate=' . $startDate) : '';
                    !empty($endDate)        ? array_push($param, 'endDate=' . $endDate) : '';

                    $front->getResponse()->setRedirect($pageUrl . '?' . implode('&', $param));
                }
            }
        } else {
            $front->getResponse()->setRedirect('/dang-nhap');
            $front->getResponse()->sendHeaders();
        }

        $userModel = $front->getModel('UserModel');
        $this->assign('users', $userModel->getUser(array('status' => 1), 1, 50));

        $model = $front->getModel('NewsModel');

        //Lấy thông tin phân trang
        $pageIndex  = $request->getParts(2);
        $pageIndex	= $pageIndex < 1 ? 1 : $pageIndex;
        $this->assign('pageIndex', $pageIndex);

        $filter = array();
        $titleLike      = $request->getParam('titlelike');
        $startDate      = $request->getParam('startDate');
        $endDate        = $request->getParam('endDate');
        !empty($titleLike)     ? $filter['titlelike']      = $request->getParam('titlelike') : '';
        !empty($startDate)     ? $filter['startDate']      = $request->getParam('startDate') : '';
        !empty($endDate)       ? $filter['endDate']        = $request->getParam('endDate') : '';
        $this->assign('filter', $filter);
        //Implode array $filter to params tring
        $this->assign('ext', '?' . implode('&', array_map(function ($value, $key) { return $key . '=' . $value; }, $filter, array_keys($filter))));
        $this->assign('pageUrl', $pageUrl);

        //Get list tin theo quyền, filter...
        $this->assign('list', $model->getNews(array('field'				=> 'news_id, user_id, modified_user, approved_user, news_title, news_sapo, news_image, news_published_date, visit_count, news_status, source_name, news_slug',
                                                    'join_user' 		=> 1,
                                                    'join_status'		=> 1,
                                                    'join_position_rel'	=> 1,
                                                    'filter_user_id' 	=> $identity['user_id'],
                                                    'start_date' 		=> isset($this->filter['startDate'])   ? $this->filter['startDate'] : '',
                                                    'end_date'		    => isset($this->filter['endDate'])     ? $this->filter['endDate'] : '',
                                                    'title_like'		=> isset($this->filter['titlelike'])   ? $this->filter['titlelike'] : '',
                                                    'role_id' 			=> $identity['role_id'],
                                                    'status'			=> 4), $pageIndex, 10));
        $this->assign('count', $model->getCountNews(array(  'filter_user_id' 	=> $identity['user_id'],
                                                            'start_date' 		=> isset($this->filter['startDate'])   ? $this->filter['startDate'] : '',
                                                            'end_date'		    => isset($this->filter['endDate'])     ? $this->filter['endDate'] : '',
                                                            'title_like'		=> isset($this->filter['titlelike'])   ? $this->filter['titlelike'] : '',
                                                            'role_id' 			=> $identity['role_id'],
                                                            'status'			=> 4)));
        $front->getLayout()->setPageTitle('Kho lưu trữ')
                            ->setPageDescription('Kho lưu trữ');
    }
}
?>