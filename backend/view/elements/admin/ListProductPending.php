<?php
class ListProductPending extends Element{

    public function setup()
    {
        $this->_templateFile = 'ListProductPending';
        $this->_templatePath = 'view/templates/admin/';
        $this->registerElement('ListNavigation', 'view/elements/common/');
        $front = Context::getInstance()->getFront();
        $request = $front->getRequest();

        //Lấy thông tin user từ session
        $identity	= HttpAuth::getInstance()->getIdentity();
        $this->assign('user', $identity);
        $pageUrl = '/admin/list-product-pending/';

        if (HttpAuth::getInstance()->isAuthenticated()){
            if (null != $request->getPost()){
                $action	= $request->getPost('action');
                if ($action == 'filter'){
                    $param = array();
                    $typeId     = $request->getPost('typeId');
                    $userId 	= $request->getPost('userId');
                    $nameLike	= $request->getPost('namelike');
                    $startDate	= $request->getPost('startdate');
                    $startDate  = strtotime($startDate);
                    $endDate	= $request->getPost('enddate');
                    $endDate    = strtotime($endDate);
                    if (!empty($endDate) && date('H', $endDate) == 0 && date('i', $endDate) == 0 && date('s', $endDate) == 0){
                        $endDate = mktime(23, 59, 59, date('m', $endDate), date('d', $endDate), date('Y', $endDate));
                    }
                    !empty($typeId)     ? array_push($param, 'typeId=' . $typeId) : '';
                    !empty($userId)     ? array_push($param, 'userId=' . $userId) : '';
                    !empty($nameLike)   ? array_push($param, 'namelike=' . $nameLike) : '';
                    !empty($startDate)  ? array_push($param, 'startDate=' . $startDate) : '';
                    !empty($endDate)    ? array_push($param, 'endDate=' . $endDate) : '';

                    $front->getResponse()->setRedirect($pageUrl . '?' . implode('&', $param));
                }
            }
        } else {
            $front->getResponse()->setRedirect('/dang-nhap');
            $front->getResponse()->sendHeaders();
        }

        $model = $front->getModel('ProductModel');

        //Get list filter
        $userModel = $front->getModel('UserModel');
        $this->assign('users', $userModel->getUser(array('user_status' => 1), 1, 50));

        //$this->assign('listProductType', $model->getListProductType(array(), 1, 50));
        $model->treeOption(0, [$request->getParam('typeId')], $treeOption);
        $this->assign('treeOption', $treeOption);

        //Lấy thông tin phân trang
        $pageIndex  = $request->getParts(2);
        $pageIndex	= $pageIndex < 1 ? 1 : $pageIndex;
        $this->assign('pageIndex', $pageIndex);

        $filter = array();
        $typeId     = $request->getParam('typeId');
        $userId     = $request->getParam('userId');
        $nameLike   = $request->getParam('namelike');
        $startDate  = $request->getParam('startDate');
        $endDate    = $request->getParam('endDate');
        !empty($typeId)         ? $filter['typeId']     = $typeId : '';
        !empty($userId)         ? $filter['userId']     = $userId : '';
        !empty($nameLike)       ? $filter['namelike']   = $nameLike : '';
        !empty($startDate)      ? $filter['startDate']  = $startDate : '';
        !empty($endDate)        ? $filter['endDate']    = $endDate : '';
        $this->assign('filter', $filter);
        //Implode array $filter to params tring
        $this->assign('ext', '?' . implode('&', array_map(function ($value, $key) { return $key . '=' . $value; }, $filter, array_keys($filter))));
        $this->assign('pageUrl', $pageUrl);

        //Get list tin theo quyền, filter...
        $this->assign('list', $model->getListProduct(array( 'field'			=> 'id, user_id, modified_user, name, image, code, short_description, price, type_id, created_at',
                                                            'join_user'     => 1,
                                                            'join_type'     => 1,
                                                            'type_id' 		=> isset($this->filter['typeId'])       ? $this->filter['typeId'] : '',
                                                            'user_id' 	    => isset($this->filter['user_id'])      ? $this->filter['user_id'] : '',
                                                            'approved_user' => isset($this->filter['approvedUser']) ? $this->filter['approvedUser'] : '',
                                                            'start_date' 	=> isset($this->filter['startDate'])    ? $this->filter['startDate'] : '',
                                                            'end_date'		=> isset($this->filter['endDate'])      ? $this->filter['endDate'] : '',
                                                            'name_like'		=> isset($this->filter['namelike'])     ? $this->filter['namelike'] : '',
                                                            'status'		=> 0), $pageIndex, 10));

        $front->getLayout()->setPageTitle('Sản phẩm đã chờ duyệt');
    }
}
?>