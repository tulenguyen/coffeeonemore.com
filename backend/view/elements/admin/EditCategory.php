<?php

class EditCategory extends Element {

    public function setup() {
        $this->setTemplatePath('view/templates/admin/')
                ->setTemplateFile('EditCategory');

        //Lấy thông tin user từ session
        $identity = HttpAuth::getInstance()->getIdentity();
        $this->assign('user', $identity);

        $front = Context::getInstance()->getFront();
        $request = $front->getRequest();
        if (HttpAuth::getInstance()->isAuthenticated()) {
            if (null != $request->getPost()) {
                $this->update($identity['user_id']);
            }
        } else {
            $front->getResponse()->setRedirect('/dang-nhap');
            $front->getResponse()->sendHeaders();
        }

        $parentId = 0;
        $model = $front->getModel('CategoryModel');
        if (($id = Context::getInstance()->getRoute()->getParam('id')) > 0) {
            $this->assign('category', $model->getListCategory(array('category_id' => $id)));
            $parentId = $this->category['parent_id'];
        }

        //Danh sách chuyên mục
        $model->treeView(0, array(), '/admin/edit-category', $treeView, '');
        $this->assign('treeView', $treeView);

        //Chọn cat cha
        $model->treeOption(0, array($parentId), $treeOption, '');
        $this->assign('treeOption', $treeOption);
    }

    public function update($userId) {
        $request = Context::getInstance()->getFront()->getRequest();
        $categoryName = $request->getPost('categoryName');
        $categorySlug = $request->getPost('categorySlug');
        $categoryRedirectLink = $request->getPost('categoryRedirectLink');
        $categoryTitle = $request->getPost('categoryTitle');
        $categoryKeyword = $request->getPost('categoryKeyword');
        $categoryDescription = $request->getPost('categoryDescription');
        $categoryStatus = $request->getPost('categoryStatus');
        $categoryTemplate = $request->getPost('categoryTemplate');
        $categoryHeaderOrder = $request->getPost('categoryHeaderOrder');
        $categoryHomeOrder = $request->getPost('categoryHomeOrder');
        $categoryFooterOrder = $request->getPost('categoryFooterOrder');
        $categoryCmsOrder = $request->getPost('categoryCmsOrder');
        $parentId = $request->getPost('parentId');

        if (!empty($categoryRedirectLink) && stripos($categoryRedirectLink, 'http://') !== false) {
            $isOutside = 1;
        }

        $categoryId = Context::getInstance()->getRoute()->getParam('id');
        $uploadPath = Helper::getInstance()->getConfig('ftp_upload_path');
        $image = Transfer::getInstance()->curlFtpUpload('image', $uploadPath);

        $model = Context::getInstance()->getFront()->getModel('CategoryModel');
        $historyModel = Context::getInstance()->getFront()->getModel('HistoryModel');

        if (!empty($categoryName)) {
            if ($categoryId > 0) {
               
                $model->updateCategory(array('user_id' => $userId,
                    'category_name' => $categoryName,
                    'category_slug' => !empty($categorySlug) ? $categorySlug : Util::toUnsign($categoryName),
                    'category_title' => $categoryTitle,
                    'category_keyword' => $categoryKeyword,
                    'category_description' => $categoryDescription,
                    'category_image' => $image['src'],
                    'category_status' => $categoryStatus,
                    'category_template' => $categoryTemplate,
                    'category_header_order' => $categoryHeaderOrder,
                    'category_home_order' => $categoryHomeOrder,
                    'category_footer_order' => $categoryFooterOrder,
                    'category_cms_order' => $categoryCmsOrder,
                    'parent_id' => $parentId,
                    'is_outside' => isset($isOutside) ? $isOutside : 0,
                    'category_redirect_link' => $categoryRedirectLink,
                    'category_id' => $categoryId));
                $historyModel->InsertHistory(array('history_name' => 'UpdateCategory',
                    'history_time' => strtotime(date('Y-m-d H:i:s')),
                    'route_id' => 'admin/edit-category',
                    'object_id' => $categoryId,
                    'object_name' => 'Category',
                    'user_id' => $userId));
            } else {
                $id = $model->insertCategory(array('user_id' => $userId,
                    'category_name' => $categoryName,
                    'category_slug' => !empty($categorySlug) ? $categorySlug : Util::toUnsign($categoryName),
                    'category_title' => $categoryTitle,
                    'category_keyword' => $categoryKeyword,
                    'category_description' => $categoryDescription,
                    'category_image' => $image['src'],
                    'category_status' => $categoryStatus,
                    'category_template' => $categoryTemplate,
                    'category_header_order' => $categoryHeaderOrder,
                    'category_home_order' => $categoryHomeOrder,
                    'category_footer_order' => $categoryFooterOrder,
                    'category_cms_order' => $categoryCmsOrder,
                    'parent_id' => $parentId,
                    'is_outside' => isset($isOutside) ? $isOutside : 0,
                    'category_redirect_link' => $categoryRedirectLink));
                $historyModel->InsertHistory(array('history_name' => 'InsertCategory',
                    'history_time' => strtotime(date('Y-m-d H:i:s')),
                    'route_id' => 'admin/edit-category',
                    'object_id' => $id,
                    'object_name' => 'Category',
                    'user_id' => $userId));
            }

            $this->assign('error', 'Cập nhật thành công!');
        } else {
            $this->assign('error', 'Bạn chưa điền tên chuyên mục!');
        }
    }

}

?>
