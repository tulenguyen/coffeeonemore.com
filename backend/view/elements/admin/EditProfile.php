<?php
class EditProfile  extends Element{
	
	public function setup()
	{
		$this->setTemplatePath('view/templates/admin/')
			 ->setTemplateFile('EditProfile');
		
		$front = Context::getInstance()->getFront();
		$request = $front->getRequest();
		$identity	= HttpAuth::getInstance()->getIdentity();
		
		if (HttpAuth::getInstance()->isAuthenticated()){
			if (null != $request->getPost()){
				if ($request->getPost('action') == 'update'){
					$this->UpdateUser($identity);
				}
			}
		} else {
			$front->getResponse()->setRedirect('/dang-nhap');
			$front->getResponse()->sendHeaders();
		}
		
		$model 		= $front->getModel('UserModel');
		if (($id	= $identity['user_id']) > 0){
            $user = $model->getUser(array('user_id' => $id));
			if (empty($user)){
				$front->getResponse()->setRedirect('/admin/edit-user');
			} else {
				$user['role_name'] = $identity['role_name'];
				$this->assign('user', $user);
			}
		}
	}
	
	public function updateUser($identity)
	{
		$front 		    = Context::getInstance()->getFront();
		$request 	    = $front->getRequest();
		$email 		    = $request->getPost('user_email');
		$pass		    = $request->getPost('user_password');
		$fullname	    = $request->getPost('user_fullname');
		$phone		    = $request->getPost('user_phone');
		$address	    = $request->getPost('user_address');
        $uploadPath	    = Helper::getInstance()->getConfig('ftp_upload_path');
        $avatar 	    = Transfer::getInstance()->curlFtpUpload('image', $uploadPath);
		
		$id			    = $identity['user_id'];
		$model 		    = $front->getModel('UserModel');
		$historyModel   = $front->getModel('HistoryModel');
		
		$err = $this->isOk($email, $pass);
		if (isset($err) && strlen($err) > 0){
			$this->assign('error', $err);
		} else {
			//Update vào bảng user
			$model->updateUser(array( 'user_password'	=> $pass,
                                      'user_avatar'	    => $avatar['src'],
									  'user_fullname'	=> $fullname,
									  'user_email'		=> $email,
									  'user_phone'		=> $phone,
									  'user_address'	=> $address,
									  'user_id'			=> $id));
			//Thêm sự kiện vào bảng history
			$historyModel->InsertHistory(array( 'history_name' 	=> 'UpdateProfile',
												'history_time' 	=> strtotime(date('Y-m-d H:i:s')),
												'route_id' 		=> 'admin/edit-profile',
												'object_id' 	=> $id,
												'object_name' 	=> 'User',
												'user_id' 		=> $id));
			$done = true;
			$this->assign('error', $done ? 'Thành công!' : (isset($err) ? $err : 'Có lỗi xảy ra, bạn vui lòng kiểm tra lại'));
		}
	}
	
	private function isOk($email, $pass)
	{
		$err = '';
		if (!empty($email)){
			if (!preg_match('#^[\w\d\-\_\.]+@[\d\w\-\_]+(?:\.[\w\d\-\_]+){1,3}$#is', $email)){
				$err .= 'Email không hợp lệ! ';
			}
		} else {
			$err .= 'Email không được bỏ trống!';
		}
		if (!empty($pass)){
			if (strlen(preg_replace('#[a-z0-9]+$#is', '', $pass)) > 0 || strlen($pass) <= 6){
				$err .= 'Mật khẩu chỉ được chữ và số và lớn hơn 6 ký tự!';
			}
		}
		return $err;
	}
}
?>
