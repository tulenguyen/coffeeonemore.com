<?php
class EditNews extends Element
{
    public function setup()
    {
        $this->_templateFile = 'EditNews';
        $this->_templatePath = 'view/templates/admin/';
        $front = Context::getInstance()->getFront();
        $request = $front->getRequest();
        //Lấy thông tin user từ session
        $identity = HttpAuth::getInstance()->getIdentity();
        $this->assign('user', $identity);

        if (HttpAuth::getInstance()->isAuthenticated()) {
            if (null != $request->getPost()) {
                $action = $request->getPost('action');
                if ($action == 'search') {
                    $this->searchNews();
                }  else if ($action == 'add' || $action == 'update') {
                    $this->updateNews();
                }
               
            }
        } else {
            $front->getResponse()->setRedirect('/dang-nhap');
            $front->getResponse()->sendHeaders();
        }

        $newsModel = $front->getModel('NewsModel');
        if (($id = Context::getInstance()->getRoute()->getParam('id')) > 0) {
            //Lấy tin theo Id
            $news = $newsModel->getNews([
                    'join_status'   => 1,
                    'join_user'     => 1,
                    'news_id'       => $id,
                    'role_id'       => $identity['role_id'],
                    'user_id'       => $identity['user_id']
                ]
            );
            $this->assign('news', $news);
            if (!isset($news['news_id'])) {
                $front->getResponse()->setRedirect('/admin/edit-news');
            }
            $listNewsRelation = isset($news['news_relation']) ? $newsModel->getNews(['field' => 'n.news_id, n.news_title', 'news_ids' => $news['news_relation']]) : null;
            $this->assign('newsRelation', !empty($listNewsRelation) ? json_encode($listNewsRelation, true) : '');

            $title  = (isset($news['news_title']) ? $news['news_title'] : '');
        } else {
            $title  = 'Thêm tin mới';
        }

        //Cay chuyen muc
        $catModel = $front->getModel('CategoryModel');
        $catModel->treeOption(0, [isset($news['category_id']) ? $news['category_id'] : 0], $treeOption, '', ['category_status' => 1]);
        $this->assign('treeOption', $treeOption);

        Context::getInstance()->getFront()->getLayout()->setPageTitle($title);
    }

    //Bắt sự kiện update hoặc insert tin
    public function updateNews()
    {
        $front                  = Context::getInstance()->getFront();
        $request                = $front->getRequest();
        $identity               = HttpAuth::getInstance()->getIdentity();

        $newsTitle              = $request->getPost('newsTitle');
        $newsSapo               = $request->getPost('newsSapo');

        $content                = Util::removeTag($request->getPost('newsContent'), 'html|title|head|body');
        $content                = html_entity_decode($content, ENT_QUOTES, "UTF-8");
        $content                = preg_replace('#<div[^>]*>\s*</div>#is', '', $content);

        $newsTag                = $request->getPost('newsTag');
        $newsPublishDate        = $request->getPost('newsPublishDate');
        $categoryId             = $request->getPost('categoryId');
        $newsRelation           = $request->getPost('newsRelationId');
        $newsSeoTitle           = $request->getPost('newsSeoTitle');
        $newsSeoDescription     = $request->getPost('newsSeoDescription');
        $newsSeoKeyword         = $request->getPost('newsSeoKeyword');

        $newsImageUrl           = $request->getPost('newsImageUrl');
        if (!empty($newsImageUrl) && strpos($newsImageUrl, 'http') !== false) {
            $imageSrc = $newsImageUrl;
        } else {
            $imageSrc           = $request->getPost('imgSrc');
        }

        $isVideo                = $request->getPost('isVideo');
        $newsStatus             = $request->getPost('newsStatus');
        $newsSlug               = $request->getPost('newsSlug');
        $newsRedirectLink       = $request->getPost('newsRedirectLink');
        if (!empty($link) && stripos($link, 'http://') !== false){
            $isOutside = 1;
        }
        $sourceName             = $request->getPost('sourceName');

        $newsId                 = Context::getInstance()->getRoute()->getParam('id');
        $newsModel              = $front->getModel('NewsModel');
        $historyModel           = $front->getModel('HistoryModel');

        if (!empty($content) && (stripos($content, '.mp4') !== false || stripos($content, 'youtube') !== false)){
            $isVideo = 1;
        }
        if ($newsId > 0) {
            //Update vào bảng news
            $newsModel->updateNews([
                'modified_user'         => $identity['user_id'],
                'news_title'            => str_replace('"', '\'', $newsTitle),
                'news_sapo'             => $newsSapo,
                'news_content'          => $content,
                'news_tag_slug'         => Util::toUnsign($newsTag),
                'news_tag'              => $newsTag,
                'news_relation'         => $newsRelation,
                'news_published_date'   => strtotime($newsPublishDate),
                'news_modified_date'    => strtotime(date('Y-m-d H:i:s')),
                'category_id'           => $categoryId,
                'news_image'            => $imageSrc,
                'is_video'              => $isVideo,
                'news_seo_title'        => $newsSeoTitle,
                'news_seo_description'  => $newsSeoDescription,
                'news_seo_keyword'      => $newsSeoKeyword,
                'source_name'           => $sourceName,
                'is_outside'            => 0,
                'news_redirect_link'    => $newsRedirectLink,
                'news_slug'             => isset($newsSlug) && !empty($newsSlug) ? $newsSlug : Util::toUnsign($newsTitle),
                'news_id'               => $newsId
            ]);
            //Thêm sự kiện vào bảng history
            $historyModel->InsertHistory([
                    'history_name'  => 'UpdateNews',
                    'history_time'  => strtotime(date('Y-m-d H:i:s')),
                    'route_id'      => 'admin/edit-news',
                    'object_id'     => $newsId,
                    'object_name'   => 'News',
                    'user_id'       => $identity['user_id']
                ]
            );

            $url = Helper::getInstance()->getConfig('domain') . 'managecache?action=request' . '&href=/admin/edit-news/' . $newsId . '&categoryId=' . $categoryId;
            Transfer::getInstance()->getContent($url, Helper::getInstance()->getConfig('domain'), true, ['_cookie_refer' => 'cúc cu'], null, 1);

            $done = true;
        } else {
            //Insert tin vào bảng news
            $id = $newsModel->insertNews([
                    'user_id'               => $identity['user_id'],
                    'modified_user'         => $identity['user_id'],
                    'approved_user'         => $identity['user_id'],
                    'news_title'            => str_replace('"', '\'', $newsTitle),
                    'news_sapo'             => $newsSapo,
                    'news_content'          => $content,
                    'news_tag_slug'         => Util::toUnsign($newsTag),
                    'news_tag'              => $newsTag,
                    'news_relation'         => $newsRelation,
                    'news_published_date'   => strtotime($newsPublishDate),
                    'news_modified_date'    => strtotime(date('Y-m-d H:i:s')),
                    'category_id'           => $categoryId,
                    'news_image'            => $imageSrc,
                    'is_video'              => $isVideo,
                    'news_seo_title'        => $newsSeoTitle,
                    'news_seo_description'  => $newsSeoDescription,
                    'news_seo_keyword'      => $newsSeoKeyword,
                    'source_name'           => $sourceName,
                    'is_outside'            => 0,
                    'news_redirect_link'    => $newsRedirectLink,
                    'news_slug'             => isset($newsSlug) && !empty($newsSlug) ? $newsSlug : Util::toUnsign($newsTitle),
                    'news_status'           => 4
                ]
            );
            //Thêm sự kiện vào bảng history
            $historyModel->InsertHistory([
                'history_name'  => 'InsertNews',
                'history_time'  => strtotime(date('Y-m-d H:i:s')),
                'route_id'      => 'admin/edit-news',
                'object_id'     => $id,
                'object_name'   => 'News',
                'user_id'       => $identity['user_id']
            ]);
            $done = true;
        }
        $this->assign('error', $done ? 'Cập nhật thành công' : 'Có lỗi xảy ra, bạn vui lòng kiểm tra lại');
        if ($done){
            switch($newsStatus){
                case 0:
                    $url = '/admin/list-news-delete';
                    break;
                case 1:
                    $url = '/admin/list-news-publish';
                    break;
                case 2:
                    $url = '/admin/list-news-pending';
                    break;
                case 3:
                    $url = '/admin/list-news-unapproved';
                    break;
                case 4:
                    $url = '/admin/list-news-store';
                    break;
                default:
                    $url = '/admin/list-news-store';
                    break;
            }
            Context::getInstance()->getFront()->getResponse()->setRedirect($url);
        }
    }
    
    public function searchNews() {
        $title = Context::getInstance()->getFront()->getRequest()->getParam('keyword');
        $pageIndex = Context::getInstance()->getFront()->getRequest()->getParam('page');
        $pageIndex = is_numeric($pageIndex) && $pageIndex > 1 ? $pageIndex : 1;
        Context::getInstance()->getFront()->getResponse()->setHeader('Content-Type', 'Application/json');
        Context::getInstance()->getFront()->getResponse()->sendHeaders();
        $model = Context::getInstance()->getFront()->getModel('NewsModel');
        $records = $model->getNews(array('field' => 'news_id, news_title, news_image, news_sapo, news_status, news_slug', 'status' => 1, 'title_like' => $title), $pageIndex);

        if (!empty($records)) {
            foreach ($records as &$item) {
                if (!isset($item['link_web'])) {
                    $item['link_web'] = '/';
                }
            }
        }
        echo json_encode($records);
        die;
    }
}
?>