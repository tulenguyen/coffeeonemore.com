<?php
class UploadVideo extends Element{
	public function setup()
	{
		$identity	= HttpAuth::getInstance()->getIdentity();
		$request 	= Context::getInstance()->getFront()->getRequest();
		if ($userId	= $identity['user_id']){
			if ($request->getParam('action') == 'search'){
				$pageIndex 	= $request->getParam('page');
				$pageIndex  = $pageIndex > 1 ? $pageIndex  : 1;
				$fileName 	= $request->getParam('keyword');
				Context::getInstance()->getFront()->getResponse()->setHeader('Content-Type', 'application/json');
                $model 		= Context::getInstance()->getFront()->getModel('VideoModel');
				echo json_encode($model->getVideos(array('title_like' => '%'. $fileName. '%'), $pageIndex, 10));
				die;
			}
			
			set_time_limit(0);
		
			$done		    = false;
			$name 		    = $request->getPost('video_name');
			$desc 		    = $request->getPost('video_description');
			$active 	    = $request->getPost('status') ? 1 : 0;
			$focus		    = $request->getPost('video_focus');
			$id			    = $request->getPost('video_id');
			$source		    = $request->getPost('video_source');
			$publishdate 	= $request->getPost('publishdate');
			$catId			= $request->getPost('category_id');
			$uploadPath		= Helper::getInstance()->getConfig('ftp_upload_path');
			$ftpUloadPath 	= Helper::getInstance()->getConfig('ftp_upload_video');
			$video 			= Transfer::getInstance()->ftpUpload('file', $ftpUloadPath, true, 122400000);
			$avatar			= Transfer::getInstance()->curlFtpUpload('avatar', $uploadPath);
			$source         = !empty($source) && $source > 0 ? $source : 0;
			$catId 	        = !empty($catId) && $catId > 0 ? $catId : 1;
            $model 		    = Context::getInstance()->getFront()->getModel('VideoModel');
            $historyModel 	= Context::getInstance()->getFront()->getModel('HistoryModel');

			if (isset($video['src']) && !empty($video['src']) && stripos($video['src'], '.mp4') !== false){
                if (intval($id) > 0){
                    $model->updateVideo(array(	'name'				=> $name,
                                                'type'				=> isset($video['src']) ? strtolower(@end(explode('.', $video['src']))) : '',
                                                'size'				=> isset($video['size'])? $video['size'] : 0,
                                                'src'				=> isset($video['src']) ? $video['src'] : '',
                                                'avatar'			=> isset($avatar['src']) ? $avatar['src'] : '',
                                                'video_focus'		=> $focus,
                                                'active'			=> $active,
                                                'category_id'		=> $catId,
                                                'video_posted_date' => $publishdate ? strtotime($publishdate) : time(),
                                                'description'		=> $desc,
                                                'source'			=> $source,
                                                'user_id'			=> $identity['user_id'],
                                                'id'				=> $id));
                    //Thêm sự kiện vào bảng history
                    $historyModel->InsertHistory(array(	'history_name' 	=> 'UpdateVideo',
                                                        'history_time' 	=> strtotime(date('Y-m-d H:i:s')),
                                                        'route_id' 		=> 'admin/edit-video',
                                                        'object_id' 	=> $id,
                                                        'object_name' 	=> 'Video',
                                                        'user_id' 		=> $identity['user_id']));

                } else {
                    $id = $model->addVideo(array(	'name' 				=> $name,
                                                    'type'				=> isset($video['src']) ? strtolower(@end(explode('.', $video['src']))) : '',
                                                    'size'				=> isset($video['size'])? $video['size'] : 0,
                                                    'src'				=> isset($video['src']) ? $video['src'] : '',
                                                    'avatar'			=> isset($avatar['src']) ? $avatar['src'] : '',
                                                    'video_focus'		=> $focus,
                                                    'active'			=> $active,
                                                    'category_id'		=> $catId,
                                                    'video_posted_date' => $publishdate ? strtotime($publishdate) : time(),
                                                    'description'		=> $desc,
                                                    'source'			=> $source,
                                                    'user_id'			=> $identity['user_id']));
                    //Thêm sự kiện vào bảng history
                    $historyModel->InsertHistory(array(	'history_name' 	=> 'InsertVideo',
                                                        'history_time' 	=> strtotime(date('Y-m-d H:i:s')),
                                                        'route_id' 		=> 'admin/edit-video',
                                                        'object_id' 	=> $id,
                                                        'object_name' 	=> 'Video',
                                                        'user_id' 		=> $identity['user_id']));
                }
                Context::getInstance()->getFront()->getResponse()->setRedirect('/admin/edit-video/' . $id);
            }
		}
	}
}
?>
