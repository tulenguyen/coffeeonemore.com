<?php
class EditRoute extends Element{
	
	public function setup()
	{
		$this->setTemplatePath('view/templates/admin/')
			 ->setTemplateFile('EditRoute');
		$front = Context::getInstance()->getFront();
		
		//Lấy thông tin user từ session
		$identity	= HttpAuth::getInstance()->getIdentity();
		$this->assign('user', $identity);
		
		$request = $front->getRequest();
		if (HttpAuth::getInstance()->isAuthenticated()){
			if (null != $request->getPost()){
				$action	= $request->getPost('action');
				if ($action == 'add' || $action == 'update'){
					$this->UpdateRoute($identity['user_id']);
				} elseif ($action == 'delete'){
					$this->DeleteRoute($identity['user_id']);
				}
			}
		} else {
			$front->getResponse()->setRedirect('/dang-nhap');
			$front->getResponse()->sendHeaders();
		}
		
		$model 		= $front->getModel('RouteModel');
		if (($id	= Context::getInstance()->getRoute()->getParam('id')) > 0){
			$this->assign('route', $model->getRoutes(array('route_id' => $id)));
		}

        $this->assign('routes', $model->getRoutes());
	}

	public function UpdateRoute($userId)
	{
		$done		= false;
		$front 		= Context::getInstance()->getFront();
		$request 	= $front->getRequest();
		$route		= Context::getInstance()->getRoute();
		$pattern 	= $request->getPost('route_pattern');

		$id				= $route->getParam('id');
		$model 			= $front->getModel('RouteModel');
		$historyModel 	= $front->getModel('HistoryModel');
		
		if (null != ($pattern = trim($pattern))){
			if ($id > 0){
				$model->updateRoute(array('pattern'		=> $pattern,
										  'id'			=> $id));
				//Thêm sự kiện vào bảng history
				$historyModel->InsertHistory(array(	'history_name' 	=> 'UpdateRoute',
													'history_time' 	=> strtotime(date('Y-m-d H:i:s')),
													'route_id' 		=> 'admin/edit-route',
													'object_id' 	=> $id,
													'object_name' 	=> 'Route',
													'user_id' 		=> $userId));
				$done = true;
			} else {
				$id = $model->addRoute(array('pattern'		=> $pattern));
				//Thêm sự kiện vào bảng history
				$historyModel->InsertHistory(array(	'history_name' 	=> 'InsertRoute',
													'history_time' 	=> strtotime(date('Y-m-d H:i:s')),
													'route_id' 		=> 'admin/edit-route',
													'object_id' 	=> $id,
													'object_name' 	=> 'Route',
													'user_id' 		=> $userId));
				$done = true;
			}
			
			$this->assign('error', $done ? 'Cập nhật thành công!' : 'Có lỗi xảy ra, bạn vui lòng kiểm tra lại');
			
		} else {
			$this->assign('error', 'Bạn chưa điền biểu mẫu phân vùng!');
		}
		
	}
}
?>
