<?php
class ViewHistory extends Element{
	
	public function setup()
	{
		$this->_templateFile = 'ViewHistory';
		$this->_templatePath = 'view/templates/admin/';
		
		$front = Context::getInstance()->getFront();
		$request = $front->getRequest();
		
		//Lấy thông tin user từ session
		$identity	= HttpAuth::getInstance()->getIdentity();
		$this->assign('user', $identity);
		
		if (HttpAuth::getInstance()->isAuthenticated()){
			if (null != $request->getPost()){
				$action	= $request->getPost('action');
				
			}
		} else {
			$front->getResponse()->setRedirect('/dang-nhap');
			$front->getResponse()->sendHeaders();
		}

		//Lấy thông tin phân trang
		$pageIndex  = $request->getParts(2);
		$pageIndex	= $pageIndex < 1 ? 1 : $pageIndex;
		$this->assign('pageIndex', $pageIndex);
		//Get list tin theo quyền, filter...
		$model = $front->getModel('HistoryModel');
		$this->assign('list', $model->GetListHistory(array(	'filter' 		=> $request->getParam('filter'), 
															'join_route' 	=> 1,
															'join_user' 	=> 1), 
															$pageIndex, 10));
		foreach ($this->list as &$item){
			switch($item['object_name']){
				case 'News':
					$model = $front->getModel('NewsModel');
                    $record = $model->getNews(array('news_id' => $item['object_id']));
					$item['object_detail'] = '<a href="/admin/edit-news/' . $record['news_id'] . '">' . $record['news_title'] . '</a>';
					break;
				case 'Category':
					$model = $front->getModel('CategoryModel');
					$record = $model->getCategoryById($item['object_id']);
					$item['object_detail'] = '<a href="/admin/edit-category/' . $record['category_id'] . '">' . $record['category_name'] . '</a>';
					break;
				case 'Video':
					$model = $front->getModel('VideoModel');
					$record = $model->getVideoById($item['object_id']);
					$item['object_detail'] = '<a href="/admin/edit-video/' . $record['video_id'] . '">' . $record['video_name'] . '</a>';
					break;
				case 'Audio':
					$model = $front->getModel('AudioModel');
					$record = $model->getAudioById($item['object_id']);
					$item['object_detail'] = '<a href="/admin/edit-audio/' . $record['audio_id'] . '">' . $record['audio_name'] . '</a>';
					break;
				case 'Route':
					$model = $front->getModel('RouteModel');
					$record = $model->getRouteById($item['object_id']);
					$item['object_detail'] = '<a href="/admin/edit-route/' . $record['route_id'] . '">' . $record['route_pattern'] . '</a>';
					break;
				case 'User':
					$model = $front->getModel('UserModel');
					$record = $model->getUserById($item['object_id']);
					$item['object_detail'] = '<a href="/admin/edit-user/' . $record['user_id'] . '">' . $record['user_name'] . '</a>';
					break;
				case 'Royalty':
					$model = $front->getModel('RoyaltyModel');
					$record = $model->GetListRoyalty(array('royalty_id' => $item['object_id']));
					$item['object_detail'] = '<a href="/admin/edit-royalty/' . $record['royalty_id'] . '">' . $record['royalty_name'] . '</a>';
					break;
				case 'Mistake':
					$model = $front->getModel('MistakeModel');
					$record = $model->GetListMistake(array('mistake_id' => $item['object_id']));
					$item['object_detail'] = '<a href="/admin/edit-mistake/' . $record['mistake_id'] . '">' . $record['mistake_name'] . '</a>';
					break;
				case 'Quality':
					$model = $front->getModel('QualityModel');
					$record = $model->GetListQuality(array('quality_id' => $item['object_id']));
					$item['object_detail'] = '<a href="/admin/edit-quality/' . $record['quality_id'] . '">' . $record['quality_name'] . '</a>';
					break;
				case 'Advertise':
					$model = $front->getModel('AdvertiseModel');
					$record = $model->getAdvertiseById($item['object_id']);
					$item['object_detail'] = '<a href="/admin/edit-advertise/' . $record['advertise_id'] . '">' . $record['advertise_title'] . '</a>';
					break;
				case 'Poll':
					$model = $front->getModel('PollModel');
					$record = $model->GetListPoll(array('poll_id' => $item['object_id']));
					$item['object_detail'] = '<a href="/admin/edit-poll/' . $record['poll_id'] . '">' . $record['poll_title'] . '</a>';
					break;
				case 'PollAnswer':
					$model = $front->getModel('PollModel');
					$record = $model->GetPollAnswer(array('answer_id' => $item['object_id']));
					$item['object_detail'] = '<a href="/admin/edit-poll-answer/' . $record['poll_id'] . '/' . $record['answer_id'] . '">' . $record['answer_title'] . '</a>';
					break;
				default: break;
			}
		}
		
		$front->getLayout()->setPageTitle('Lịch sử hoạt động')
							->setPageDescription('Lịch sử hoạt động')
							->setPageKeywords('');
	
	}
}
?>