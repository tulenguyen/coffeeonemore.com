<?php
class UploadManager extends Element{
    public function setup()
    {
    	$this->setTemplatePath('view/templates/admin/')
			 ->setTemplateFile('UploadManager');
			 
        if (HttpAuth::getInstance()->isAuthenticated()){
            $this->indexAction();
        }else{
        	Context::getInstance()->getFront()->getResponse()->setRedirect('/');
        	Context::getInstance()->getFront()->getResponse()->sendHeaders();
        }
    }
    
    public function getFolderAction()
    {
    	$request 	= Context::getInstance()->getFront()->getRequest();
    	$pid		= $request->getPost('pid');
    	$model  	= Context::getInstance()->getFront()->getModel('FileModel');
    	
    	if ($items	= $model->getFolder(array('parent_id' => $pid ? $pid : 0, 0))){
    		foreach ($items as &$item){
    			$item['count'] = $model->countFolder(array('parent_id' => $item['folder_id'], 0)); 
    		}
    	}
    	echo json_encode($items, false); die;
    }

    public function deleteFolderAction(){
        $request 	= Context::getInstance()->getFront()->getRequest();
        $pid		= $request->getPost('pid');
        $model  	= Context::getInstance()->getFront()->getModel('FileModel');
        $model->deleteFolder($pid);
        echo json_encode('true');die;
    }
    
    public function getFileAction()
    {
    	$request 	= Context::getInstance()->getFront()->getRequest();
    	$fid		= $request->getPost('fid');
    	$model  	= Context::getInstance()->getFront()->getModel('FileModel');
    	
    	$images		= $model->getImage(array('folder_id' => $fid), 1, 50);
    	$videos		= $model->getVideo(array('folder_id' => $fid), 1, 50);
    	
    	echo json_encode(array( 'video'     => $videos,
                                'image'     => $images,
                                'imageIds'  => isset($_SESSION['justUploadImage']) && !empty($_SESSION['justUploadImage']) ? ','. implode(',', $_SESSION['justUploadImage']). ',' : '0',
                                'videoIds'  => isset($_SESSION['justUploadVideo']) && !empty($_SESSION['justUploadVideo']) ? ','. implode(',', $_SESSION['justUploadVideo']) . ',' : '0'), true); die;
    }
    
    public function createFolderAction()
    {
    	$result		= false;
    	$identity	= HttpAuth::getInstance()->getIdentity();
	    if (isset($identity['user_id']) && $userId = $identity['user_id']){
	    	$request 	= Context::getInstance()->getFront()->getRequest();
	    	$pid		= $request->getPost('pid');
	    	$name	 	= trim($request->getPost('name'));
	    	$model  	= Context::getInstance()->getFront()->getModel('FileModel');
	    	$items		= array('folder_name' => $name, 'parent_id' => $pid, 'user_id' => $userId);
	    	$result		= $model->insertFolder($items);
    	}
    	echo json_encode(array('ok' => $result, 'data' => null), true); die;
    }

    public function uploadAction()
    {
        $identity	= HttpAuth::getInstance()->getIdentity();

        if (isset($identity['user_id']) && $userId = $identity['user_id']){
            $request 	= Context::getInstance()->getFront()->getRequest();
            $uploadPath	= Helper::getInstance()->getConfig('ftp_upload_path');
            $file 		= Transfer::getInstance()->curlFtpUpload('uploadfile', $uploadPath);
            $title 		= $request->getPost('name');

            $folderId	= $request->getPost('folder_id');

            $justUploadVideo = array();
            $justUploadImage = array();

            if (isset($file['src']) && !empty($file['src'])){
                if (empty($title) && isset($file['name']) && !empty($file['name'])){
                    $title = $file['name'];
                }

                if (preg_match('#\.(mp4|mp3|flv)$#is', $file['src'])){
                    $model 		= Context::getInstance()->getFront()->getModel('VideoModel');

                    $id			= $model->addVideo(array(	'name'				=> $title,
                                                            'type'				=> isset($file['src']) 	? strtolower(@end(explode('.', $file['src']))) : '',
                                                            'size'				=> isset($file['size'])	? $file['size'] : 0,
                                                            'src'				=> isset($file['src']) ? $file['src'] : '',
                                                            'folder_id'			=> $folderId,
                                                            'avatar'			=> '',
                                                            'video_focus'		=> '',
                                                            'active'			=> '',
                                                            'category_id'		=> '',
                                                            'video_posted_date' => time(),
                                                            'description'		=> '',
                                                            'source'			=> '',
                                                            'user_id'			=> $userId));
                    array_push( $justUploadVideo, $id);


                }elseif(preg_match('#\.(jpg|jpeg|png|gif|bmp)$#is', $file['src'])){
                    $model 		= Context::getInstance()->getFront()->getModel('ImageModel');
                    $id			= $model->InsertImage(array('user_id' 			=> $userId,
                                                            'folder_id'			=> $folderId,
                                                            'image_title' 		=> $title,
                                                            'image_src' 		=> Helper::getInstance()->getConfig('image_url') . $file['src'],
                                                            'image_size' 		=> $file['size'],
                                                            'image_status' 		=> 1,
                                                            'image_posted_date' => time()));
                    array_push( $justUploadImage, $id);

                }
            } elseif (isset($file[0]['src'])){
                foreach ($file as $item){
                    if (empty($title) && isset($item['name']) && !empty($item['name'])){
                        $title = $item['name'];
                    }

                    if (preg_match('#\.(mp4|mp3|flv)$#is', $item['src'])){
                        $model 		= Context::getInstance()->getFront()->getModel('VideoModel');

                        $id			= $model->addVideo(array(	'name'			    => $title,
                                                                'type'				=> isset($item['src']) 	? strtolower(@end(explode('.', $item['src']))) : '',
                                                                'size'				=> isset($item['size'])	? $item['size'] : 0,
                                                                'src'				=> isset($item['src']) ? $item['src'] : '',
                                                                'folder_id'			=> $folderId,
                                                                'avatar'			=> '',
                                                                'video_focus'		=> '',
                                                                'active'			=> '',
                                                                'category_id'		=> '',
                                                                'video_posted_date' => time(),
                                                                'description'		=> '',
                                                                'source'			=> '',
                                                                'user_id'			=> $userId));

                        array_push( $justUploadVideo, $id);
                    }elseif(preg_match('#\.(jpg|jpeg|png|gif|bmp)$#is', $item['src'])){
                        $model 		= Context::getInstance()->getFront()->getModel('ImageModel');
                        $id			= $model->InsertImage(array('user_id' 			=> $userId,
                                                                'folder_id'			=> $folderId,
                                                                'image_title' 		=> $title,
                                                                'image_src' 		=> Helper::getInstance()->getConfig('image_url') . $item['src'],
                                                                'image_size' 		=> $item['size'],
                                                                'image_status' 		=> 1,
                                                                'image_posted_date' => time()));
                        array_push( $justUploadImage, $id);
                    }
                }
            }

            $_SESSION['justUploadVideo'] = $justUploadVideo;
            $_SESSION['justUploadImage'] = $justUploadImage;
        }

        die;
    }

    public function indexAction()
    {
        $request 	= Context::getInstance()->getFront()->getRequest();
        if ($action	= $request->getPost('action')){
	        $actionName = $action. 'Action';
	        if ($action != 'index' && method_exists($this, $actionName)){
	        	$this->{$actionName}();
	        }
        }
    }

}
?>
