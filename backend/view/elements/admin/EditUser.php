<?php
class EditUser extends Element{
	
	public function setup()
	{
		$this->setTemplatePath('view/templates/admin/')
			 ->setTemplateFile('EditUser');

		$front = Context::getInstance()->getFront();
		$request = $front->getRequest();
		$identity	= HttpAuth::getInstance()->getIdentity();

		if (HttpAuth::getInstance()->isAuthenticated()){
			if (null != $request->getPost()){
				//bắt post action thêm sửa xóa tin
				$action	= $request->getPost('action');
				if ($action == 'add' || $action == 'update'){
					$this->updateUser($identity);
				}
			}
		} else {
			$front->getResponse()->setRedirect('/dang-nhap');
			$front->getResponse()->sendHeaders();
		}

		$model 		= $front->getModel('UserModel');
		if (($id	= Context::getInstance()->getRoute()->getParam('id')) > 0){
			$user = $model->getUser(array('user_id' => $id));
			if (empty($user)){
				$front->getResponse()->setRedirect('/admin/edit-user');
			}
			else {
				$this->assign('user', $user);
			}
		}
		
		$roleModel 		= $front->getModel('RoleModel');
		$this->assign('roles', $roleModel->getRoles(1));
	}

	public function updateUser($identity)
	{
		$done		    = false;
		$request 	    = Context::getInstance()->getFront()->getRequest();
		$name 		    = $request->getPost('user_name');
		$pass		    = $request->getPost('user_password');
		$fullname	    = $request->getPost('user_fullname');
		$status		    = $request->getPost('user_status');
		$roleId		    = $request->getPost('role_id');
		$id			    = Context::getInstance()->getRoute()->getParam('id');
		$model 		    = Context::getInstance()->getFront()->getModel('UserModel');
		$historyModel   = Context::getInstance()->getFront()->getModel('HistoryModel');

		$err = '';
		if ($err == null || strlen($err) == 0){
			if (!empty($id)){
				//Update vào bảng user
				$model->updateUser(array( 	'user_password'	        => $pass,
											'user_fullname'	        => $fullname,
										  	'role_id'		        => $roleId,
										  	'user_status'	        => $status,
										  	'user_id'		        => $id,
                                            'user_modified_date'	=> strtotime(date('Y-m-d H:i:s'))
                ));
				//Thêm sự kiện vào bảng history
				$historyModel->InsertHistory(array( 'history_name' 	=> 'UpdateUser',
													'history_time' 	=> strtotime(date('Y-m-d H:i:s')),
													'route_id' 		=> 'admin/edit-user',
													'object_id' 	=> $id,
													'object_name' 	=> 'User',
													'user_id' 		=> $identity['user_id']));
				$done = true;
			} else {
                //Thêm vào bảng user
                $id = $model->insertUser(array( 'user_name'		        => $name,
                                                'user_password'	        => $pass,
                                                'user_fullname'	        => $fullname,
                                                'role_id'		        => $roleId,
                                                'user_status'	        => $status,
                                                'user_created_date'	    => strtotime(date('Y-m-d H:i:s')),
                                                'user_modified_date'	=> strtotime(date('Y-m-d H:i:s'))
                ));
                //Thêm sự kiện vào bảng history
                $historyModel->InsertHistory(array( 'history_name' 	=> 'InsertUser',
                                                    'history_time' 	=> strtotime(date('Y-m-d H:i:s')),
                                                    'route_id' 		=> 'admin/edit-user',
                                                    'object_id' 	=> $id,
                                                    'object_name' 	=> 'User',
                                                    'user_id' 		=> $identity['user_id']));
                $done = true;
			}
		}
		$this->assign('error', $done ? 'Thành công!' : (isset($err) ? $err : 'Có lỗi xảy ra, bạn vui lòng kiểm tra lại'));
        if ($done){
            Context::getInstance()->getFront()->getResponse()->setRedirect('/admin/edit-user/' . $id);
        }
	}
}
?>
