<?php
class UploadFile extends Element{
	public function setup()
	{
		if (HttpAuth::getInstance()->isAuthenticated()){
			$this->HandleAction();
		}
		die();
	}
	
	public function HandleAction()
	{
		$request 	= Context::getInstance()->getFront()->getRequest();
		$identity	= HttpAuth::getInstance()->getIdentity();
		$imageModel = Context::getInstance()->getFront()->getModel('ImageModel');
		
		if ($request->getParam('action') == 'search'){
			$pageIndex 	= $request->getParam('page');
			$pageIndex  = $pageIndex > 1 ? $pageIndex  : 1;
			$fileName 	= $request->getParam('keyword');
			Context::getInstance()->getFront()->getResponse()->setHeader('Content-Type', 'application/json');
			echo json_encode($imageModel->GetListImages(array('title' => $fileName), $pageIndex, 30));
			die;
		}
		
		$action	= $request->getPost('action');

		if ($action == 'upload'){
			$fileget 	= $request->getPost('fileurl');
			$fileget 	= $fileget ? $fileget :'uploadfile';
            $fileupload = stripos($fileget, 'ngoisao.vn') !== false ? preg_replace('#/resize_[\d]+x[\d]+|.stamp[\d]+#is', '', $fileget) : $fileget;
			$uploadPath	= Helper::getInstance()->getConfig('ftp_upload_path');
			$image 		= Transfer::getInstance()->curlFtpUpload($fileupload, $uploadPath);

			$title 		= $request->getPost('image_title');
			if (isset($image['src']) && !empty($image['src'])){
				$imageModel->InsertImage(array(	'user_id' 			=> $identity['user_id'],
												'image_title' 		=> !empty($title) ? $title : preg_replace('#^.*/|(\.\w+)+$#is', '', $image['src']),
												'image_src' 		=> Helper::getInstance()->getConfig('image_url') . $image['src'],
												'image_size' 		=> $image['size'],
												'image_status' 		=> 1,
												'image_posted_date' => strtotime(date('Y-m-d H:i:s'))));
				
				if ($fileget != 'uploadfile'){ 
					// upload from URL
					echo json_encode(array('o' => $fileget, 'n' => Helper::getInstance()->getConfig('image_url') . $image['src']));
					die;
				}
			}elseif (isset($image[0]['src'])){
				foreach ($image as $item){
					$imageModel->InsertImage(array(	'user_id' 			=> $identity['user_id'],
													'image_title' 		=> !empty($title) ? $title : preg_replace('#^.*/|(\.\w+)+$#is', '', $item['src']),
													'image_src' 		=> Helper::getInstance()->getConfig('image_url') . $item['src'],
													'image_size' 		=> $item['size'],
													'image_status' 		=> 1,
													'image_posted_date' => strtotime(date('Y-m-d H:i:s'))));
				
				}
			}
		} else if ($action == 'upload-avatar'){
            $uploadPath	= Helper::getInstance()->getConfig('ftp_upload_path');
            $image      = Transfer::getInstance()->curlFtpUpload('data', $uploadPath);
            echo json_encode(array('image' => isset($image['src']) && !empty($image['src']) ? Helper::getInstance()->getConfig('image_url') . $image['src'] : ''));
            die;
        }
	}
	
}
?>
