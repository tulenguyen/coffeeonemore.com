<?php

class EditProductType extends Element
{
    public function setup()
    {
        $this->setTemplatePath('view/templates/admin/')
            ->setTemplateFile('EditProductType');

        //Lấy thông tin user từ session
        $identity	= HttpAuth::getInstance()->getIdentity();
        $this->assign('user', $identity);

        $front = Context::getInstance()->getFront();
        $request = $front->getRequest();
        if (HttpAuth::getInstance()->isAuthenticated()){
            if (null != $request->getPost()){
                $action = $request->getPost('action');
                if ($action == 'add' || $action == 'update') {
                    $this->update();
                } else if ($action == 'delete') {
                    $this->delete();
                }
            }
        } else {
            $front->getResponse()->setRedirect('/dang-nhap');
            $front->getResponse()->sendHeaders();
        }
        $model 		= $front->getModel('ProductModel');
        $parentId = 0;
        if (($id	= Context::getInstance()->getRoute()->getParam('id')) > 0){
            $this->assign('productType', $model->getListProductType(['id' => $id]));
            $parentId = $this->productType['parent_id'];

            $title  = (isset($this->productType['name']) ? $this->productType['name'] : '');
        } else {
            $title  = 'Thêm loại sản phẩm mới';
        }

        $this->assign('listProductType', $model->getListProductType(array(), 1, 50));

        //Danh sách productType
        $model->treeView(0, array(), '/admin/edit-product-type', $treeView, '');
        $this->assign('treeView', $treeView);

        //Chọn cat cha
        $model->treeOption(0, array($parentId), $treeOption, '');
        $this->assign('treeOption', $treeOption);
        Context::getInstance()->getFront()->getLayout()->setPageTitle($title);
    }

    private function update()
    {
        $identity	        = HttpAuth::getInstance()->getIdentity();
        $userId             = $identity['user_id'];
        $request 			= Context::getInstance()->getFront()->getRequest();
        $name 			    = $request->getPost('name');
        $parentId           = $request->getPost('parentId');
        $slug 			    = $request->getPost('slug');
        $description 		= $request->getPost('description');
        $extraDescription 	= $request->getPost('extraDescription');
        $productImageUrl    = $request->getPost('productImageUrl');
        if (!empty($productImageUrl) && strpos($productImageUrl, 'http') !== false) {
            $imageSrc       = $productImageUrl;
        } else {
            $imageSrc       = $request->getPost('imgSrc');
        }
        $headerOrder 	    = $request->getPost('headerOrder');
        $homeOrder 	        = $request->getPost('homeOrder');
        $footerOrder 	    = $request->getPost('footerOrder');
        $status		        = $request->getPost('status');
        $isSale		        = $request->getPost('isSale');
        $descPosition       = $request->getPost('descPosition');
        $redirect_link 	    = $request->getPost('redirectLink');

        $id                 = Context::getInstance()->getRoute()->getParam('id');
        $model 				= Context::getInstance()->getFront()->getModel('ProductModel');
        $historyModel 		= Context::getInstance()->getFront()->getModel('HistoryModel');

        if (!empty($name)){
            if ($id > 0){
                $model->updateProductType([
                    'modified_user'         => $userId,
                    'name'			        => $name,
                    'parent_id'             => $parentId,
                    'slug'			        => !empty($slug) ? $slug : Util::toUnsign($name),
                    'description'	        => $description,
                    'extra_description'     => $extraDescription,
                    'image'	                => $imageSrc,
                    'header_order'	        => $headerOrder,
                    'home_order'	        => $homeOrder,
                    'footer_order'	        => $footerOrder,
                    'status'		        => $status,
                    'is_sale'               => $isSale,
                    'redirect_link'	        => $redirect_link,
                    'updated_at'	        => strtotime(date('Y-m-d H:i:s')),
                    'id'			        => $id
                ]);

                $historyModel->InsertHistory([
                    'history_name' 	=> 'UpdateProductType',
                    'history_time' 	=> strtotime(date('Y-m-d H:i:s')),
                    'route_id' 		=> 'admin/edit-product-type',
                    'object_id' 	=> $id,
                    'object_name' 	=> 'ProductType',
                    'user_id' 		=> $userId
                ]);
            } else {
                $id = $model->insertProductType([
                    'user_id' 		        => $userId,
                    'modified_user'         => $userId,
                    'name'			        => $name,
                    'parent_id'             => $parentId,
                    'slug'			        => !empty($slug) ? $slug : Util::toUnsign($name),
                    'description'	        => $description,
                    'extra_description'     => $extraDescription,
                    'image'	                => $imageSrc,
                    'header_order'	        => $headerOrder,
                    'home_order'	        => $homeOrder,
                    'footer_order'	        => $footerOrder,
                    'status'		        => $status,
                    'is_sale'               => $isSale,
                    'redirect_link'	        => $redirect_link,
                    'created_at'	        => strtotime(date('Y-m-d H:i:s')),
                    'updated_at'	        => strtotime(date('Y-m-d H:i:s'))
                ]);

                $historyModel->InsertHistory([
                    'history_name' 	=> 'InsertProductType',
                    'history_time' 	=> strtotime(date('Y-m-d H:i:s')),
                    'route_id' 		=> 'admin/edit-product-type',
                    'object_id' 	=> $id,
                    'object_name' 	=> 'ProductType',
                    'user_id' 		=> $userId
                ]);
            }

            $this->assign('error', 'Cập nhật thành công!');

        } else {
            $this->assign('error', 'Bạn chưa điền tên!');
        }

    }

    private function delete() {
        $id                 = Context::getInstance()->getRoute()->getParam('id');
        $model 				= Context::getInstance()->getFront()->getModel('ProductModel');
        $historyModel 		= Context::getInstance()->getFront()->getModel('HistoryModel');

        $identity	        = HttpAuth::getInstance()->getIdentity();
        $userId             = $identity['user_id'];

        if (!empty($id)) {
            $model->deleteProductType($id);

            $historyModel->InsertHistory([
                'history_name' 	=> 'DeleteProductType',
                'history_time' 	=> strtotime(date('Y-m-d H:i:s')),
                'route_id' 		=> 'admin/edit-product-type',
                'object_id' 	=> $id,
                'object_name' 	=> 'ProductType',
                'user_id' 		=> $userId
            ]);

            Context::getInstance()->getFront()->getResponse()->setRedirect('/admin/edit-product-type/');
        }
    }
}
?>
