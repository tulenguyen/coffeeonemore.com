<?php

class EditProduct extends Element {

    public function setup() {
        $this->setTemplatePath('view/templates/admin/')
                ->setTemplateFile('EditProduct');

        //Lấy thông tin user từ session
        $identity = HttpAuth::getInstance()->getIdentity();
        $this->assign('user', $identity);

        $front = Context::getInstance()->getFront();
        $request = $front->getRequest();
        if (HttpAuth::getInstance()->isAuthenticated()) {
            if (null != $request->getPost()) {
                $action = $request->getPost('action');
                if ($action == 'add' || $action == 'update') {
                    $this->update();
                }
            }
        } else {
            $front->getResponse()->setRedirect('/dang-nhap');
            $front->getResponse()->sendHeaders();
        }
        $model = $front->getModel('ProductModel');
        $newsModel = $front->getModel('NewsModel');
        if (($id = Context::getInstance()->getRoute()->getParam('id')) > 0) {
            $product = $model->getListProduct(array('id' => $id, 'join_user' => 1));
            if (empty($product) || !isset($product['id'])) {
                $front->getResponse()->setRedirect('/admin/edit-product');
            }
            $this->assign('product', $product);

            $listNewsRelation = isset($product['news_relation']) && !empty($product['news_relation']) ? $newsModel->getNews(['field' => 'n.news_id, n.news_title', 'news_ids' => $product['news_relation']]) : null;
            $this->assign('newsRelation', !empty($listNewsRelation) ? json_encode($listNewsRelation, true) : '');

            $listProductRelation = isset($product['product_relation']) && !empty($product['product_relation']) ? $model->getListProduct(['field' => 'id, name', 'ids' => $product['product_relation']]) : null;
            $this->assign('productRelation', !empty($listProductRelation) ? json_encode($listProductRelation, true) : '');

            $title = (isset($product['name']) ? $product['name'] : '');
        } else {
            $title = 'Thêm sản phẩm mới';
        }

        $model->treeOption(0, [isset($this->product['type_id']) ? $this->product['type_id'] : 0], $treeOption);
        $this->assign('treeOption', $treeOption);
        Context::getInstance()->getFront()->getLayout()->setPageTitle($title);
    }

    public function update() {
        $identity = HttpAuth::getInstance()->getIdentity();
        $userId = $identity['user_id'];
        $request = Context::getInstance()->getFront()->getRequest();
        $name = $request->getPost('name');
        $slug = $request->getPost('slug');
        $shortDescription = $request->getPost('shortDescription');
        $code = $request->getPost('code');

        $description = Util::removeTag($request->getPost('description'), 'html|title|head|body');
        $description = html_entity_decode($description, ENT_QUOTES, "UTF-8");
        $description = preg_replace('#<div[^>]*>\s*</div>#is', '', $description);

        $productImageUrl = $request->getPost('productImageUrl');
        if (!empty($productImageUrl) && strpos($productImageUrl, 'http') !== false) {
            $imageSrc = $productImageUrl;
        } else {
            $imageSrc = $request->getPost('imgSrc');
        }

        $price = $request->getPost('price');
        $old_price = $request->getPost('old_price');
        $material = $request->getPost('material');
        $size = $request->getPost('size');
        $source = $request->getPost('source');
        $support = $request->getPost('support');
        $typeId = $request->getPost('typeId');
        $newsRelation = $request->getPost('newsRelationId');
        $productRelation = $request->getPost('productRelationId');
        $status = $request->getPost('status');
        $createdAt = $request->getPost('createdAt');

        $id = Context::getInstance()->getRoute()->getParam('id');
        $model = Context::getInstance()->getFront()->getModel('ProductModel');
        $historyModel = Context::getInstance()->getFront()->getModel('HistoryModel');

        if (!empty($name)) {
            if ($id > 0) {
                $model->updateProduct([
                    'modified_user' => $userId,
                    'name' => $name,
                    'slug' => !empty($slug) ? $slug : Util::toUnsign($name),
                    'code' => $code,
                    'short_description' => $shortDescription,
                    'description' => $description,
                    'image' => $imageSrc,
                    'old_price' => $old_price,
                    'price' => $price,
                    'material' => $material,
                    'size' => $size,
                    'source' => $source,
                    'support' => $support,
                    'type_id' => $typeId,
                    'status' => $status,
                    'news_relation' => $newsRelation,
                    'product_relation' => $productRelation,
                    'created_at' => strtotime($createdAt),
                    'updated_at' => strtotime(date('Y-m-d H:i:s')),
                    'id' => $id
                ]);

                $url = Helper::getInstance()->getConfig('domain') . 'managecache?action=request' . '&href=/admin/edit-product/' . $id . '&typeId=' . $typeId;
                Transfer::getInstance()->getContent($url, Helper::getInstance()->getConfig('domain'), true, ['_cookie_refer' => 'cúc cu'], null, 1);

                $historyModel->InsertHistory([
                    'history_name' => 'UpdateProduct',
                    'history_time' => strtotime(date('Y-m-d H:i:s')),
                    'route_id' => 'admin/edit-product',
                    'object_id' => $id,
                    'object_name' => 'Product',
                    'user_id' => $userId
                ]);
            } else {
                $id = $model->insertProduct([
                    'user_id' => $userId,
                    'modified_user' => $userId,
                    'name' => $name,
                    'slug' => !empty($slug) ? $slug : Util::toUnsign($name),
                    'code' => $code,
                    'short_description' => $shortDescription,
                    'description' => $description,
                    'image' => $imageSrc,
                    'old_price' => $old_price,
                    'price' => $price,
                    'material' => $material,
                    'size' => $size,
                    'source' => $source,
                    'support' => $support,
                    'type_id' => $typeId,
                    'status' => $status,
                    'news_relation' => $newsRelation,
                    'product_relation' => $productRelation,
                    'created_at' => strtotime($createdAt),
                    'updated_at' => strtotime(date('Y-m-d H:i:s'))
                ]);

                $historyModel->InsertHistory([
                    'history_name' => 'InsertProduct',
                    'history_time' => strtotime(date('Y-m-d H:i:s')),
                    'route_id' => 'admin/edit-product',
                    'object_id' => $id,
                    'object_name' => 'Product',
                    'user_id' => $userId
                ]);
            }

            $this->assign('error', 'Cập nhật thành công!');
        } else {
            $this->assign('error', 'Bạn chưa điền tên!');
        }

        switch ($status) {
            case 0:
                $url = '/admin/list-product-pending';
                break;
            case 1:
                $url = '/admin/list-product-publish';
                break;
            default:
                $url = '/admin/list-product-pending';
                break;
        }
        Context::getInstance()->getFront()->getResponse()->setRedirect($url);
    }

}

?>
