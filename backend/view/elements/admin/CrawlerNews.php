<?php

class CrawlerNews extends Element {

    public function setup() {
        $this->_templateFile = 'CrawlerNews';
        $this->_templatePath = 'view/templates/admin/';
        $front = Context::getInstance()->getFront();
        $request = $front->getRequest();

        if (HttpAuth::getInstance()->isAuthenticated()) {
            if (null != $request->getPost()) {
                //bắt post action thêm sửa xóa tin
                $action = $request->getPost('action');
                if ($action == 'add') {
                    $this->updateNews();
                }
            }
        } else {
            $front->getResponse()->setRedirect('/dang-nhap');
            $front->getResponse()->sendHeaders();
        }

        Context::getInstance()->getFront()->getLayout()->setPageTitle('Crawler tin')->setPageDescription('Crawler tin');
    }

    public function updateNews() {
        $front = Context::getInstance()->getFront();
        $request = $front->getRequest();
        $identity = HttpAuth::getInstance()->getIdentity();
        $newsTitle = $request->getPost('newsTitle');
        $newsSapo = $request->getPost('newsSapo');

        $content = Util::removeTag($request->getPost('newsContent'), 'html|title|head|body');
        $content = html_entity_decode($content, ENT_QUOTES, "UTF-8");
        $content = preg_replace('#<div[^>]*>\s*</div>#is', '', $content);
        $content = preg_replace('#<div[^>]*>(\s*videoOptions[^<]+)</div>#is', '<script type="text/javascript">\\1</script>', $content);
        $newsTag = $request->getPost('newsTag');
        $newsPublishDate = $request->getPost('newsPublishDate');
        $sourceName = $request->getPost('sourceName');
        $domain = $request->getPost('domain');
        $newsSeoTitle = $request->getPost('newsSeoTitle');
        $newsSeoDescription = $request->getPost('newsSeoDescription');
        $newsSeoKeyword = $request->getPost('newsSeoKeyword');

        $uploadPath = Helper::getInstance()->getConfig('ftp_upload_path');
        $fileget = $request->getPost('image');

        $fileget = $fileget ? $fileget : 'uploadfile';
        $fileupload = stripos($fileget, 'ngoisao.vn') !== false || stripos($fileget, 'xahoi.com.vn') !== false ? preg_replace('#/resize_[\d]+x[\d]+|.stamp[\d]+#is', '', $fileget) : $fileget;

        $image = Transfer::getInstance()->curlFtpUpload($fileupload, $uploadPath);

        $newsModel = $front->getModel('NewsModel');
        $historyModel = $front->getModel('HistoryModel');

        /* $content                = preg_replace_callback('#<img[^>]*src="([^"]+)"[^>]*>#', function($match) use ($uploadPath, $domain) {
          $originUrl          = $match[1];
          if (stripos($originUrl, 'http') === false){
          $originUrl      = 'http://' . trim($domain, '/') . ltrim($originUrl, '/');
          }
          $originUrl          = stripos($originUrl, 'ngoisao.vn') !== false || stripos($originUrl, 'xahoi.com.vn') !== false ? preg_replace('#/resize_[\d]+x[\d]+|.stamp[\d]+#is', '', $originUrl) : $originUrl;
          $imageUrl           = Transfer::getInstance()->curlFtpUpload($originUrl, $uploadPath);
          return isset($imageUrl['src']) && !empty($imageUrl['src']) ? '<img src="' .Helper::getInstance()->getConfig('image_url') . $imageUrl['src'] . '" />' : $match[0];
          }, $content); */

        //Insert tin vào bảng news
        $id = $newsModel->insertNews(array('user_id' => $identity['user_id'],
            'modified_user' => $identity['user_id'],
            'approved_user' => $identity['user_id'],
            'news_title' => str_replace('"', '\'', $newsTitle),
            'news_sapo' => $newsSapo,
            'category_id' => '',
            'news_tag_slug' => Util::toUnsign($newsTag),
            'news_tag' => $newsTag,
            'news_content' => $content,
            'source_name' => $sourceName,
            'news_relation' => '',
            'news_published_date' => strtotime($newsPublishDate),
            'news_modified_date' => strtotime(date('Y-m-d H:i:s')),
            'news_image' => isset($image['src']) ? Helper::getInstance()->getConfig('image_url') . $image['src'] : '',
            'is_video' => 0,
            'is_outside' => 0,
            'news_slug' => '',
            'news_seo_title' => $newsSeoTitle,
            'news_seo_description' => $newsSeoDescription,
            'news_seo_keyword' => $newsSeoKeyword,
            'news_redirect_link' => '',
            'news_status' => 4)
        );
        //Thêm sự kiện vào bảng history
        $historyModel->InsertHistory(array('history_name' => 'InsertNews',
            'history_time' => strtotime(date('Y-m-d H:i:s')),
            'route_id' => 'admin/edit-news-crawler',
            'object_id' => $id,
            'object_name' => 'News',
            'user_id' => $identity['user_id']));
        if ($id) {
            $front->getResponse()->setRedirect('/admin/edit-news/' . $id);
        }
    }

}

?>