<?php
class ListUser extends Element{
	public function setup(){
		$this->_templateFile = 'ListUser';
		$this->_templatePath = 'view/templates/admin/';
		
		$front = Context::getInstance()->getFront();
		$request = $front->getRequest();
		if (HttpAuth::getInstance()->isAuthenticated()){
			if (null != $request->getPost()){
				$action	= $request->getPost('action');
				if ($action == 'filter'){
					$userLike	= $request->getPost('searchUser');
					$front->getResponse()->setRedirect('/admin/list-user/1?userlike=' . $userLike);
				}
			}
		} else {
			$front->getResponse()->setRedirect('/dang-nhap');
			$front->getResponse()->sendHeaders();
		}
		
		//Lấy thông tin phân trang
		$pageIndex  = $request->getParts(2);
		$pageIndex	= $pageIndex < 1 ? 1 : $pageIndex;
		$this->assign('pageIndex', $pageIndex);
		$this->assign('filter', array('user_like' => $request->getParam('userlike')));
		$ext = '';
		if (isset($this->filter['user_like']) && $this->filter['user_like'] != ''){
			$ext = '?userlike=' . $this->filter['user_like'];
		}
		$this->assign('ext', $ext);
		//Lấy danh sách user
		$model 		= Context::getInstance()->getFront()->getModel('UserModel');
		$this->assign('users', $model->getUser(array('user_like' => '%' . $this->filter['user_like'] . '%'), $pageIndex, 10));
		Context::getInstance()->getFront()->getLayout()->setPageTitle('Danh sách thành viên')
														->setPageDescription('Danh sách thành viên')
														->setPageKeywords('');
	}
}
?>