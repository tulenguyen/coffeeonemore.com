<?php
class EditRole extends Element{
	public function setup()
	{
		$this->setTemplatePath('view/templates/admin/')
		->setTemplateFile('EditRole');
	
		$request = Context::getInstance()->getFront()->getRequest();
		if (HttpAuth::getInstance()->isAuthenticated()){
			if (null != $request->getPost()){
				$this->update();
			}
		} else {
			Context::getInstance()->getFront()->getResponse()->setRedirect('/dang-nhap');
			Context::getInstance()->getFront()->getResponse()->sendHeaders();
		}
	
		$roleModel 		= Context::getInstance()->getFront()->getModel('RoleModel');
		if (($id		= Context::getInstance()->getRoute()->getParam('id')) > 0){
			$this->assign('role', $roleModel->getRoleById($id));
			$ruleModel 	= Context::getInstance()->getFront()->getModel('RuleModel');
			$listRoute = $ruleModel->GetRouteByRoleId($id);
			$this->assign('routeList', $listRoute);
		}
	
		$this->assign('roles', $roleModel->getRoles());
		$routeModel 	= Context::getInstance()->getFront()->getModel('RouteModel');
		$this->assign('routes', $routeModel->getRoutes(array('is_backend' => 1), 1, 0));
	}
	
	public function update()
	{
		$done		= false;
		$request 	= Context::getInstance()->getFront()->getRequest();
		$name 		= $request->getPost('role_name');
		$desc 		= $request->getPost('role_description');
		$status		= $request->getPost('role_status');
		$id			= Context::getInstance()->getRoute()->getParam('id');
		$model 		= Context::getInstance()->getFront()->getModel('RoleModel');
		$ruleModel 	= Context::getInstance()->getFront()->getModel('RuleModel');
		$routeList 	= $request->getPost('checkboxes');
		
		if (null != ($name = trim($name))){
			if ($id > 0){
				$model->updateRole(array(	'name'			=> $name,
											'description'	=> $desc,
											'status'		=> $status,
											'id'			=> $id));
				$ruleModel->UpdateRoleRoute($id, $routeList);
				$done = true;
			} else {
				$id = $model->addRole(array('name'			=> $name,
											'description'	=> $desc,
											'status'		=> $status));
				$ruleModel->UpdateRoleRoute($id, $routeList);
				$done = true;
			}
				
			$this->assign('error', $done ? 'Cập nhật thành công!' : 'Có lỗi xảy ra, bạn vui lòng kiểm tra lại');
				
		} else
			$this->assign('error', 'Bạn chưa điền tên phân quyền!');
	}
}
?>