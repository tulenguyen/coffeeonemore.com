<?php
class Header extends Element
{
	public function setup()
	{
		$this->setTemplatePath('view/templates/common/')
			 ->setTemplateFile('Header');
		
		$front = Context::getInstance()->getFront();
		//Lấy thông tin user từ session
		$identity	= HttpAuth::getInstance()->getIdentity();
		$this->assign('user', $identity);
	}
}
?>
