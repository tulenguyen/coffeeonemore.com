<?php
class LeftBar extends Element
{
	public function setup()
	{
		$this->setTemplatePath('view/templates/common/')
			 ->setTemplateFile('LeftBar');
	 	
		$request 	= Context::getInstance()->getFront()->getRequest();
		$parts		= $request->getParts();
		$route		= implode('/', array_slice($parts, 0, 2));
		Context::getInstance()->getFront()->getModel('RouteModel');
		
		//Lấy thông tin user từ session
		$identity	= HttpAuth::getInstance()->getIdentity();
		$this->assign('user', $identity);

		if (!HttpAuth::getInstance()->isAuthenticated()){
			Context::getInstance()->getFront()->getResponse()->setRedirect('/dang-nhap');
			Context::getInstance()->getFront()->getResponse()->sendHeaders();
		}

        if (isset($identity['listRoute']) && !empty($identity['listRoute']) && stripos($identity['listRoute'], $route) !== false){
			$this->assign('admin', 'allow');
			$this->assign('listRoute', $identity['listRoute']);
		}
	}
}
?>
