<?php echo $this->renderElement('Head'); ?>
<!-- <script type="text/javascript" src="ckeditor/ckeditor.js"></script> -->
<?php echo  $this->renderElement('Header'); ?>
<div class="wrapper">
	<?php echo  $this->renderElement('LeftBar'); ?>
	<!--Body content-->
	<div id="content" class="clearfix">
        <div class="contentwrapper"><!--Content wrapper-->
			<?php if (isset($this->elementName)) : ?>
				<?php echo $this->renderElement($this->elementName)?>
			<?php else: ?>
				<div>
					Bạn không có quyền truy cập vào khu vực này!
				</div>
		<?php endif;?>
		</div>
	</div>
</div>

<?php echo  $this->renderElement('Footer'); ?>
<?php echo  $this->renderElement('Bottom'); ?>

