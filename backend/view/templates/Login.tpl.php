<?php echo $this->renderElement('Head'); ?>
<div class="container-fluid">
	<?php echo  $this->renderElement('Header'); ?>
</div>

<div class="container-fluid">
	<div class="loginContainer">
		<div id="response">
			<?php echo isset($this->response) ? $this->response : ''?>
		</div>
		<form method="POST" class="form-horizontal" action="" id="loginForm" />
            <div class="form-row row-fluid">
                <div class="span12">
                    <div class="row-fluid">
                        <label class="form-label span12" for="username">
                            Tên đăng nhập:<span class="icon16 icomoon-icon-user-2 right gray marginR10"></span>
                        </label>
                        <input class="span12" id="username" type="text" name="username" placeholder="User Name" />
                        <label for="username" generated="true" class="error" style="display: none;">Fill me please</label>
                    </div>
                </div>
            </div>

            <div class="form-row row-fluid">
                <div class="span12">
                    <div class="row-fluid">
                        <label class="form-label span12" for="password">
                            Mật khẩu: <span class="icon16 icomoon-icon-locked right gray marginR10"></span>
                        </label>
                        <input class="span12 error" id="password" type="password" name="password" value="" />
                        <label class="form-label span12"><span class="forgot">
                            <a href="<?php echo Context::getInstance()->getRoute()->createUrl('Forgotpass', array())?>">Quên mật khẩu?</a>
                        </span></label>
                    </div>
                </div>
            </div>

            <div class="form-row row-fluid">
                <div class="span12">
                    <div class="row-fluid">
                        <div class="form-actions">
                            <div class="span12 controls">
                                <button type="submit" class="btn btn-info right" id="loginBtn">
                                    <span class="icon16 icomoon-icon-enter white"></span> Đăng nhập
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
		</form>
	</div>
</div>
<!-- End .container-fluid -->
<?php echo  $this->renderElement('Footer'); ?>
	<script type="text/javascript">
        // document ready function
        $(document).ready(function() {
            $("input, textarea, select").not('.nostyle').uniform();
            $("#loginForm").validate({
                rules: {
                    username: {
                        required: true,
                        minlength: 4
                    },
                    password: {
                        required: true,
                        minlength: 6
                    }  
                },
                messages: {
                    username: {
                        required: "Fill me please",
                        minlength: "My name is bigger"
                    },
                    password: {
                        required: "Please provide a password",
                        minlength: "My password is more that 6 chars"
                    }
                }   
            });
        });
    </script>
<?php echo  $this->renderElement('Bottom'); ?>


