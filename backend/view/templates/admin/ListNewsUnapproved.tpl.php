<div class="heading">
	<h3>Kho chờ duyệt</h3>
</div><!-- End .heading-->

<form name="news" action="" method="post" enctype="multipart/form-data">
    <div class="row-fluid">
        <?php echo $this->renderElement('ListNavigation'); ?>
    </div><!-- End .row-fluid -->

    <div class="row-fluid">
        <div class="box gradient">
            <div class="row-fluid filter-date-block">
                <div class="left listing-post-sta span12">
                    <a href="#" class="margin10 left">Tất cả <span class="counting">(<?php echo isset($this->count) ? $this->count : '0'?>)</span></a>
                    <?php if (isset($this->list) && !empty($this->list)): ?>
                        <div class="right">
                            <?php echo Util::paginate(1000, $this->pageIndex, $this->pageUrl, 'paging', '', $this->ext)?>
                        </div>
                    <?php endif;?>
                </div>
                <hr class="line" />
                <div class="clearfix">
                    <div class="left margin10">
                        <div class="left marginR10 span2">
                            <input type="text"  style="width: 446px !important;" name="titlelike" value="<?php echo isset($this->filter['titlelike']) ? $this->filter['titlelike'] : ''?>" placeholder="Tìm kiếm theo title..." />
                        </div>
                    </div>
                    <div class="left margin10">
                        <?php if(isset($this->royalties[0])):?>
                            <select name="royaltyId">
                                <option value="0">_Chọn loại tin_</option>
                                <?php foreach ($this->royalties as $royalty):?>
                                    <option value="<?php echo $royalty['royalty_id']?>" <?php echo isset($this->filter['royaltyId']) && $this->filter['royaltyId'] == $royalty['royalty_id'] ? 'selected="selected"' : ''?>>
                                        <?php echo $royalty['royalty_name']?>
                                    </option>
                                <?php endforeach;?>
                            </select>
                        <?php endif;?>
                    </div>
                    <div class="left margin10">
                        <input type="hidden" name="action" value="" />
                        <button class="btn btn-info" onclick="news.action.value='filter'">Tìm bài viết</button>
                    </div>
                </div><!-- End .clearfix -->
                <div class="clearfix">
                    <div class="left marginL10">
                        <input type="text" style="width: 120px;" class="datepicker" name="startdate" value="<?php echo isset($this->filter['startDate']) && !empty($this->filter['startDate']) ? date('m/d/Y H:i:s', intval($this->filter['startDate'])) : ''?>" placeholder="Từ ngày..." />
                    </div>
                    <div class="left marginL10">
                        <input type="text" style="width: 120px;" class="datepicker" name="enddate" value="<?php echo isset($this->filter['endDate']) && !empty($this->filter['endDate']) ? date('m/d/Y H:i:s', intval($this->filter['endDate'])) : ''?>" placeholder="Đến ngày..." />
                    </div>
                    <div class="left marginL10">
                        <?php if(isset($this->treeOption) && !empty($this->treeOption)):?>
                            <select name="categoryId">
                                <option value="0">_Chọn chuyên mục_</option>
                                <?php echo $this->treeOption?>
                            </select>
                        <?php endif;?>
                    </div>
                    <div class="left marginL10">
                        <?php if(isset($this->users[0])):?>
                            <select name="userId">
                                <option value="0">_Chọn người tạo_</option>
                                <?php foreach ($this->users as $user):?>
                                    <option value="<?php echo $user['user_id']?>" <?php echo isset($this->filter['userId']) && $this->filter['userId'] == $user['user_id'] ? 'selected="selected"' : ''?>>
                                        <?php echo $user['user_name']?>
                                    </option>
                                <?php endforeach;?>
                            </select>
                        <?php endif;?>
                    </div>
                    <div class="left marginL10">
                        <?php if(isset($this->users[0])):?>
                            <select name="approvedUser">
                                <option value="0">_Chọn người duyệt_</option>
                                <?php foreach ($this->users as $user):?>
                                    <option value="<?php echo $user['user_id']?>" <?php echo isset($this->filter['approvedUser']) && $this->filter['approvedUser'] == $user['user_id'] ? 'selected="selected"' : ''?>>
                                        <?php echo $user['user_name']?>
                                    </option>
                                <?php endforeach;?>
                            </select>
                        <?php endif;?>
                    </div>
                </div><!-- End .clearfix -->
            </div>
            <?php if (!isset($this->list) || empty($this->list)):?>
                <div class="alert alert-error">
                    <button type="button" class="close" data-dismiss="alert">&times;</button>
                    <div><strong>Có lỗi!</strong> Không có bài viết nào thỏa mãn</div>
                </div>
            <?php endif;?>
            <div class="content noPad">
                <table class="responsive table table-bordered" id="checkAll">
                    <thead>
                        <tr>
                            <th id="masterCh" class="ch">
                                <input type="checkbox" name="checkboxMaster" value="all" class="styled" />
                                <input type="hidden" name="checkboxes" value=""/>
                            </th>
                            <th colspan="2">Tiêu đề</th>
                            <th>Tác giả - Nguồn - Thời gian xuất bản</th>
                            <th>Người duyệt</th>
                            <th>Điều khiển</th>
                        </tr>
                    </thead>
                    <tbody>
                    <?php if (isset($this->list) && !empty($this->list)): ?>
                        <?php foreach ($this->list as $item):
                            $style = '';
                            if (isset($item['is_approved']) && $item['is_approved'] == 0 && isset($item['royalty_id']) && in_array($item['royalty_id'], array(5, 6, 8))){
                                $is_approved = 0;
                                $style = ' style="background-color:#2f96b4"';
                            }
                            ?>
                            <tr<?php echo $style?>>
                                <td class="chChildren">
                                    <input type="checkbox" name="checkboxChild" value="<?php echo $item['news_id']?>" class="styled" />
                                </td>
                                <td width="100">
                                    <a href="/admin/edit-news/<?php echo $item['news_id']?>" class="list-post-thumb" style="width: 100%">
                                        <img src="<?php echo isset($item['news_image']) ? Util::getThumbSrc($item['news_image'], 100, 80) : '';?>" style="width: 100px; height: 100%;" />
                                    </a>
                                </td>
                                <td width="350" class="alignleft">
                                    <a href="<?php echo isset($item['link_preview']) ? $item['link_preview'] : '#'?>" target="_blank" class="list-post-thumb" style="width: 100%">
                                        <strong id="newsId-<?php echo isset($item['news_id']) ? $item['news_id'] : 0?>">
                                            <?php echo $item['news_title']?>
                                        </strong>
                                    </a>
                                    <div class="listing-lead">
                                        <?php if (isset($item['is_approved']) && $item['is_approved'] == 0 && isset($item['royalty_id']) && in_array($item['royalty_id'], array(5, 6, 8))):?>
                                            <div>
                                                <strong>Tin Công lý chưa duyệt</strong>
                                            </div>
                                        <?php endif;?>
                                        <div>
                                            <?php echo isset($item['news_sapo']) ? $item['news_sapo'] : ''?>
                                        </div>
                                        <?php if (isset($item['visit_count'])):?>
                                            <div class="highlight">
                                                <strong>Lượt view:</strong>
                                                <strong><?php echo $item['visit_count'];?></strong>
                                            </div>
                                        <?php endif;?>
                                    </div>
                                </td>
                                <td width="100">
                                    <div><?php echo (isset($item['source_name']) ? $item['source_name'] : ''); ?></div>
                                    <div><?php echo (isset($item['news_published_date']) ? date('Y-m-d H:i:s', intval($item['news_published_date'])) : '');?></div>
                                </td>
                                <td width="100">
                                    <?php if (isset($item['approved_name']) && !empty($item['approved_name'])):?>
                                        <div>
                                            Duyệt: <strong><?php echo $item['approved_name'] ?></strong>
                                        </div>
                                        <br>
                                    <?php endif;?>
                                    <?php if (isset($item['create_name']) && !empty($item['create_name'])):?>
                                        <div>
                                            Tạo: <strong><?php echo $item['create_name'] ?></strong>
                                        </div>
                                        <br>
                                    <?php endif;?>
                                    <?php if (isset($item['modified_name']) && !empty($item['modified_name'])):?>
                                        <div>
                                            Sửa: <strong><?php echo $item['modified_name'] ?></strong>
                                        </div>
                                    <?php endif;?>
                                </td>
                                <td width="100" class="quk-edit">
                                    <?php if (isset($this->user['role_id']) && $this->user['role_id'] <= 4):?>
                                        <form action="/admin/edit-news" method="post">
                                            <a class="left btnNews" act="2">Lên chờ xuất bản</a>
                                            <div class="clearfix"></div>
                                            <?php if (isset($this->user['role_id']) && $this->user['role_id'] <= 3):?>
                                                <a class="left btnPosition" act="0">Tin thông thường</a>
                                                <div class="clearfix"></div>
                                                <a class="left btnPosition" act="2">Nổi bật mục</a>
                                                <div class="clearfix"></div>
                                                <a class="left btnPosition highlight" act="1">Tin tiêu điểm</a>
                                                <div class="clearfix"></div>
                                            <?php endif;?>

                                            <div class="left">
                                                <a class="btnNews" href="/admin/edit-news/<?php echo $item['news_id']?>">Sửa</a>
                                                |
                                                <a class="btnNews highlight" act="0">Xóa</a>
                                                <input type="hidden" value="<?php echo $item['news_id']?>" class="newsId">
                                            </div>
                                            <div class="clearfix"></div>
                                            <div class="left">
                                                <a class="left btnReturnNews">
                                                    Trả về kho
                                                    <ul class="reason-box hide" data="<?php echo $item['news_id']?>">
                                                        <li class="menu">
                                                            <textarea rows="3"><?php echo isset($item['news_note']) ? $item['news_note'] : ''?></textarea>
                                                            <br/>
                                                            <input type="hidden" value="<?php echo $item['news_id']?>" class="newsId">
                                                            <span class="btn btn-mini right btnNews" act="4" style="color: red">Trả</span>
                                                        </li>
                                                    </ul>
                                                </a>
                                                |
                                                <a class="btnNews" act="2">Trả về chờ xuất bản</a>
                                                <input type="hidden" value="<?php echo $item['news_id']?>" class="newsId">
                                            </div>
                                            <input type="hidden" value="<?php echo $item['news_id']?>" class="newsId">
                                        </form>
                                    <?php else:?>
                                        Bạn không có quyền thực hiện thao tác này
                                    <?php endif;?>
                                </td>
                            </tr>
                        <?php endforeach;?>
                    <?php endif;?>
                    </tbody>
                </table>

                <?php if (isset($this->list) && !empty($this->list)): ?>
                    <div class="margin10 clearfix">
                        <?php echo Util::paginate(1000, $this->pageIndex, $this->pageUrl, 'paging', '', $this->ext)?>
                    </div>
                <?php endif;?>
            </div><!-- End .content -->
        </div><!-- End .box -->
    </div><!-- End .row-fluid -->
</form>
