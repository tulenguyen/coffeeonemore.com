<div class="heading">
	<h3>Lịch sử hoạt động</h3>
</div><!-- End .heading-->

<form name="history" action="" method="post" enctype="multipart/form-data">
	<div class="row-fluid">
		<div class="box gradient">
			<div class="content noPad">
				<table class="responsive table table-bordered" id="checkAll">
					<thead>
						<tr>
							<th>Hoạt động</th>
							<th>Route</th>
							<th>Người hoạt động</th>
							<th>Thời gian</th>
						</tr>
					</thead>
					<tbody>
					<?php if (isset($this->list) && !empty($this->list)): ?>
					<?php foreach ($this->list as $item):?>
						<tr>
							<td width="420">
								<strong><a href="/admin/view-history?filter=<?php echo $item['history_name']?>"><?php echo $item['history_name']?></a></strong>
								<p class="listing-lead">Object: <?php echo $item['object_name']?> / Id: <?php echo $item['object_id']?></p>
								<p class="listing-lead">Chi tiết: <?php echo isset($item['object_detail']) ? $item['object_detail'] : ''?></p>
							</td>
							<td><?php echo (isset($item['route_pattern']) ? $item['route_pattern'] : ''); ?></td>
							<td>
								<div><?php echo (isset($item['user_name']) ? $item['user_name'] : 'Người viết'); ?></div>
								<p class="listing-lead">
									<?php echo (isset($item['role_name']) ? $item['role_name'] : ''); ?>
								</p>
							</td>
							<td><?php echo (isset($item['history_time']) ? date('Y-m-d H:i:s', intval($item['history_time'])) : '');?></td>
						</tr>
					<?php endforeach;?>
					<?php else:?>
						Chưa có lịch sử nào!!!
					<?php endif;?>
					</tbody>
				</table>

				<div class="margin10 clearfix">
					<?php echo Util::paginate(1000, $this->pageIndex, '/admin/view-history/', 'paging', '')?>
				</div>
			</div><!-- End .content -->
			
		</div><!-- End .box -->
	</div><!-- End .row-fluid -->
</form>