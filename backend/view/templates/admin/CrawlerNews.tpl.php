<link type="text/css" rel="stylesheet" href="/public/css/extendCKeditor.css" />

<div class="row-fluid clearfix">
    <form name="news" action="" method="post" enctype="multipart/form-data">
        <div class="span12">
            <div class="box">
                <div class="title">
                    <h4>
                        <span class="icon16 brocco-icon-grid"></span><span>Nội dung bài viết</span>
                    </h4>
                </div>
                <div class="content clearfix">
                    <div class="form-row row-fluid">
                        <div class="span10">
                            <div class="row-fluid">
                                <label class="form-label text_left marginL5 span2">Nhập URL</label>
                                <div class="span9">
                                    <input id="newsURL" placeholder="Nhập url" />
                                </div>
                            </div>
                        </div>

                        <div class="span2">
                            <input type="submit" class="btn btn-info btn-large" id="btnCrawlNews" value="Crawl"/>
                        </div>
                    </div>

                    <div class="alert alert-error hide">
                        <button type="button" class="close" data-dismiss="alert">&times;</button>
                        <div>
                            <span id="edit-news-error"></span>
                        </div>
                    </div>

                    <div class="well">
                        <a href="javascript:void(0)" onclick="open_seo(this)">SEO</a>
                        <div class="seo-wrap">
                            <div class="form-row row-fluid">
                                <div class="span12">
                                    <div class="row-fluid">
                                        <div class="span2">
                                            <label class="form-label text_left marginL5">Tiêu đề SEO</label>
                                        </div>
                                        <div class="span9">
                                            <textarea name="newsSeoTitle"></textarea>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="form-row row-fluid">
                                <div class="span12">
                                    <div class="row-fluid">
                                        <div class="span2">
                                            <label class="form-label text_left marginL5">Mô tả SEO</label>
                                        </div>
                                        <div class="span9">
                                            <textarea name="newsSeoDescription"></textarea>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="form-row row-fluid">
                                <div class="span12">
                                    <div class="row-fluid">
                                        <div class="span2">
                                            <label class="form-label text_left marginL5">Từ khóa SEO</label>
                                        </div>
                                        <div class="span9">
                                            <textarea name="newsSeoKeyword"></textarea>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div><!--SEO wrap-->
                    </div><!--END SEO-->

                    <div class="form-row row-fluid">
                        <div class="span12">
                            <div class="row-fluid">
                                <label class="form-label text_left marginL5 span2">Tên bài viết</label>
                                <div class="span9">
                                    <input name="newsTitle" />
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="form-row row-fluid">
                        <div class="span12">
                            <div class="row-fluid">
                                <div class="span2">
                                    <div><label class="form-label text_left marginL5">Trích dẫn</label></div>
                                </div>
                                <div class="span9">
                                    <textarea name="newsSapo"></textarea>
                                </div>
                            </div>
                        </div>
                    </div>
                    
                            <!-- Tags -->
                <div class="form-row row-fluid">
                    <div class="span12">
                        <div class="row-fluid">
                            <label class="form-label text_left marginL5 span2">Tag bài viết</label>
                            <div class="span9">
                                <div class="controls">
                                    <textarea name="newsTag" rows="1" id="newsTag"><?php echo isset($this->news['news_tag']) ? $this->news['news_tag'] : ''?></textarea>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                    <div class="form-row row-fluid">
                        <div class="span12">
                            <div class="row-fluid span6">
                                <label class="form-label text_left marginL5 span4">
                                    Ảnh đại diện
                                    <span class="help-block">(Ảnh hình chữ nhật ngang 420x252px)</span>
                                </label>
                                <div class="span7">
                                    <span>
                                        <img id="news_image" style="width: 300px; height: 100%;">
                                        <input type="hidden" name="image" value=""/>
                                    </span>
                                </div>
                            </div>
                        </div>
                    </div>

                    <!-- Main content -->
                    <div class="form-row row-fluid">
                        <div class="span12">
                            <div class="row-fluid">
                                <label class="form-label text_left marginL5 span2">Nội dung</label>
                                <div class="span9">
                                    <script type="text/javascript" src="/ckeditor/ckeditor.js"></script>
                                    <textarea id="news_content" name="newsContent" row="30"></textarea>
                                    <style type="text/css">
                                        .tooltips{position:absolute; top:5px; right:5px; background:#000; padding:5px; color:#fff}
                                    </style>
                                    <script type="text/javascript">
                                        var doPaste = function(evt){
                                            // Update the text
                                            var oldData 	= evt.editor.getData();
                                            var clipboard 	= evt.data.dataValue;

                                            clipboard 		= clipboard.replace(/&quot;/g, '"')
                                                .replace(/&#39;/g, "'")
                                                .replace(/&lt;/g, '<')
                                                .replace(/&gt;/g, '>')
                                                .replace(/&amp;/g, '&')
                                                .replace(/&nbsp;/g, ' ');

                                            var imgReg 		= /<img[^>]+src="(.*?)"[^>]*>/gi;
                                            var divs		= [], imgs = {}, i = 0;
                                            var height		= $(document).scrollTop() + $(window).height();
                                            while (imgSrc 	= imgReg.exec(clipboard)) {
                                                imgs[imgSrc[1]] = 1;
                                            }

                                            for (img in imgs){
                                                if (img.indexOf('media.coffeeonemore.com') < 0){
                                                    $.ajax({ type: "POST",
                                                        url: "/admin/upload-file",
                                                        data: { fileurl : img, action: 'upload', image_title: 'No title'},
                                                        dataType: 'json',
                                                        beforeSend: function(){
                                                            divs[i] = $('<div class="tooltips">Uploading '+img+'</div>').css({top:(height - ((i+1)*35))+'px'}).appendTo('body');
                                                        },
                                                        success: function (data) {
                                                            if (data && data.n){
                                                                clipboard = clipboard.replace(new RegExp(data.o, 'gi'), data.n);
                                                                evt.editor.setData(oldData + clipboard);
                                                            }
                                                            $('.tooltips').fadeOut();
                                                        }});
                                                    i++;
                                                }
                                            }
                                        };
                                        $(document).ready(function () {
                                            var editorName = 'news_content';
                                            CKEDITOR.config.height = 500;
                                            CKEDITOR.config.contentsCss = '/public/css/extendCKeditor.css';
                                            CKEDITOR.replace(editorName);
                                            var editor = CKEDITOR.instances.news_content;
                                            editor.on('paste', function (evt) {
                                                doPaste(evt);
                                            }, editor.element.$);
                                        });
                                    </script>
                                </div>
                            </div>
                        </div>
                    </div>

                    <!-- Tác giả - nguồn -->
                    <div class="form-row row-fluid">
                        <div class="span12">
                            <div class="row-fluid span6">
                                <label class="form-label text_left marginL5 span4">Tác giả - Nguồn:</label>
                                <div class="noMar span8">
                                    <input name="sourceName" />
                                    <input type="hidden" name="domain" />
                                </div>
                            </div>

                            <div class="row-fluid span6">
                                <label class="form-label text_left marginL5 span4">Lịch xuất bản</label>
                                <div class="noMar span8">
                                    <link rel="stylesheet" href="public/css/bootstrap/bootstrap-datetimepicker.min.css" type="text/css" media="screen">
                                    <div id="datetimepicker" class="input-append date">
                                        <input class="left" style="width: 120px;" name="newsPublishDate" />
                                        <span class="add-on left" style="margin-left: 5px;"><i data-time-icon="icon-time" data-date-icon="icon-calendar"></i></span>
                                    </div>
                                    <script type="text/javascript" src="public/js/bootstrap/bootstrap.min.js"></script>
                                    <script type="text/javascript" src="public/js/bootstrap/bootstrap-datetimepicker.min.js"></script>
                                    <script type="text/javascript">
                                        $('#datetimepicker').datetimepicker({
                                            format: 'yyyy-MM-dd hh:mm:ss'
                                        });
                                    </script>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="form-row row-fluid">
                        <div class="span12">
                            <div class="row-fluid">
                                <input type="hidden" name="action" value="" />
                                <div class="btn btn-info">
                                    <input type="submit" class="btn btn-info btn-large" value="LẤY BÀI" onclick="news.action.value='add';"/>
                                </div>
                            </div>
                        </div>
                    </div>
                </div><!--content-->
            </div><!--box-->
        </div><!--cot trai-->
    </form>
</div>