<div class="heading">
	<h3>Quản lý thành viên</h3>

	<div class="resBtnSearch">
		<a href="#">
			<span class="icon16 brocco-icon-search"></span>
		</a>
	</div>

	<div class="search">
		<form id="searchform" action="#" />
		<input type="text" class="top-search" placeholder="Tìm kiếm ..." />
		<input type="submit" class="search-btn" value="" />
		</form>
	</div><!-- End search -->
</div>

<div class="row-fluid clearfix">
	<form action="" method="post" enctype="multipart/form-data" name="user">
		<div class="span8" style="margin: 0;">
			<div class="box">
				<div class="title">
					<h4>
						<span class="icon16 brocco-icon-grid"></span>
						<span>
							<?php if (isset($this->user)):?>Sửa thông tin thành viên
							<?php else:?>Tạo thành viên
							<?php endif;?>
						</span>
					</h4>
				</div>
				<div class="content clearfix">
					<?php if (isset($this->error)):?>
					<div class="alert alert-error">
	                    <button type="button" class="close" data-dismiss="alert">&times;</button>
	                    <div><?php echo $this->error;?></div>
                    </div>
                    <?php endif;?>
				
					<div class="form-row row-fluid">
						<div class="span12">
							<div class="row-fluid">
								<label class="form-label span3" for="normal">Tên đăng nhập</label>
								<div class="span8">
									<?php if (isset($this->user['user_id'])): ?>
										<strong><?php echo isset($this->user['user_name']) ? $this->user['user_name'] : ''?></strong>
										<input type="hidden" value="<?php echo isset($this->user['user_name']) ? $this->user['user_name'] : ''?>" name="user_name" id="user_name"/>
									<?php else: ?>
										<input class="span8 error" type="text" name="user_name" id="user_name"/>
									<?php endif;?>
								</div>
							</div>
						</div>
					</div>

					<div class="form-row row-fluid">
						<div class="span12">
							<div class="row-fluid">
								<label class="form-label span3" for="normal">Mật khẩu ban đầu</label>
								<div class="span8">
									<input class="span5" type="password" name="user_password"/>
								</div>
							</div>
						</div>
					</div>
					
					<div class="form-row row-fluid">
						<div class="span12">
							<div class="row-fluid">
								<label class="form-label span3" for="normal">Tên thật</label>
								<div class="span8">
									<input class="span8" type="text" value="<?php echo isset($this->user['user_fullname']) ? $this->user['user_fullname'] : ''?>" name="user_fullname"/>
								</div>
							</div>
						</div>
					</div>

					<div class="form-row row-fluid">
						<div class="span12">
							<div class="row-fluid">
								<label class="form-label span3" for="checkboxes">Chức danh</label>
								<div class="span8">
									<div class="controls">
										<select name="role_id">
										<?php foreach ($this->roles as $role):?>
											<?php if ($role['role_id'] != 1):?>
											<option value="<?php echo $role['role_id']?>" <?php echo isset($this->user['role_id']) && $this->user['role_id'] == $role['role_id'] ? 'selected="selected"' : ''?>><?php echo $role['role_name']?></option>
											<?php endif;?>
										<?php endforeach;?>
										</select>
									</div>
								</div>
							</div>
						</div>
					</div>
					
					<div class="form-row row-fluid">
						<div class="span12">
							<div class="row-fluid">
								<label class="form-label span3" for="checkboxes">Trạng thái</label>
								<div class="span8">
									<div class="controls">
										<select name="user_status">
                                            <option value="1" <?php echo isset($this->user['user_status']) && $this->user['user_status'] == 1 ? 'selected="selected"' : ''?>>Active</option>
											<option value="0" <?php echo isset($this->user['user_status']) && $this->user['user_status'] == 0 ? 'selected="selected"' : ''?>>Deactive</option>
										</select>
									</div>
								</div>
							</div>
						</div>
					</div>
					
					<hr class="line" />

					<div class="form-row row-fluid">
						<div class="span12">
							<div class="row-fluid">
								<input type="hidden" name="action" value=""/>
								<?php if (isset($this->user['user_id'])) :?>
									<button class="btn btn-success btn-large" onclick="checkForm(); GetListCheckboxes(); user.action.value='update';">Lưu thay đổi</button>
								<?php else:?>
									<button class="btn btn-success btn-large" onclick="checkForm(); GetListCheckboxes(); user.action.value='add';">Tạo thành viên</button>
								<?php endif;?>
							</div>
						</div>
					</div>
				</div><!--content-->
			</div><!--box-->
		</div><!--cot trai-->
	</form>
</div><!-- End .row-fluid -->
