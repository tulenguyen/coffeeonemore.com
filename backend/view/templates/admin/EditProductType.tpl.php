<?php
    if (isset($this->productType)) {
        $productType = $this->productType;
    }
?>

<div class="heading">
    <h3>Quản trị loại sản phẩm</h3>
    <div class="resBtnSearch">
        <a href="#">
            <span class="icon16 brocco-icon-search"></span>
        </a>
    </div>

    <div class="search">
        <form id="searchform" action="#" />
        <input class="top-search" placeholder="Tìm kiếm ..." />
        <input type="submit" class="search-btn" value="" />
        </form>
    </div><!-- End search -->
</div><!-- End .heading-->

<div class="row-fluid clearfix">
    <form name="productType" action="" method="post" enctype="multipart/form-data">
        <div class="span8" style="margin: 0;">
            <div class="box">
                <div class="title">
                    <h4>
                        <span class="icon16 brocco-icon-grid"></span><span>Loại sản phẩm</span>
                    </h4>
                </div><!-- title -->

                <div class="content clearfix">
                    <?php if (isset($this->error)):?>
                        <div class="alert alert-error">
                            <button type="button" class="close" data-dismiss="alert">&times;</button>
                            <?php echo $this->error;?>
                        </div>
                    <?php endif;?>

                    <div class="form-row row-fluid">
                        <div class="span12">
                            <div class="row-fluid">
                                <label class="form-label span3">Tên sản phẩm</label>
                                <div class="span8">
                                    <input name="name" value="<?php echo isset($productType['name']) ? $productType['name'] : ''?>" />
                                </div>
                            </div>
                        </div>
                    </div>

                    <hr class="line" />

                    <div class="form-row row-fluid<?php echo (isset($this->user['role_id']) && $this->user['role_id'] <= 2 ? '' : ' hide')?>">
                        <div class="row-fluid span12">
                            <label class="form-label span3">Slug</label>
                            <div class="span8">
                                <input name="slug" value="<?php echo isset($productType['slug']) ? $productType['slug'] : ''?>" />
                            </div>
                        </div>
                    </div>

                    <div class="form-row row-fluid<?php echo (isset($this->user['role_id']) && $this->user['role_id'] <= 2 ? '' : ' hide')?>">
                        <div class="row-fluid span12">
                            <label class="form-label span3">Redirect link</label>
                            <div class="span8">
                                <input name="redirectLink" value="<?php echo isset($productType['redirect_link']) ? $productType['redirect_link'] : ''?>" />
                            </div>
                        </div>
                    </div>

                    <hr class="line" />

                    <div class="form-row row-fluid">
                        <div class="row-fluid span12">
                            <label class="form-label span3">Miêu tả</label>
                            <div class="span8">
                                <script type="text/javascript" src="/ckeditor/ckeditor.js"></script>
                                <textarea id="description" name="description"><?php echo isset($productType['description']) ? stripcslashes($productType['description']) : ''?></textarea>
                                <script type="text/javascript">
                                    $(document).ready(function () {
                                        var editorName = 'description';
                                        CKEDITOR.config.height = 200;
                                        CKEDITOR.config.contentsCss = '/public/css/extendCKeditor.css';
                                        CKEDITOR.replace(editorName);
                                    });
                                </script>
                            </div>
                        </div>
                    </div>

                    <hr class="line" />

                    <div class="form-row row-fluid">
                        <div class="row-fluid span12">
                            <label class="form-label span3">Miêu tả dưới</label>
                            <div class="span8">
                                <textarea name="extraDescription" style="height: 100px;"><?php echo isset($productType['extra_description']) ? $productType['extra_description'] : ''?></textarea>
                            </div>
                        </div>
                    </div>

                    <hr class="line" />

                    <div class="form-row row-fluid">
                        <div class="span12">
                            <div class="row-fluid upload-image">
                                <label class="form-label text_left marginL5 span3">
                                    Ảnh đại diện
                                    <span class="help-block">(Ảnh hình chữ nhật ngang 400x300px)</span>
                                </label>
                                <div class="span4 left">
                                    <input type="file" class="custom-file-input nostyle input-image" name="image"/>
                                </div>
                                <div class="span3 left alert alert-error hide">
                                    <button type="button" class="close" data-dismiss="alert">&times;</button>
                                    <div><span class="upload-error"></span></div>
                                </div>
                                <div class="span8 marginT10">
                                    <span>
                                        <img class="avatar span6" src="<?php echo (isset($productType['image']) ? $productType['image'] : '""');?>" />
                                    </span>
                                    <br>
                                    <input name="productImageUrl" value="" class="marginT10" placeholder="Url ảnh thay thế"/>
                                </div>
                                <input type="hidden" name="imgSrc" class="img-src">
                            </div>
                        </div>
                    </div>

                    <hr class="line" />

                    <div class="form-row row-fluid">
                        <div class="span12">
                            <div class="row-fluid">
                                <label class="form-label span3" for="normal">Chuyên mục cha</label>
                                <div class="span8">
                                    <select name="parentId" class="nostyle">
                                        <option value="0">- Không có cat cha -</option>
                                        <?php echo $this->treeOption;?>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>

                    <hr class="line" />

                    <div class="form-row row-fluid">
                        <div class="span12">
                            <div class="row-fluid">
                                <label class="form-label span3">Thứ tự header</label>
                                <div class="span8">
                                    <input name="headerOrder" value="<?php echo isset($productType['header_order']) ? $productType['header_order'] : ''?>" />
                                </div>
                            </div>
                        </div>
                    </div>

                    <hr class="line" />

                    <div class="form-row row-fluid">
                        <div class="span12">
                            <div class="row-fluid">
                                <label class="form-label span3">Thứ tự home</label>
                                <div class="span8">
                                    <input name="homeOrder" value="<?php echo isset($productType['home_order']) ? $productType['home_order'] : ''?>" />
                                </div>
                            </div>
                        </div>
                    </div>

                    <hr class="line" />

                    <div class="form-row row-fluid">
                        <div class="span12">
                            <div class="row-fluid">
                                <label class="form-label span3">Thứ tự footer</label>
                                <div class="span8">
                                    <input name="footerOrder" value="<?php echo isset($productType['footer_order']) ? $productType['footer_order'] : ''?>" />
                                </div>
                            </div>
                        </div>
                    </div>

                    <hr class="line" />

                    <div class="form-row row-fluid">
                        <div class="span12">
                            <div class="row-fluid">
                                <label class="form-label span3">Trạng thái</label>
                                <div class="span8">
                                    <select name="status">
                                        <option <?php echo isset($productType['status']) && $productType['status'] == 1 ? 'selected="selected"' : '';?> value="1">Hiện</option>
                                        <option <?php echo isset($productType['status']) && $productType['status'] == 0 ? 'selected="selected"' : '';?> value="0">Ẩn</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>

                    <hr class="line" />

                    <div class="form-row row-fluid">
                        <div class="span12">
                            <div class="row-fluid">
                                <label class="form-label span3">Sale cả danh mục?</label>
                                <div class="span8">
                                    <select name="isSale">
                                        <option <?php echo isset($productType['is_sale']) && $productType['is_sale'] == 0 ? 'selected="selected"' : '';?> value="0">Không sale</option>
                                        <option <?php echo isset($productType['is_sale']) && $productType['is_sale'] == 1 ? 'selected="selected"' : '';?> value="1">Sale</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>

                    <hr class="line" />

                    <div class="form-row row-fluid">
                        <div class="span12">
                            <div class="row-fluid">
                                <input type="hidden" name="action" value="" />
                                <?php if (isset($productType['id'])) : ?>
                                    <button class="btn btn-info btn-large" onclick="productType.action.value='update';">Cập nhật</button>
                                    <button class="btn btn-info btn-large" onclick="if(confirm('Bạn chắc chắn muốn xóa?')){productType.action.value='delete';}">Xóa</button>
                                <?php else:?>
                                    <button class="btn btn-info btn-large" onclick="productType.action.value='add';">Thêm</button>
                                <?php endif;?>
                            </div>
                        </div>
                    </div>
                </div><!--content-->
            </div><!--box-->
        </div><!-- cot trai -->

        <div class="span4">
            <div class="box">
                <div class="title">
                    <h4>
                        <span class="icon16 cut-icon-comment"></span>
                        <span>Các loại sản phẩm</span>
                    </h4>
                    <a href="#" class="minimize">Thu nhỏ</a>
                </div>
                <div class="content">
                    <div class="form-row row-fluid" style="height: 460px; overflow:scroll;">
                        <?php echo $this->treeView?>
                    </div>
                </div><!-- content -->
            </div><!-- box -->
        </div><!-- cot phai -->
    </form>
</div>
