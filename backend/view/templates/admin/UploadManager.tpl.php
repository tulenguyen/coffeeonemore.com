<script type="text/javascript" src="public/js/jquery.js"></script>


<style type="text/css">
*{margin:0;padding:0;list-style:none; font:500 12px/16px Arial}
#sidebar{float:left; width:25%; background:#ccc}
#listview{float:left; width:75%}
#treeview{margin:10px}
#treeview li{display:block; padding:6px 10px 6px 25px; background:url(/public/images/folder.gif) no-repeat 0 6px}
#treeview li.expand{background-image:url(/public/images/folder-expand.png)}
#treeview li.current{font-weight:bold}
#treeview li:hover{cursor:pointer}
ul.context-menu{position:absolute; width:250px; top:20px; left:20px; border:1px solid rgba(0,0,0,0.5); box-shadow:0 0 10px rgba(0,0,0,0.5); background:#ccc}
ul.context-menu li{padding:3px 10px; cursor:pointer}
ul.context-menu li:hover,#treeview li ul.context-menu li.active{background:#000; color:#fff}
.frm{position:absolute; z-index:2; padding:10px; box-shadow:0 0 50px rgba(255,255,255, 0.5); background:#fff}
.frm label{float:left; width:100px; padding:5px}
.frm input{border:1px solid #000; padding:5px}
.hide{display:none}
.show{display:block}
#loading{display:none; padding:5px 10px; background:#fff; position:absolute; top:0; left:0}
.on{display:block!important}
.off{display:none}

form{margin:10px}
form input{border:1px solid:#000; height:25px; float:left}
form span{position:relative; height:25px; float:left;  background:#ccc; color:#000; padding:0 5px; font:700 12px/25px Arial; margin-right:10px; overflow:hidden}
form span input{opacity:0; position:absolute; top:0; left:0; padding:5px 0}
form span input.btn{padding:0 5px;background:#000; color:#fff}
form span input.btn:hover{color:#000; background:#fff}

.listview {float:left; width:100%}
.listview li{float:left; width:100px; height:150px; margin:10px 0 0 10px; overflow:hidden; position:relative}
.listview li .icon{position:absolute; top:5px; right:5px; width:20px}
.listview li:hover,.listview li.active{cursor:pointer; box-shadow:0 0 10px rgba(0,0,0,0.5)}
.listview li.select{background:#ccc; border: 1px solid red; margin: 5px 0 0 10px;}
</style>
<div id="loading">Đang tải dữ liệu...</div>
<div id="uploadmanager">
	<div id="sidebar">
		<ul id="treeview">
			<li id="folder0">Thư mục</li>
		</ul>
	</div>
	<div id="listview">
		<div>
			<form id="form_upload" name="upload" action="/upload-manager" method="POST" enctype="multipart/form-data" target="iframeupload">
                <input type="text" name="name" placeholder="Đặt tên bộ ảnh" style="border: 1px solid red"/>
				<span>
                    Chọn file
                	<input type="file" id="uploadfile" name="uploadfile[]" accept="image/*,video/*" multiple="multiple">
                </span>
                <input type="hidden" name="folder_id"/>
                <input type="submit" value="Upload" class="btn" style="margin-right:50px">
                <input type="button" id="ok" value="Đồng ý chọn" style="margin-right:5px" class="hide"/>
                <input type="button" id="cancel" value="Thoát"  class="hide"/>
                <input type="hidden" name="action" value="upload">
            </form>
            <iframe name="iframeupload" id="iframeupload" data-action="" style="display:none"></iframe>
		</div>
	</div>
</div>
<div id="overlay"></div>

<script type="text/javascript">
    var uploadmanager = function(){
        me 		= this;
        current = null;

        me.sendData = function(data, before, after){
            if (typeof data != 'object'){
                return false;
            }
            $.ajax({
                type: 'POST',
                url: '/upload-manager',
                data: data,
                beforeSend: function(){
                    if (typeof before == 'function'){
                        before();
                    }
                    $('#loading').addClass('on');
                },
                success: function(responseData){
                    if (typeof after == 'function'){
                        after(responseData);
                    }
                    $('#loading').removeClass('on');
                    me.treeView(responseData);

                }
            });
        };

        var doClick = function(e, item, obj){
            e.stopPropagation();
            me.current = $(e.target);
            me.listView(0);
            child = $('ul', obj);
            if (child.length > 0){
                if (child.is('.hide')){
                    child.removeClass('hide');
                    $(obj).addClass('expand');
                }else{
                    child.addClass('hide');
                    $(obj).removeClass('expand');
                }
            }else{
                //if (item.count > 0){
                    $(obj).addClass('expand');
                    me.sendData({pid: item.folder_id, action:'getFolder'});
                //}
            }
            $('.current').removeClass('current');
            me.current.addClass('current');

            $('.listview li').each(function(){
                if ($(this).hasClass('select')){
                    $(this).removeClass('select');
                }
            });
        };

        me.treeView = function(data){
            var ul  = $('<ul></ul>');
            if (data){
                if (typeof data == 'string'){
                    eval('data='+ data);
                }

                if (typeof data[0] == 'undefined'){
                    return;
                }

                $(data).each(function(i, item){
                    if ($('#folder'+ item.folder_id).length < 1){
                        $('<li id="folder'+item.folder_id+'" data-count="'+item.count+'">' +
                                item.folder_name + (parseInt(item.count) > 0 ? '<span class="data-count">('+item.count+')</span>' : '') +
                            '</li>'
                        ).click(function(e){
                            doClick(e, item, this);
                        }).appendTo(ul);
                    }
                });

                if (data[0].parent_id){
                   parentItem = $('#folder'+ data[0].parent_id);
                   parentItem.append(ul);
                   if (data[0].parent_id == 0){
                       parentItem.bind('click', function(e){
                            doClick(e, {folder_id:0}, this);
                          });
                   }
                }
            }
        };

        var doSelect = function(obj){
            if ($(obj).is('.select')){
                $(obj).removeClass('select');
            }else{
                $(obj).addClass('select');
            }

            $('#ok, #cancel').removeClass('hide');
        };

        me.listView = function(isUpload){
            if (me.current == null){
                return null;
            }
            fid = me.current.attr('id') ? me.current.attr('id').replace('folder', '') : 0;
            me.sendData({fid: fid, action: 'getFile'}, function(){}, function(data){
                if (typeof data == 'string'){
                    eval('data='+ data);
                }
                $('ul.listview', '#listview').remove();
                var ulFile = $('<ul id="file'+ fid+'" class="listview"></ul>').appendTo('#listview');

                $(data.image).each(function(i, item){
                    if ($('#image'+ item.image_id).length < 1){
                        $('<li id="image'+item.image_id+'" class="image'+(data.imageIds.indexOf(','+item.image_id+',') != -1 && isUpload == 1 ? ' select' : '')+'">'+
                            '<img class="avatar" alt="' + item.image_title + '" title="' + item.image_title + '"  src="'+ item.image_src.replace(/(http\:\/\/[^\/]+\/)(.*)/gi, '$1resize_100x100/$2')+ '" style="width: 100%" />'+
                            '<img class="icon" src="/public/images/photo.png"  /> ' +
                            '<span class="filename">'+	item.image_title+ '</span></li>'
                        ).click(function(){
                            doSelect(this);
                        }).appendTo(ulFile);
                    }
                });

                $(data.video).each(function(i, item){
                    if ($('#image'+ item.image_id).length < 1){
                        $('<li id="video'+item.video_id+'" class="video'+(data.videoIds.indexOf(','+item.video_id+',') != -1? ' select' : '')+'">'+
                                '<img class="avatar" alt="' + item.video_name + '" src="'+item.video_avatar.replace(/[a-z0-9]+.ngoisao.vn/gi, 'media.ngoisao.vn').replace(/(http\:\/\/[^\/]+\/)(.*)/gi, '$1resize_100x100/$2') +'"  style="width: 100%"/>'+
                                '<img class="icon" src="/public/images/video.png" /> ' +
                                '<span class="filename"> ' + item.video_name + '</span> ' +
                                '<span class="filesrc"> ' + item.video_src + '</span></li>'
                        ).click(function(){
                            doSelect(this);
                        }).appendTo(ulFile);
                    }
                });

                fixHeight();

            });
        };

        me.showContextMenu = function(obj){
            $(obj).bind('click', function(e){
                    if (e.which != 2){
                        $('.context-menu').addClass('hide');
                    }
                }).
                bind('contextmenu', function(e){

                e.stopPropagation();
                me.current = $(e.target);
                $('.current').removeClass('current');
                me.current.addClass('current');
                pid  = me.current.attr('id') ? me.current.attr('id').replace('folder', '') : 0;

                var self = this, contextMenu = $('.context-menu', self);

                    if (contextMenu.length > 0){
                        if (contextMenu.is('.hide')){
                            contextMenu.removeClass('hide');
                        }else{
                            contextMenu.addClass('hide');
                        }
                        contextMenu.css({top:e.pageY+10, left:e.pageX - 10});
                    } else{
                        contextMenu 	= $('<ul class="context-menu"></ul>')
                                            .css({top:e.pageY+10, left:e.pageX - 10})
                                            .appendTo(obj);
                        var refreshBtn 	= $('<li class="refresh">Tải lại</li>').appendTo(contextMenu);
                        var newBtn		= $('<li class="new">Tạo thư mục con mới</li>').appendTo(contextMenu);
                        var renameBtn	= $('<li class="rename">Đổi tên thư mục</li>').appendTo(contextMenu);
                        var deleteBtn	= $('<li class="delete">Xóa thư mục</li>').appendTo(contextMenu);

                        newBtn.click(function(){
                            var frm = $('.frm');
                            if (frm.length < 1){
                                frm = $('<div class="frm"><label>Tên thư mục</label><input type="text" /></div>')
                                        .css({top:50,left:50})
                                        .appendTo('#uploadmanager');
                                $('input[type=text]', frm).keypress(function(e){
                                    if (e.which == 13){
                                        folderName = $(this).val().replace(/^\s+|\s+$/g,'');
                                        if (folderName){
                                            me.sendData({pid: pid, name: folderName, action: 'createFolder'},
                                                        function(){},
                                                        function(data){
                                                           me.sendData({pid:pid, action: 'getFolder'});
                                                           frm.addClass('hide');
                                                           me.overlay('hide');
                                                        }
                                            );
                                        }else{
                                            alert('Tên thư mục không hợp lệ!');
                                        }
                                    }
                                });
                            }
                            frm.removeClass('hide');
                            me.overlay();
                        });

                        deleteBtn.click(function(){
                            me.sendData(
                                {pid: pid, action: 'deleteFolder'},
                                function(){},
                                function(data){
                                    $('#folder' + pid).remove();
                                }
                            );
                        });
                    }
                    return false;

            });

            return obj;
        };

        me.overlay = function(action){
            if (action){
                action == 'hide' ? $('#overlay').addClass('hide') : $('#overlay').removeClass('hide');
            }else{
                $('#overlay').click(function(){
                    $(this).addClass('hide');
                    $('.frm').addClass('hide');
                }).css({width:$(document).width(),
                    height:$(document).height(),
                    position:'absolute',
                    background:'rgba(0,0,0,0.8)'
                }).removeClass('hide');
            }
        };

        var fixHeight = function(){
             var dochei = $(document).height();
             $('#sidebar').height(dochei);
             $('#listview').height(dochei);
        };

        me.buildUI = function(){
            fixHeight();

            $('input.btn', '#form_upload').bind('click', function(){
                fid  = me.current.attr('id') ? me.current.attr('id').replace('folder', '') : 0;
                $('input[name=folder_id]').val(fid);
                $('#loading').addClass('on');
                $('#iframeupload').attr('data-action', 'uploading');
            });

            $('#ok', '#form_upload').bind('click', function(){
                var html = '';
                $($('li.select', '#listview').get().reverse()).each(function(){
                    if ($(this).is('.image')){
                        img 	 = $(this).find('.avatar');
                        html 	+= '<p style="text-align:center"><img src="'+img.attr('src').replace(/(resize|crop)_\d+x\d+/gi, 'resize_580x1100')+'" alt="'+img.attr('alt')+'" /></p>';
                    }else if($(this).is('.video')){
                        ext 	 = $(this).find('.filesrc').html().replace(/.*\.([a-z0-9]+)$/, '$1').toLowerCase();
                        html	+= '<div id="videoplayer'+ $(this).attr('id')+'">'+
                                       '<div class="video-option">'+
                                        'videoOptions = $.extend(options, {"file": "rtmp://media.ngoisao.vn:1935/vod/'+ext+':'+$(this).find('.filesrc').html()+'","image": "'+$(this).find('.avatar').attr('src').replace(/(resize|crop)_\d+x\d+/gi, 'resize_580x1100')+'"});'+
                                        'jwplayer("videoplayer'+$(this).attr('id')+'").setup(videoOptions);' +
                                   '</div></div>';
                    }
                 });
                window.opener.CKEDITOR.instances.news_content.insertHtml(html);
                window.close();
            });

            $('#cancel', '#form_upload').bind('click', function(){
                window.close();
            });

            $('#iframeupload').bind('load', function(){
                if ($(this).attr('data-action') == 'uploading'){
                    me.listView(1);
                }
                $('#loading').removeClass('on');
                $('#iframeupload').attr('data-action', '');
                $('#ok, #cancel').removeClass('hide');
            });

        };

        me.init = function(){
            me.buildUI();
            me.showContextMenu('#sidebar');
            me.sendData({pid:0, action:'getFolder'}, function(){}, function(){

            });
        };
    };

    (function(){
        var upl = new uploadmanager;
        upl.init();
    })();
</script>