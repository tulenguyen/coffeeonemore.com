<div class="heading">
    <h3>Sản phẩm chờ duyệt</h3>
</div><!-- End .heading-->

<form name="product" action="" method="post" enctype="multipart/form-data">
    <div class="row-fluid">
        <?php echo $this->renderElement('ListNavigation'); ?>
    </div><!-- End .row-fluid -->

    <div class="row-fluid">
        <div class="box gradient">
            <div class="row-fluid filter-date-block">
                <?php if (isset($this->list) && !empty($this->list)): ?>
                    <div class="marginB10 clearfix">
                        <?php echo Util::paginate(1000, $this->pageIndex, $this->pageUrl, 'paging', '', $this->ext)?>
                    </div>
                <?php endif;?>
                <hr class="line" />
                <div class="clearfix">
                    <div class="left margin10">
                        <div class="left marginR10 span2">
                            <input type="text"  style="width: 446px !important;" name="namelike" value="<?php echo isset($this->filter['namelike']) ? $this->filter['namelike'] : ''?>" placeholder="Tìm kiếm theo tên..." />
                        </div>
                    </div>
                    <div class="left margin10">
                        <input type="hidden" name="action" value="" />
                        <button class="btn btn-info" onclick="product.action.value='filter'">Tìm sản phẩm</button>
                    </div>
                </div><!-- End .clearfix -->
                <div class="clearfix">
                    <div class="left marginL10">
                        <input type="text" style="width: 120px;" class="datepicker" name="startdate" value="<?php echo isset($this->filter['startDate']) && !empty($this->filter['startDate']) ? date('m/d/Y H:i:s', intval($this->filter['startDate'])) : ''?>" placeholder="Từ ngày..." />
                    </div>
                    <div class="left marginL10">
                        <input type="text" style="width: 120px;" class="datepicker" name="enddate" value="<?php echo isset($this->filter['endDate']) && !empty($this->filter['endDate']) ? date('m/d/Y H:i:s', intval($this->filter['endDate'])) : ''?>" placeholder="Đến ngày..." />
                    </div>
                    <div class="left marginL10">
                        <?php if(isset($this->treeOption) && !empty($this->treeOption)):?>
                            <select name="typeId">
                                <option value="0">_Chọn loại sản phẩm_</option>
                                <?php echo $this->treeOption?>
                            </select>
                        <?php endif;?>
                    </div>
                    <div class="left marginL10">
                        <?php if(isset($this->users) && !empty($this->users)):?>
                            <select name="userId">
                                <option value="0">_Chọn người tạo_</option>
                                <?php foreach ($this->users as $item):?>
                                    <option value="<?php echo $item['user_id']?>" <?php echo isset($this->filter['userId']) && $this->filter['userId'] == $item['user_id'] ? 'selected="selected"' : ''?>>
                                        <?php echo $item['user_name']?>
                                    </option>
                                <?php endforeach;?>
                            </select>
                        <?php endif;?>
                    </div>
                </div><!-- End .clearfix -->
            </div>
            <?php if (!isset($this->list) || empty($this->list)):?>
                <div class="alert alert-error">
                    <button type="button" class="close" data-dismiss="alert">&times;</button>
                    <div><strong>Có lỗi!</strong> Không có sản phẩm nào thỏa mãn</div>
                </div>
            <?php endif;?>
            <div class="content noPad">
                <table class="responsive table table-bordered" id="checkAll">
                    <thead>
                    <tr>
                        <th id="masterCh" class="ch">
                            <input type="checkbox" name="checkboxMaster" value="all" class="styled" />
                            <input type="hidden" name="checkboxes" value=""/>
                        </th>
                        <th colspan="2">Tên sản phẩm</th>
                        <th>Thời gian xuất bản</th>
                        <th>Người duyệt</th>
                        <th>Điều khiển</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php if (isset($this->list) && !empty($this->list)): ?>
                        <?php foreach ($this->list as $item):?>
                            <tr>
                                <td class="chChildren">
                                    <input type="checkbox" name="checkboxChild" value="<?php echo $item['id']?>" class="styled" />
                                </td>
                                <td width="100">
                                    <a href="/admin/edit-product/<?php echo $item['id']?>" class="list-post-thumb" style="width: 100%">
                                        <img src="<?php echo isset($item['image']) ? Util::getThumbSrc($item['image'], 100, 80) : '';?>" style="width: 100px; height: 100%;" />
                                    </a>
                                </td>
                                <td width="350" class="alignleft">
                                    <a href="/admin/edit-product/<?php echo $item['id']?>" target="_blank" class="list-post-thumb" style="width: 100%">
                                        <strong id="id-<?php echo isset($item['id']) ? $item['id'] : 0?>">
                                            <?php echo $item['name']?>
                                        </strong>
                                    </a>
                                    <div>
                                        <span>Loại sản phẩm:</span>
                                        <span class="highlight">
                                            <?php echo (isset($item['product_type_name']) ? $item['product_type_name'] : ''); ?>
                                        </span>
                                    </div>
                                    <div class="listing-lead">
                                        <div>
                                            <strong>Mô tả ngắn:</strong>
                                            <?php echo isset($item['short_description']) ? $item['short_description'] : ''?>
                                        </div>
                                    </div>
                                </td>
                                <td width="100">
                                    <div><?php echo (isset($item['created_at']) ? date('Y-m-d H:i:s', intval($item['created_at'])) : '');?></div>
                                </td>
                                <td width="100">
                                    <?php if (isset($item['create_name']) && !empty($item['create_name'])):?>
                                        <div>
                                            Tạo: <strong><?php echo $item['create_name'] ?></strong>
                                        </div>
                                        <br>
                                    <?php endif;?>
                                    <?php if (isset($item['modified_name']) && !empty($item['modified_name'])):?>
                                        <div>
                                            Sửa: <strong><?php echo $item['modified_name'] ?></strong>
                                        </div>
                                    <?php endif;?>
                                </td>
                                <td width="100" class="quk-edit">
                                    <?php if (isset($this->user['role_id']) && $this->user['role_id'] <= 3):?>
                                        <form action="/admin/edit-product" method="post">
                                            <a class="left btnProduct" act="1">Xuất bản</a>
                                            <div class="clearfix"></div>
                                            <a class="left" href="/admin/edit-product/<?php echo $item['id']?>">Sửa</a>
                                            <input type="hidden" value="<?php echo $item['id']?>" class="id">
                                        </form>
                                    <?php else:?>
                                        Bạn không có quyền thực hiện thao tác này
                                    <?php endif;?>
                                </td>
                            </tr>
                        <?php endforeach;?>
                    <?php endif;?>
                    </tbody>
                </table>

                <?php if (isset($this->list) && !empty($this->list)): ?>
                    <div class="marginB10 clearfix">
                        <?php echo Util::paginate(1000, $this->pageIndex, $this->pageUrl, 'paging', '', $this->ext)?>
                    </div>
                <?php endif;?>
            </div><!-- End .content -->
        </div><!-- End .box -->
    </div><!-- End .row-fluid -->
</form>