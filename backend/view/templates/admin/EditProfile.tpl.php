<div class="heading">
	<h3>Quản lý thành viên</h3>

	<div class="resBtnSearch">
		<a href="#">
			<span class="icon16 brocco-icon-search"></span>
		</a>
	</div>

	<div class="search">
		<form id="searchform" action="#" />
		<input type="text" class="top-search" placeholder="Tìm kiếm ..." />
		<input type="submit" class="search-btn" value="" />
		</form>
	</div><!-- End search -->
</div>

<div class="row-fluid clearfix">
	<form action="" method="post" enctype="multipart/form-data" name="user">
		<div class="span8" style="margin: 0;">
			<div class="box">
				<div class="title">
					<h4>
						<span class="icon16 brocco-icon-grid"></span>
						<span>Sửa thông tin thành viên</span>
					</h4>
				</div>
				<div class="content clearfix">
					<?php if (isset($this->error)):?>
					<div class="alert alert-error">
	                    <button type="button" class="close" data-dismiss="alert">&times;</button>
	                    <div><?php echo $this->error;?></div>
                    </div>
                    <?php endif;?>
				
					<div class="form-row row-fluid">
						<div class="span12">
							<div class="row-fluid">
								<label class="form-label span3" for="normal">Tên đăng nhập</label>
								<div class="span8">
									<strong><?php echo isset($this->user['user_name']) ? $this->user['user_name'] : ''?></strong>
									<input type="hidden" value="<?php echo isset($this->user['user_name']) ? $this->user['user_name'] : ''?>" name="user_name" id="user_name"/>
								</div>
							</div>
						</div>
					</div>

					<div class="form-row row-fluid">
						<div class="span12">
							<div class="row-fluid">
								<label class="form-label span3" for="normal">Mật khẩu ban đầu</label>
								<div class="span8">
									<input class="span5" type="password" name="user_password"/>
								</div>
							</div>
						</div>
					</div>

                    <hr class="line" />

                    <div class="form-row row-fluid">
                        <div class="span12">
                            <div class="row-fluid">
                                <label class="form-label span3" for="textarea">
                                    Ảnh đại diện
                                    <span class="help-block">Kích thước tối đa được tải lên 2mb</span>
                                </label>
                                <div class="span8">
                                    <input type="file" name="image" value="" />
                                </div>
                                <div class="span8">
									<span style="width: 110px; height: 90px;">
										<img src="<?php echo (isset($this->user['user_avatar']) ? $this->user['user_avatar'] : '""');?>" style="width: 110px; height: 90px;" />
									</span>
                                </div>
                            </div>
                        </div>
                    </div>

                    <hr class="line" />

					<div class="form-row row-fluid">
						<div class="span12">
							<div class="row-fluid">
								<label class="form-label span3" for="normal">Tên thật</label>
								<div class="span8">
									<input class="span8" type="text" value="<?php echo isset($this->user['user_fullname']) ? $this->user['user_fullname'] : ''?>" name="user_fullname"/>
								</div>
							</div>
						</div>
					</div>

					<div class="form-row row-fluid">
						<div class="span12">
							<div class="row-fluid">
								<label class="form-label span3" for="normal">Email</label>
								<div class="span8">
									<input class="span8" type="text" value="<?php echo isset($this->user['user_email']) ? $this->user['user_email'] : ''?>" name="user_email"/>
								</div>
							</div>
						</div>
					</div>

					<div class="form-row row-fluid">
						<div class="span12">
							<div class="row-fluid">
								<label class="form-label span3" for="checkboxes">Địa chỉ</label>
								<div class="span8">
									<textarea style="height: 50px; width: 70%" name="user_address"><?php echo isset($this->user['user_address']) ? $this->user['user_address'] : ''?></textarea>
								</div>
							</div>
						</div>
					</div>

					<div class="form-row row-fluid">
						<div class="span12">
							<div class="row-fluid">
								<label class="form-label span3" for="checkboxes">Số điện thoại</label>
								<div class="span8">
									<input class="span6" type="text" value="<?php echo isset($this->user['user_phone']) ? $this->user['user_phone'] : ''?>" name="user_phone"/>
								</div>
							</div>
						</div>
					</div>
					
					<hr class="line" />

					<div class="form-row row-fluid">
						<div class="span12">
							<div class="row-fluid">
								<label class="form-label span3" for="checkboxes">Chức danh</label>
								<div class="span8">
									<strong><?php echo isset($this->user['role_name']) ? $this->user['role_name'] : ''?></strong>
								</div>
							</div>
						</div>
					</div>

					<hr class="line" />

					<div class="form-row row-fluid">
						<div class="span12">
							<div class="row-fluid">
								<input type="hidden" name="action" value=""/>
								<button class="btn btn-success btn-large" onclick="if (confirm('Bạn chắc chắn muốn cập nhật thông tin?')) { user.action.value='update'; SendNotification('đã cập nhật profile: ' + $('#user_name').val()); }">Lưu thay đổi</button>
							</div>
						</div>
					</div>
				</div><!--content-->
			</div><!--box-->
		</div><!--cot trai-->
	</form>
</div><!-- End .row-fluid -->
