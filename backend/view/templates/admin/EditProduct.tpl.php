<link type="text/css" rel="stylesheet" href="/public/css/extendCKeditor.css" />
<?php
    if (isset($this->product)) {
        $product = $this->product;
    }
?>

<div class="row-fluid clearfix">
    <form name="product" action="" method="post" enctype="multipart/form-data">
        <div class="span12">
            <div class="box">
                <div class="title">
                    <h4>
                        <span class="icon16 brocco-icon-grid"></span><span>Thông tin sản phẩm</span>
                    </h4>
                </div>
                <div class="content clearfix">
                    <div class="alert alert-error<?php echo isset($this->error) && !empty($this->error) ? '' : ' hide'?>">
                        <button type="button" class="close" data-dismiss="alert">&times;</button>
                        <div><span id="edit-news-error"><?php echo isset($this->error) && !empty($this->error) ? $this->error : '';?></span></div>
                    </div>

                    <div class="form-row row-fluid">
                        <div class="span12">
                            <div class="row-fluid">
                                <label class="form-label text_left marginL5 span2">Thông tin</label>
                                <div class="span9" style="margin-top: 6px;">
                                    <?php if(isset($product['create_name']) && !empty($product['create_name'])):?>
                                        <label class="left marginR10">
                                            <b>Người tạo:</b> <?php echo isset($product['create_name']) ? $product['create_name'] : ''?>
                                        </label>
                                    <?php endif;?>

                                    <?php if(isset($product['status']) && !empty($product['status'])):?>
                                        <label class="left marginR10">
                                            <b>Trạng thái:</b>
                                            <?php echo isset($product['status']) && !empty($product['status']) ? 'Xuất bản' : 'Chờ duyệt'?>
                                        </label>
                                    <?php endif;?>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="form-row row-fluid">
                        <div class="span12">
                            <div class="row-fluid">
                                <label class="form-label span3">Tên sản phẩm</label>
                                <div class="span8">
                                    <input name="name" value="<?php echo isset($product['name']) ? $product['name'] : ''?>" />
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="form-row row-fluid<?php echo (isset($this->user['role_id']) && $this->user['role_id'] = 1 ? '' : ' hide')?>">
                        <div class="row-fluid span12">
                            <label class="form-label span3">Slug</label>
                            <div class="span8">
                                <input name="slug" value="<?php echo isset($product['slug']) ? $product['slug'] : ''?>" />
                            </div>
                        </div>
                    </div>

                    <hr class="line" />

                    <div class="form-row row-fluid">
                        <div class="span12">
                            <div class="row-fluid">
                                <label class="form-label text_left marginL5 span2">Mô tả ngắn</label>
                                <div class="span9">
                                    <script type="text/javascript" src="/ckeditor/ckeditor.js"></script>
                                    <textarea id="short-description" name="shortDescription"><?php echo isset($product['short_description']) ? stripcslashes($product['short_description']) : ''?></textarea>
                                    <script type="text/javascript">
                                        $(document).ready(function () {
                                            var editorName = 'short-description';
                                            CKEDITOR.config.height = 200;
                                            CKEDITOR.config.contentsCss = '/public/css/extendCKeditor.css';
                                            CKEDITOR.replace(editorName);
                                        });
                                    </script>
                                </div>
                            </div>
                        </div>
                    </div>

                    <hr class="line" />

                    <div class="form-row row-fluid">
                        <div class="span12">
                            <div class="row-fluid upload-image">
                                <label class="form-label text_left marginL5 span3">
                                    Ảnh đại diện
                                    <span class="help-block">(Ảnh vuông 400x400px)</span>
                                </label>
                                <div class="span4 left">
                                    <input type="file" class="custom-file-input nostyle input-image" name="image"/>
                                </div>
                                <div class="span3 left alert alert-error hide">
                                    <button type="button" class="close" data-dismiss="alert">&times;</button>
                                    <div><span class="upload-error"></span></div>
                                </div>
                                <div class="span8 marginT10">
                                    <span>
                                        <img class="avatar span6" src="<?php echo (isset($product['image']) ? $product['image'] : '""');?>" />
                                    </span>
                                    <br>
                                    <input name="productImageUrl" value="" class="marginT10" placeholder="Url ảnh thay thế"/>
                                </div>
                                <input type="hidden" name="imgSrc" class="img-src">
                            </div>
                        </div>
                    </div>

                    <hr class="line" />

                    <div class="form-row row-fluid">
                        <div class="span12">
                            <div class="row-fluid">
                                <label class="form-label span3">Loại sản phẩm</label>
                                <div class="span8">
                                    <?php if(isset($this->treeOption) && !empty($this->treeOption)):?>
                                        <select name="typeId" class="nostyle">
                                            <option value="0"> - Chọn loại sản phẩm - </option>
                                            <?php echo $this->treeOption?>
                                        </select>
                                    <?php endif;?>
                                </div>
                            </div>
                        </div>
                    </div>

                    <hr class="line" />

                    <div class="form-row row-fluid">
                        <div class="span12">
                            <div class="row-fluid">
                                <label class="form-label span3">Mã SP</label>
                                <div class="span8">
                                    <input name="code" value="<?php echo isset($product['code']) ? $product['code'] : ''?>" />
                                </div>
                            </div>
                        </div>
                    </div>

                    <hr class="line" />

                    <div class="form-row row-fluid">
                        <div class="span12">
                            <div class="row-fluid">
                                <label class="form-label span3">Giá</label>
                                <div class="span6">
                                    <input name="price" value="<?php echo isset($product['price']) ? $product['price'] : ''?>" />
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="form-row row-fluid">
                        <div class="span12">
                            <div class="row-fluid">
                                <label class="form-label span3">Giá cũ</label>
                                <div class="span6">
                                    <input name="old_price" value="<?php echo isset($product['old_price']) ? $product['old_price'] : ''?>" />
                                </div>
                            </div>
                        </div>
                    </div>

                    <hr class="line" />

                    <div class="form-row row-fluid">
                        <div class="span12">
                            <div class="row-fluid">
                                <label class="form-label span3">Chất liệu</label>
                                <div class="span8">
                                    <textarea name="material" style="height: 50px;"><?php echo isset($product['material']) ? $product['material'] : ''?></textarea>
                                </div>
                            </div>
                        </div>
                    </div>

                    <hr class="line" />

                    <div class="form-row row-fluid">
                        <div class="span12">
                            <div class="row-fluid">
                                <label class="form-label span3">Kích thước</label>
                                <div class="span8">
                                    <textarea name="size" style="height: 50px;"><?php echo isset($product['size']) ? $product['size'] : ''?></textarea>
                                </div>
                            </div>
                        </div>
                    </div>

                    <hr class="line" />

                    <div class="form-row row-fluid">
                        <div class="span12">
                            <div class="row-fluid">
                                <label class="form-label span3">Nguồn gốc</label>
                                <div class="span8">
                                    <textarea name="source" style="height: 50px;"><?php echo isset($product['source']) ? $product['source'] : ''?></textarea>
                                </div>
                            </div>
                        </div>
                    </div>

                    <hr class="line" />

                    <div class="form-row row-fluid">
                        <div class="span12">
                            <div class="row-fluid">
                                <label class="form-label span3">Bảo hành</label>
                                <div class="span8">
                                    <textarea id="support" name="support"><?php echo isset($product['support']) ? stripcslashes($product['support']) : ''?></textarea>
                                    <script type="text/javascript">
                                        $(document).ready(function () {
                                            var editorName = 'support';
                                            CKEDITOR.config.height = 200;
                                            CKEDITOR.config.contentsCss = '/public/css/extendCKeditor.css';
                                            CKEDITOR.replace(editorName);
                                        });
                                    </script>
                                </div>
                            </div>
                        </div>
                    </div>

                    <hr class="line" />

                    <!-- Tin liên quan -->
                    <div class="form-row row-fluid">
                        <div class="span12">
                            <div class="row-fluid">
                                <label class="form-label text_left marginL5 span2">Tìm bài liên quan</label>
                                <div class="span9 stack_arr">
                                    <div class="input_news data_arr">
                                        <ul></ul>
                                        <input class="text_input" value="" placeholder="Tìm tin liên quan...">
                                        <script type="text/javascript">
                                            <?php if (isset($this->newsRelation) && !empty($this->newsRelation)):?>
                                            var newsRelates =  <?php echo $this->newsRelation ;?> ;
                                            <?php else:?>
                                            var newsRelates = {};
                                            <?php endif;?>
                                            var titles = [], ids = [];
                                            if (newsRelates){
                                                var ulInput = '';
                                                for(i in newsRelates){
                                                    ulInput += '<li class="select2-search-choice" rel="' + newsRelates[i].news_id + '"><span>' + newsRelates[i].news_title + '</span><span class="icon12 entypo-icon-cancel" rel="' + newsRelates[i].news_id + '"></span></li>';
                                                    ids.push(newsRelates[i].news_id);
                                                };
                                                $('#newsRelation').val(ids.join(','));
                                                $('.input_news ul').html(ulInput);
                                            }
                                        </script>
                                        </input>
                                    </div>
                                    <input type="hidden" id="newsRelation" name="newsRelationId" value="<?php echo isset($this->news['news_relation']) ? $this->news['news_relation'] : ''?>" />
                                    <div id="show_news"></div>
                                </div>
                                <button id="btnFindNewsRelation" onclick="return false;">Tìm</button>
                            </div>
                        </div>
                    </div>

                    <!-- Sản phẩm liên quan -->
                    <div class="form-row row-fluid">
                        <div class="span12">
                            <div class="row-fluid">
                                <label class="form-label text_left marginL5 span2">Tìm sản phẩm liên quan</label>
                                <div class="span9 stack_arr">
                                    <div class="input_product data_arr">
                                        <ul></ul>
                                        <input class="text_input" value="" placeholder="Tìm sản phẩm liên quan...">
                                        <script type="text/javascript">
                                            <?php if (isset($this->productRelation) && !empty($this->productRelation)):?>
                                            var productRelates =  <?php echo $this->productRelation ;?> ;
                                            <?php else:?>
                                            var productRelates = {};
                                            <?php endif;?>
                                            var names = [], productIds = [];
                                            if (productRelates){
                                                var ulInput = '';
                                                for(i in productRelates){
                                                    ulInput += '<li class="select2-search-choice" rel="' + productRelates[i].id + '"><span>' + productRelates[i].name + '</span><span class="icon12 entypo-icon-cancel" rel="' + productRelates[i].id + '"></span></li>';
                                                    productIds.push(productRelates[i].id);
                                                };
                                                $('#productRelation').val(productIds.join(','));
                                                $('.input_product ul').html(ulInput);
                                            }
                                        </script>
                                        </input>
                                    </div>
                                    <input type="hidden" id="productRelation" name="productRelationId" value="<?php echo isset($this->news['product_relation']) ? $this->news['product_relation'] : ''?>" />
                                    <div id="show_product"></div>
                                </div>
                                <button id="btnFindProductRelation" onclick="return false;">Tìm</button>
                            </div>
                        </div>
                    </div>

                    <hr class="line" />

                    <div class="form-row row-fluid">
                        <div class="span12">
                            <div class="row-fluid">
                                <div class="span6">
                                    <label class="form-label span6">Trạng thái</label>
                                    <div class="span3">
                                        <select name="status">
                                            <option <?php echo isset($product['status']) && $product['status'] == 1 ? 'selected="selected"' : '';?> value="1">Xuất bản</option>
                                            <option <?php echo isset($product['status']) && $product['status'] == 0 ? 'selected="selected"' : '';?> value="0">Chờ duyệt</option>
                                        </select>
                                    </div>
                                </div>

                                <div class="row-fluid span6">
                                    <label class="form-label text_left marginL5 span4">Lịch xuất bản</label>
                                    <div class="span8">
                                        <input class="left datetimepicker" style="width: 120px;" name="createdAt" value="<?php echo isset($product['created_at']) ? date('Y-m-d H:i:s', intval($product['created_at'])) : date('Y-m-d H:i:s')?>"/>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <!-- Main content -->
                    <div class="form-row row-fluid">
                        <div class="span12">
                            <div class="row-fluid">
                                <label class="form-label text_left marginL5 span2">Mô tả chi tiết</label>
                                <div class="span9">
                                    <script type="text/javascript" src="/ckeditor/ckeditor.js"></script>
                                    <textarea id="description" name="description"><?php echo isset($product['description']) ? stripcslashes($product['description']) : ''?></textarea>
                                    <script type="text/javascript">
                                        $(document).ready(function () {
                                            var editorName = 'description';
                                            CKEDITOR.config.height = 200;
                                            CKEDITOR.config.contentsCss = '/public/css/extendCKeditor.css';
                                            CKEDITOR.replace(editorName);
                                        });
                                    </script>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="form-row row-fluid">
                        <div class="span12">
                            <div class="row-fluid">
                                <input type="hidden" name="action" value="" />
                                <input type="hidden" id="id" value="<?php echo isset($product['id']) ? $product['id'] : '0'?>" />
                                <?php if (isset($product['id'])): ?>
                                    <?php if (isset($this->user) && ($this->user['role_id'] <= 3 || $this->user['user_id'] == $product['user_id'])):?>
                                        <div class="btn btn-info">
                                            <button class="btn btn-info btn-large" onclick="product.action.value='update';">Cập nhật</button>
                                        </div>
                                    <?php endif;?>
                                    <a href="<?php echo isset($product['link_preview']) ? $product['link_preview'] : ''?>" class="btn btn-info btn-large" target="_blank">Xem trước</a>
                                    <?php if (isset($product['link_web']) && !empty($product['link_web']) && $product['status'] == 1): ?>
                                        <a href="<?php echo $product['link_web']?>" class="btn btn-info btn-large" target="_blank">Link Web</a>
                                    <?php endif;?>
                                <?php else: ?>
                                    <div class="btn btn-info">
                                        <button class="btn btn-info btn-large" onclick="product.action.value='add';">Thêm</button>
                                    </div>
                                <?php endif; ?>
                            </div>
                        </div>
                    </div>

                </div><!--content-->
            </div><!--box-->
        </div><!--cot trai-->
    </form>
</div>
