<div class="heading">
	<h3>Quản trị phân vùng</h3>
	<div class="resBtnSearch">
		<a href="#"><span class="icon16 brocco-icon-search"></span> </a>
	</div>

	<div class="search">
		<form id="searchform" action="#" />
		<input type="text" class="top-search" placeholder="Tìm kiếm ..." /> <input
			type="submit" class="search-btn" value="" />
		</form>
	</div><!-- End search -->
</div><!-- End .heading-->

<div class="row-fluid clearfix">
	<form name="routes" action="" method="post" enctype="multipart/form-data">
		<div class="span6" style="margin: 0;">
			<div class="box">
				<div class="title">
					<h4>
						<span class="icon16 brocco-icon-grid"></span><span>Phân vùng</span>
					</h4>
				</div><!-- title -->
				<div class="content clearfix">
					<?php if (isset($this->error)):?>
					<div class="alert alert-error">
						<button type="button" class="close" data-dismiss="alert">&times;</button>
						<?php echo $this->error;?>
					</div>
					<?php endif;?>
					<div class="form-row row-fluid">
						<div class="span12">
							<div class="row-fluid">
								<label class="form-label span3" for="normal">Biểu mẫu</label>
								<div class="span8">
									<input type="text" value="<?php echo isset($this->route['route_pattern']) ? $this->route['route_pattern'] : ''?>" name="route_pattern" id="route_pattern" />
								</div>
							</div>
						</div>
					</div>

					<hr class="line" />

					<div class="form-row row-fluid">
						<div class="span12">
							<div class="row-fluid">
								<input type="hidden" name="action" value="" />
								<?php if (isset($this->route['route_id'])) : ?>
								    <button class="btn btn-info btn-large" onclick="routes.action.value='update';">Lưu</button>
								<?php else:?>
								    <button class="btn btn-info btn-large" onclick="routes.action.value='add';">Thêm</button>
								<?php endif;?>
							</div>
						</div>
					</div>
				</div><!--content-->
			</div><!--box-->
		</div><!-- cot trai -->

		<div class="span6">
			<div class="box">
				<div class="title">
					<h4>
						<span class="icon16 cut-icon-comment"></span> <span>Danh sách route</span>
					</h4>
					<a href="#" class="minimize">Thu nhỏ</a>
				</div>
				<div class="content">
					<div class="form-row row-fluid" style="height: 302px; overflow:scroll;">
						<?php if(isset($this->routes)):?>
							<table class="responsive table">
								<thead>
									<tr>
                                        <th>Biểu mẫu</th>
									</tr>
								</thead>
								<?php foreach ($this->routes as $route):?>
								<tr>
                                    <td>
                                        <a href="/admin/edit-route/<?php echo $route['route_id']?>">
                                            <?php echo $route['route_pattern']?>
                                        </a>
                                    </td>
								</tr>
								<?php endforeach;?>
							</table>
						<?php endif;?>
					</div>
				</div><!--content-->
			</div><!--box-->
		</div><!--cot phai-->
	</form>
</div>
