<link type="text/css" rel="stylesheet" href="/public/css/extendCKeditor.css" />

<div class="row-fluid clearfix">
    <form name="news" action="" method="post" enctype="multipart/form-data">
        <div class="span12">
            <div class="box">
                <div class="title">
                    <h4>
                        <span class="icon16 brocco-icon-grid"></span><span>Thông tin sản phẩm</span>
                    </h4>
                </div>
                <div class="content clearfix">
                <div class="alert alert-error<?= isset($this->error) && !empty($this->error) ? '' : ' hide'?>">
                    <button type="button" class="close" data-dismiss="alert">&times;</button>
                    <div><span id="edit-news-error"><?= isset($this->error) && !empty($this->error) ? $this->error : '';?></span></div>
                </div>

                <div class="form-row row-fluid">
                    <div class="span12">
                        <div class="row-fluid">
                            <label class="form-label text_left marginL5 span2">Thông tin</label>
                            <div class="span9" style="margin-top: 6px;">
                                <?php if(isset($this->news['create_name']) && !empty($this->news['create_name'])):?>
                                    <label class="left marginR10">
                                        <b>Người tạo:</b> <?= isset($this->news['create_name']) ? $this->news['create_name'] : ''?>
                                    </label>
                                <?php endif;?>

                                <?php if(isset($this->news['status_name']) && !empty($this->news['status_name'])):?>
                                    <label class="left marginR10">
                                        <b>Trạng thái:</b>
                                        <?= isset($this->news['status_name']) ? $this->news['status_name'] : ''?>
                                    </label>
                                <?php endif;?>
                                <input type="hidden" name="newsStatus" value="<?= isset($this->news['news_status']) ? $this->news['news_status'] : '4';?>"/>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="well">
                    <a href="javascript:void(0)" onclick="open_seo(this)">SEO</a>
                    <div class="seo-wrap">
                        <div class="form-row row-fluid">
                            <div class="span12">
                                <div class="row-fluid">
                                    <div class="span2">
                                        <label class="form-label text_left marginL5">Tiêu đề SEO</label>
                                        <label class="form-label text_left marginL5 error count-label"><?= isset($this->news['news_seo_title']) ? mb_strlen($this->news['news_seo_title'], 'UTF-8') . '/170' : '0/170'?></label>
                                    </div>
                                    <div class="span9">
                                        <textarea name="newsSeoTitle" class="text count-content" style="height: 60px; width: 100%"><?= isset($this->news['news_seo_title']) ? $this->news['news_seo_title'] : ''?></textarea>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="form-row row-fluid">
                            <div class="span12">
                                <div class="row-fluid">
                                    <div class="span2">
                                        <label class="form-label text_left marginL5">Mô tả SEO</label>
                                        <label class="form-label text_left marginL5 error count-label"><?= isset($this->news['news_seo_description']) ? mb_strlen($this->news['news_seo_description'], 'UTF-8') . '/170' : '0/170'?></label>
                                    </div>
                                    <div class="span9">
                                        <textarea name="newsSeoDescription" class="text count-content" style="height: 60px; width: 100%"><?= isset($this->news['news_seo_description']) ? $this->news['news_seo_description'] : ''?></textarea>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="form-row row-fluid">
                            <div class="span12">
                                <div class="row-fluid">
                                    <div class="span2">
                                        <label class="form-label text_left marginL5">Từ khóa SEO</label>
                                        <label class="form-label text_left marginL5 error count-label"><?= isset($this->news['news_seo_keyword']) ? mb_strlen($this->news['news_seo_keyword'], 'UTF-8') . '/170' : '0/170'?></label>
                                    </div>
                                    <div class="span9">
                                        <textarea name="newsSeoKeyword" class="text count-content" style="height: 60px; width: 100%"><?= isset($this->news['news_seo_keyword']) ? $this->news['news_seo_keyword'] : ''?></textarea>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div><!--SEO wrap-->
                </div><!--END SEO-->

                <div class="form-row row-fluid">
                    <div class="span12">
                        <div class="row-fluid">
                            <label class="form-label text_left marginL5 span2">Title</label>
                            <div class="span9">
                                <input type="text" name="newsTitle" id="newsTitle" value="<?= isset($this->news['news_title']) ? str_replace('"', '\'', $this->news['news_title']) : ''?>" />
                            </div>
                        </div>
                    </div>
                </div>

                <div class="form-row row-fluid">
                    <div class="span12">
                        <div class="row-fluid">
                            <div class="span2">
                                <div><label class="form-label text_left marginL5">Sapo</label></div>
                                <label class="form-label text_left marginL5 error count-label" id="sapo-count"><?= isset($this->news['news_sapo']) ? mb_strlen($this->news['news_sapo'], 'UTF-8') . '/170' : '0/170'?></label>
                            </div>
                            <div class="span9">
                                <textarea name="newsSapo" style="height: 100px;" class="count-content"><?= isset($this->news['news_sapo']) ? $this->news['news_sapo'] : ''?></textarea>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="form-row row-fluid">
                    <div class="span12">
                        <div class="row-fluid span6">
                            <label class="form-label text_left marginL5 span4">Danh mục</label>
                            <div class="span6">
                                <?php if(isset($this->treeOption) && !empty($this->treeOption)):?>
                                    <select name="categoryId" id="catId" class="nostyle">
                                        <option value="0"> - Chọn chuyên mục - </option>
                                        <?= $this->treeOption?>
                                    </select>
                                <?php endif;?>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="form-row row-fluid<?= (isset($this->user['role_id']) && $this->user['role_id'] <= 2 ? '' : ' hide')?>">
                    <div class="span12">
                        <div class="row-fluid span6">
                            <label class="form-label text_left marginL5 span4">
                                Link redirect
                                <span class="help-block">(Dùng cho SEO)?</span>
                            </label>
                            <div class="span8">
                                <input type="checkbox" name="isOutside" class="ibutton cbChange" id="cbOutside" target="divNewsLink" />
                            </div>
                        </div>
                    </div>
                </div>

                <div class="form-row row-fluid <?= (isset($this->news['is_outside']) && $this->news['is_outside'] == 1 && isset($this->user['role_id']) && $this->user['role_id'] <= 2 ? '' : 'hide');?>" id="divNewsLink">
                    <div class="span12">
                        <div class="row-fluid">
                            <label class="form-label text_left marginL5 span2">News Link</label>
                            <div class="span9">
                                <input type="text" name="newsRedirectLink" value="<?= isset($this->news['news_redirect_link']) ? $this->news['news_redirect_link'] : ''?>" />
                            </div>
                        </div>
                    </div>
                </div>

                <div class="form-row row-fluid<?= (isset($this->user['role_id']) && $this->user['role_id'] <= 2 ? '' : ' hide')?>">
                    <div class="span12">
                        <div class="row-fluid">
                            <label class="form-label text_left marginL5 span2">News Slug</label>
                            <div class="span9">
                                <input type="text" name="newsSlug" value="<?= isset($this->news['news_slug']) ? $this->news['news_slug'] : ''?>" />
                            </div>
                        </div>
                    </div>
                </div>

                <div class="form-row row-fluid">
                    <div class="span12">
                        <div class="row-fluid span6 upload-image">
                            <label class="form-label text_left marginL5 span4">
                                Ảnh đại diện
                                <span class="help-block">(Ảnh hình chữ nhật ngang 420x252px)</span>
                            </label>
                            <div class="span5 left">
                                <input type="file" class="custom-file-input nostyle input-image" name="image"/>
                            </div>
                            <div class="span3 left alert alert-error hide">
                                <button type="button" class="close" data-dismiss="alert">&times;</button>
                                <div><span class="upload-error"></span></div>
                            </div>
                            <div class="span7 marginT10">
                                <span>
                                    <img class="avatar" src="<?= (isset($this->news['news_image']) ? $this->news['news_image'] : '""');?>"
                                        <?= isset($this->news['news_image']) && !empty($this->news['news_image']) ? 'style="width: 300px; height: 100%;"' : ''?> />
                                </span>
                                <br>
                                <input type="text" name="newsImageUrl" value="" class="marginT10" placeholder="Url ảnh thay thế"/>
                            </div>
                            <input type="hidden" name="imgSrc" class="img-src">
                        </div>
                    </div>
                </div>

                <!-- Tags -->
                <div class="form-row row-fluid">
                    <div class="span12">
                        <div class="row-fluid">
                            <label class="form-label text_left marginL5 span2">Tags</label>
                            <div class="span9">
                                <div class="controls">
                                    <textarea name="newsTag" rows="1" id="newsTag"><?= isset($this->news['news_tag']) ? $this->news['news_tag'] : ''?></textarea>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <!-- Tin liên quan -->
                <div class="form-row row-fluid">
                    <div class="span12">
                        <div class="row-fluid">
                            <label class="form-label text_left marginL5 span2">Tìm tin liên quan</label>
                            <div class="span9 stack_arr">
                                <div class="input_data data_arr">
                                    <ul></ul>
                                    <input type="text" class="text_input" value="" placeholder="Tìm tin liên quan...">
                                    <script type="text/javascript">
                                        <?php if (isset($this->newsRelation) && !empty($this->newsRelation)):?>
                                            var newsRelates =  <?= $this->newsRelation ;?> ;
                                        <?php else:?>
                                            var newsRelates = {};
                                        <?php endif;?>
                                        var titles = [], ids = [];
                                        if (newsRelates){
                                            var ulInput = '';
                                            for(i in newsRelates){
                                                ulInput += '<li class="select2-search-choice" rel="' + newsRelates[i].news_id + '"><span>' + newsRelates[i].news_title + '</span><span class="icon12 entypo-icon-cancel" rel="' + newsRelates[i].news_id + '"></span></li>';
                                                ids.push(newsRelates[i].news_id);
                                            };
                                            $('#newsRelation').val(ids.join(','));
                                            $('.input_data ul').html(ulInput);
                                        }
                                    </script>
                                    </input>
                                </div>
                                <input type="hidden" id="newsRelation" name="newsRelationId" value="<?= isset($this->news['news_relation']) ? $this->news['news_relation'] : ''?>" />
                                <div id="show_data"></div>
                            </div>
                            <button id="btnFindNewsRelation" onclick="return false;">Tìm</button>
                        </div>
                    </div>
                </div>

                <div class="form-row row-fluid">
                    <div class="span12">
                        <div class="row-fluid">
                            <label class="form-label text_left marginL5 span2"></label>
                            <div class="span9 stack_arr">
                                <button class="btn  btn-large" id="btnInsertImage" onclick="return insertImageInto();">Chèn ảnh vào bài viết</button>
                            </div>
                        </div>
                    </div>
                </div>
                <script type="text/javascript">
                    function insertImageInto(){
                        var opener = window.open('/upload-manager', '_blank');
                        return false;
                    }
                </script>

                <!-- Main content -->
                <div class="form-row row-fluid">
                    <div class="span12">
                        <div class="row-fluid">
                            <label class="form-label text_left marginL5 span2">Nội dung</label>
                            <div class="span9">
                                <script type="text/javascript" src="/ckeditor/ckeditor.js"></script>
                                <textarea id="news_content" name="newsContent" row="30"><?= isset($this->news['news_content']) ? preg_replace('#<script[^>]*>(\s*videoOptions[^<]+)</script>#is', '<div>\\1</div>', stripcslashes($this->news['news_content'])) : ''?></textarea>
                                <style type="text/css">
                                    .tooltips{position:absolute; top:5px; right:5px; background:#000; padding:5px; color:#fff}
                                </style>
                                <script type="text/javascript">
                                    $(document).ready(function () {
                                        var editorName = 'news_content';
                                        CKEDITOR.config.height = 500;
                                        CKEDITOR.config.contentsCss = '/public/css/extendCKeditor.css';
                                        CKEDITOR.replace(editorName);
                                        var editor = CKEDITOR.instances.news_content;
                                    });
                                </script>
                                <div class="loading" style="display: none;">
                                    <img src="public/images/loader.gif" />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <!-- Tác giả - nguồn -->
                <div class="form-row row-fluid">
                    <div class="span12">
                        <!-- Is video? -->
                        <div class="row-fluid span6">
                            <label class="form-label text_left marginL5 span4">Là video?</label>
                            <div class="span8">
                                <input type="checkbox" class="ibutton cbChange" onchange="toggleCheckbox(this)"
                                    <?= isset($this->news['is_video']) && $this->news['is_video'] == 1 ? 'checked="checked"' : '';?> />
                                <input type="hidden" class="result" name="isVideo" value="<?= isset($this->news['is_video']) ? $this->news['is_video'] : '0';?>"/>
                            </div>
                        </div>

                        <div class="row-fluid span6">
                            <label class="form-label text_left marginL5 span4">Lịch xuất bản</label>
                            <div class="span8">
                                <input type="text" class="left datetimepicker" style="width: 120px;" name="newsPublishDate" value="<?= isset($this->news['news_published_date']) ? date('Y-m-d H:i:s', intval($this->news['news_published_date'])) : date('Y-m-d H:i:s')?>"/>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="form-row row-fluid">
                    <div class="span12">
                        <div class="row-fluid span6">
                            <label class="form-label text_left marginL5 span4">Nguồn:</label>
                            <div class="noMar span8">
                                <input type="text" name="sourceName" value="<?= isset($this->news['source_name']) ? $this->news['source_name'] : ''?>" />
                            </div>
                        </div>
                    </div>
                </div>

                <div class="form-row row-fluid">
                    <div class="span12">
                        <div class="row-fluid">
                            <input type="hidden" name="action" value="" />
                            <input type="hidden" id="newsId" value="<?= isset($this->news['news_id']) ? $this->news['news_id'] : '0'?>" />
                            <?php if (isset($this->news['news_id'])): ?>
                                <?php if (isset($this->user) && ($this->user['role_id'] <= 3 || $this->user['user_id'] == $this->news['user_id'])):?>
                                    <div class="btn btn-info">
                                        <input type="submit" id="btnSaveNews" value="LƯU"/>
                                    </div>
                                <?php endif;?>
                                <a href="<?= isset($this->news['link_preview']) ? $this->news['link_preview'] : ''?>" class="btn btn-info btn-large" target="_blank">Xem trước</a>
                                <?php if (isset($this->news['link_web']) && !empty($this->news['link_web']) && $this->news['news_status'] == 1): ?>
                                    <a href="<?= $this->news['link_web']?>" class="btn btn-info btn-large" target="_blank">Link Web</a>
                                <?php endif;?>
                            <?php else:?>
                                <div class="btn btn-info">
                                    <input type="submit" class="btn btn-info btn-large" id="btnInsertNews" value="LƯU BÀI"/>
                                </div>
                            <?php endif;?>
                        </div>
                    </div>
                </div>

                </div><!--content-->
            </div><!--box-->
        </div><!--cot trai-->
    </form>
</div>