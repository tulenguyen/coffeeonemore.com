<div class="heading">
	<h3>Danh sách thành viên</h3>
	<div class="resBtnSearch">
		<a href="#"><span class="icon16 brocco-icon-search"></span>
		</a>
	</div>

	<div class="search">
		<form id="searchform" name="user" action="" method="post" enctype="multipart/form-data"/>
			<input type="text" class="top-search" placeholder="Tìm kiếm ..." value="<?php echo isset($this->filter['user_like']) ? $this->filter['user_like'] : ''?>" name="searchUser" />
			<input type="hidden" name="action" value="" />
			<button class="search-btn" onclick="user.action.value='filter'"></button>
		</form>
	</div><!-- End search -->
</div><!-- End .heading-->

<!-- Build page from here: -->
<div class="row-fluid">
	<div class="box gradient">
		<div class="title">
			<h4>
				<span>Danh sách thành viên</span>
			</h4>
		</div>
		
		<hr class="line" />

		<div class="clearfix marginB10">
			<!-- <a class="btn" href="#"><span class="icon16 minia-icon-trashcan"></span>Xóa thành viên</a> -->
			<a class="btn btn btn-success" href="/admin/edit-user">Tạo thành viên</a>
		</div>
		<div class="content noPad">
			<table class="responsive table table-bordered" id="checkAll">
				<thead>
					<tr>
						<th id="masterCh" class="ch">
							<input type="checkbox" name="checkbox" value="all" class="styled" />
						</th>
						<th>UserID</th>
						<th>Tên thành viên</th>
						<th>Chức danh</th>
						<th>Số điện thoại</th>
						<th>Địa chỉ</th>
						<th>Làm việc từ ngày</th>
					</tr>
				</thead>
				<?php if (isset($this->users)):?>
				<tbody>
					<?php foreach($this->users as $user):?>
					<tr>
						<td class="chChildren">
							<input type="checkbox" name="checkbox" value="1" class="styled" />
						</td>
						<td><?php echo isset($user['user_id']) ? $user['user_id'] : 0?></td>
						<td class="alignleft">
							<a href="/admin/edit-user/<?php echo isset($user['user_id']) ? $user['user_id'] : 0?>">
								<strong id="userId-<?php echo isset($user['user_id']) ? $user['user_id'] : 0?>"><?php echo isset($user['user_name']) ? $user['user_name'] : ''?></strong>
							</a>
							<p class="listing-lead">Full name: <?php echo isset($user['user_fullname']) ? $user['user_fullname'] : ''?></p>
							<div class="quk-edit">
								<form action="/admin/edit-user" method="post">
								<a href="/admin/edit-user/<?php echo isset($user['user_id']) ? $user['user_id'] : 0?>" class="btn btn-mini">Sửa</a>
								<?php if (isset($user['user_status']) && $user['user_status'] == 0):?>
								<span class="btn btn-mini btnUser" act="1">Active</span>
								<?php else:?>
                                <span class="btn btn-mini btnUser" act="0">Deactive</span>
                                <?php endif;?>
                                <input type="hidden" value="<?php echo isset($user['user_id']) ? $user['user_id'] : 0?>" class="userId">
                                </form>
							</div>
						</td>
						<td><?php echo isset($user['role_name']) ? $user['role_name'] : ''?></td>
						<td><?php echo isset($user['user_phone']) ? $user['user_phone'] : ''?></td>
						<td><?php echo isset($user['user_address']) ? $user['user_address'] : ''?></td>
						<td>
							<span><?php echo date('Y-m-d H:i:s', $user['user_created_date'])?></span>
							<div>
								<strong><?php echo isset($user['user_status']) && $user['user_status'] == 1 ? 'Active' : 'Deactive'?></strong>
							</div>
						</td>
					</tr>
					<?php endforeach;?>
				</tbody>
				<?php else:?>
					Chưa có user
				<?php endif;?>
			</table>
		</div><!-- End .content -->
		<div class="margin10 clearfix">
			<?php echo Util::paginate(100, $this->pageIndex, '/admin/list-user/', 'paging', '', $this->ext)?>
		</div>
	</div><!-- End .box -->
</div><!-- End .row-fluid -->
