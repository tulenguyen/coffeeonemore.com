<div class="heading">
	<h3>Quản trị nhóm quyền</h3>
	<div class="resBtnSearch">
		<a href="#"><span class="icon16 brocco-icon-search"></span> </a>
	</div>

	<div class="search">
		<form id="searchform" action="#" />
		<input type="text" class="top-search" placeholder="Tìm kiếm ..." /> <input
			type="submit" class="search-btn" value="" />
		</form>
	</div><!-- End search -->
</div><!-- End .heading-->

<div class="row-fluid clearfix">
	<form name="role" action="" method="post" enctype="multipart/form-data">
		<div class="span8" style="margin: 0;">
			<div class="box">
				<div class="title">
					<h4>
						<span class="icon16 brocco-icon-grid"></span><span>Phân vùng</span>
					</h4>
				</div>
				<!-- title -->
				<div class="content clearfix">
					<?php if (isset($this->error)):?>
					<div class="alert alert-error">
						<button type="button" class="close" data-dismiss="alert">&times;</button>
						<?php echo $this->error;?>
					</div>
					<?php endif;?>

					<div class="form-row row-fluid">
						<div class="span12">
							<div class="row-fluid">
								<label class="form-label span3" for="normal">Tên phần quyền</label>
								<div class="span8">
									<input type="text"
										value="<?php echo isset($this->role['role_name']) ? $this->role['role_name'] : ''?>"
										name="role_name" id="role_name"/>
								</div>
							</div>
						</div>
					</div>

					<div class="form-row row-fluid">
						<div class="span12">
							<div class="row-fluid">
								<label class="form-label span3" for="normal">Trạng thái</label>
								<div class="span8">
									<select name="role_status">
										<option value="0">Trạng thái</option>
										<option
										<?php echo isset($this->role['role_status']) && $this->role['role_status'] == 1 ? 'selected="selected"' : '';?>
											value="1">Kích hoạt</option>
										<option
										<?php echo isset($this->role['role_status']) && $this->role['role_status'] == 2 ? 'selected="selected"' : '';?>
											value="2">Không kích hoạt</option>
									</select>
								</div>
							</div>
						</div>
					</div>

					<div class="form-row row-fluid">
						<div class="span12">
							<div class="row-fluid">
								<label class="form-label span3" for="normal">Mô tả nhóm</label>
								<div class="span8">
									<textarea name="role_description"><?php echo isset($this->role['role_description']) ? $this->role['role_description'] : ''?></textarea>
								</div>
							</div>
						</div>
					</div>

					<hr class="line" />

					<div class="form-row row-fluid">
						<div class="span12">
							<div class="row-fluid">
								<input type="hidden" name="action" value="" />
								<?php if (isset($this->role['role_id'])) : ?>
								<button class="btn btn-info btn-large" onclick="GetListCheckboxes(); role.action.value='update';">Sửa quyền</button>
								<?php else:?>
								<button class="btn btn-info btn-large" onclick="GetListCheckboxes(); role.action.value='add';">Thêm quyền</button>
								<?php endif;?>
							</div>
						</div>
					</div>
				</div><!--content-->
			</div><!--box-->

			<div class="box">
				<div class="title">
					<h4>
						<span class="icon16 brocco-icon-grid"></span>
						<span>Danh sách nhóm quyền</span>
					</h4>
				</div>
				<div class="content clearfix">
					<?php if(isset($this->roles)):?>
					<ul>
						<?php foreach ($this->roles as $role):?>
						<li><a href="/admin/edit-role/<?php echo $role['role_id']?>"><?php echo $role['role_name']?>
						</a></li>
						<?php endforeach;?>
					</ul>
					<?php endif;?>
				</div><!--content-->
			</div><!--box-->
			
		</div><!--cot trai-->
		
		<div class="span4">
			<div class="box">
				<div class="title">
					<h4>
						<span class="icon16 cut-icon-comment"></span>
						<span>Phân vùng</span>
					</h4>
					<a href="#" class="minimize">Thu nhỏ</a>
				</div>
				<div class="content">
					<div class="form-row row-fluid" style="max-height: 450px; overflow:scroll;">
						<?php if (isset($this->routes[0])):?>
							<?php foreach($this->routes as $route):?>
							<div class="div8">
								<input type="checkbox" name="checkboxChild" value="<?php echo $route['route_id']?>"
									<?php echo isset($this->routeList) && strrpos(',' . $this->routeList . ',', ',' . $route['route_id'] . ',') !== false ? 'checked="checked"' : ''?> />
								<span><?php echo $route['route_pattern']?></span>
							</div>
							<?php endforeach;?>
							<input type="hidden" name="checkboxes" value="<?php echo isset($this->routeList) ? $this->routeList : ''?>">
						<?php endif;?>
					</div>
				</div><!--content-->
			</div><!--box-->
		</div><!--cot phai-->
	
	</form>
</div>
