<link href="public/css/extendCKeditor.css" type="text/css" rel="STYLESHEET"/>
<script src="public/js/jquery.tipsy.js" type="text/javascript"></script>
<script src="ckeditor/plugins/uploadmanager/upload.js?v=1" type="text/javascript"></script>
<div class="photo_wrapper">
    <div class="photo_left">
        <div class="button_upload">
            <form name="upload" action="/admin/upload-file" method="POST" id="form_upload" enctype="multipart/form-data" target="upload_image">
                Upload: 
                <input type="file" id="uploadfile" name="uploadfile" accept="image/*"/>
                <br/>
                <input type="text" id="image_title" name="image_title" class="titleimg" placeholder="Tiêu đề của ảnh"/>
                <br/><br/>
                <!-- <a href="javascript:void(0);" class="cssbtn-ok btnUpload">
                    <span>Tải lên</span>
                </a>
                <input type="submit" value="Upload file" > -->
                <button class="cssbtn-ok">Tải lên</button>
                <input type="hidden" name="action" value="upload" />
            </form>
            <iframe id="upload_image" name="upload_image" style="display:none"></iframe>
        </div>
        <div class="images_infor" style="display: none">
             <img src="" /><br/>
            <input type="text" id="image_title_edit" name="image_title_edit" class="titleimg" placeholder="Tiêu đề của ảnh"/>
            <br/><br/>
            <a href="javascript:void(0);" class="cssbtn-ok btnUpdate" onclick="UpdateEditImage()">
                <span>Cập nhật</span>
            </a>
            <a href="javascript:void(0);" class="cssbtn-cancel btnCancel" onclick="CancelEditImage()">
                <span>Hủy bỏ</span>
            </a>
        </div>
    </div>
    <?php
    $postdata = http_build_query(
    		array(
    				'action' => 'search'
    		)
    );
    
    $opts = array('http' =>
    		array(
    				'method'  => 'POST',
    				'header'  => 'Content-type: application/x-www-form-urlencoded',
    				'content' => $postdata
    		)
    );
    $context  = stream_context_create($opts);
    $content = file_get_contents('/admin/upload-file', false, $context);
    $data = json_decode($content);
    ?>
    <div class="photo_right">
        <div class="list_photo">
            <ul>
                <?php foreach ($data as $k => $item): ?>
                    <?php if ($k == 0) :?>
                        ?>
                        <li class="item first" rel="<?php echo $item->id ?>">
                            <div class="img_show">
                                <img src="<?php echo $item->url ?>"/>
                            </div>
                            <h3><?php echo $item->title ?></h3>

                            <div class="img_tools">
                                <a href="javascript:void(0)" ><input type="checkbox" /></a>
                                <a href="#" class="tools_select" onclick="SelectImage($(this).parents('li'))"
                                   title="Chọn"></a>
                                <a href="#" class="tools_edit" onclick="EditImage($(this).parents('li'))" title="Sửa"></a>
                                <a href="#" class="tools_delete" onclick="DeleteImage()" title="Xóa"></a>
                            </div>
                        </li>
                    <?php else : ?>
                        <li class="item" rel="<?php echo $item->id ?>">
                            <div class="img_show">
                                <img src="<?php echo $item->url ?>"/>
                            </div>
                            <h3><?php echo $item->title ?></h3>

                            <div class="img_tools">
                                <a href="javascript:void(0)" ><input type="checkbox" /></a>
                                <a href="#" class="tools_select" onclick="SelectImage($(this).parents('li'))"
                                   title="Chọn"></a>
                                <a href="#" class="tools_edit" onclick="EditImage($(this).parents('li'))" title="Sửa"></a>
                                <a href="#" class="tools_delete" onclick="DeleteImage()" title="Xóa"></a>
                            </div>
                        </li>
                    <?php endif; endforeach; ?>
            </ul>
        </div>
    </div>
</div>