<div class="heading">
	<h3>Quản trị chuyên mục</h3>
	<div class="resBtnSearch">
		<a href="#"><span class="icon16 brocco-icon-search"></span> </a>
	</div>

	<div class="search">
		<form id="searchform" action="#" />
		<input type="text" class="top-search" placeholder="Tìm kiếm ..." />
		<input type="submit" class="search-btn" value="" />
		</form>
	</div><!---- End search -->
</div><!-- End .heading-->

<div class="row-fluid clearfix">
	<form name="category" action="" method="post" enctype="multipart/form-data">
		<div class="span8" style="margin: 0;">
			<div class="box">
				<div class="title">
					<h4>
						<span class="icon16 brocco-icon-grid"></span><span>Chuyên mục</span>
					</h4>
				</div><!-- title -->
				
				<div class="content clearfix">
					<?php if (isset($this->error)):?>
					<div class="alert alert-error">
						<button type="button" class="close" data-dismiss="alert">&times;</button>
						<?php echo $this->error;?>
					</div>
					<?php endif;?>

                    <div class="well">
                        <a href="javascript:void(0)" onclick="open_seo(this)">SEO</a>
                        <div class="seo-wrap">
                            <div class="form-row row-fluid">
                                <div class="span12">
                                    <div class="row-fluid">
                                        <div class="span2">
                                            <label class="form-label" for="checkboxes">Title</label>
                                            <label class="form-label error count-label"><?php echo isset($this->category['category_title']) ? mb_strlen($this->category['category_title'], 'UTF-8') . '/170' : '0/170'?></label>
                                        </div>
                                        <div class="span8">
                                            <textarea name="categoryTitle" class="text count-content" style="height: 60px; width: 100%"><?php echo isset($this->category['category_title']) ? $this->category['category_title'] : ''?></textarea>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <hr class="line" />
                            <div class="form-row row-fluid">
                                <div class="span12">
                                    <div class="row-fluid">
                                        <div class="span2">
                                            <label class="form-label" for="checkboxes">Keyword</label>
                                            <label class="form-label error count-label"><?php echo isset($this->category['category_keyword']) ? mb_strlen($this->category['category_keyword'], 'UTF-8') . '/170' : '0/170'?></label>
                                        </div>
                                        <div class="span8">
                                            <textarea name="categoryKeyword" class="text count-content" style="height: 60px; width: 100%"><?php echo isset($this->category['category_keyword']) ? $this->category['category_keyword'] : ''?></textarea>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <hr class="line" />
                            <div class="form-row row-fluid">
                                <div class="span12">
                                    <div class="row-fluid">
                                        <div class="span2">
                                            <label class="form-label" for="checkboxes">Description</label>
                                            <label class="form-label error count-label"><?php echo isset($this->category['category_description']) ? mb_strlen($this->category['category_description'], 'UTF-8') . '/170' : '0/170'?></label>
                                        </div>
                                        <div class="span8">
                                            <textarea name="categoryDescription" class="text count-content" style="height: 60px; width: 100%"><?php echo isset($this->category['category_description']) ? $this->category['category_description'] : ''?></textarea>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <hr class="line" />
                            <div class="form-row row-fluid">
                                <div class="span12">
                                    <div class="row-fluid">
                                        <div class="span2">
                                            <label class="form-label" for="checkboxes">Link title</label>
                                            <label class="form-label error count-label"><?php echo isset($this->category['category_link_title']) ? mb_strlen($this->category['category_link_title'], 'UTF-8') . '/170' : '0/170'?></label>
                                        </div>
                                        <div class="span8">
                                            <textarea name="categoryLinkTitle" class="text count-content" style="height: 60px; width: 100%"><?php echo isset($this->category['category_link_title']) ? $this->category['category_link_title'] : ''?></textarea>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div><!--SEO wrap-->
                    </div><!--END SEO-->

					<div class="form-row row-fluid">
						<div class="span12">
							<div class="row-fluid">
								<label class="form-label span3" for="normal">Tên chuyên mục</label>
								<div class="span8">
									<input name="categoryName" value="<?php echo isset($this->category['category_name']) ? $this->category['category_name'] : ''?>" />
								</div>
							</div>
						</div>
					</div>

					<hr class="line" />
					
					<div class="form-row row-fluid">
						<div class="span12">
							<div class="row-fluid">
								<label class="form-label span3" for="normal">Ảnh đại diện</label>
								<div class="span8">
									<span style="width: 110px; height: 90px;">
										<img src="<?php echo (isset($this->category['category_image']) ? $this->category['category_image'] : '""');?>" style="width: 110px; height: 90px;" />
									</span>
									<input type="file" name="image" value="" />
								</div>
							</div>
						</div>
					</div>

                    <div class="form-row row-fluid<?php echo (isset($this->user['role_id']) && $this->user['role_id'] == 1 ? '' : ' hide')?>">
                        <div class="row-fluid span12">
                            <label class="form-label span3" for="checkboxes">
                                Link redirect
                                <span class="help-block">(Dùng cho SEO)?</span>
                            </label>
                            <div class="span8">
                                <input type="checkbox" name="isOutside" class="ibutton cbChange" id="cbOutside" target="divNewsLink"/>
                            </div>
                        </div>
                    </div>

                    <div id="divNewsLink" class="hide">
                        <div class="form-row row-fluid">
                            <div class="row-fluid span12">
                                <label class="form-label span3" for="normal">News Link</label>
                                <div class="span8">
                                    <input type="text" name="categoryRedirectLink" value="<?php echo isset($this->category['category_redirect_link']) ? $this->category['category_redirect_link'] : ''?>" />
                                </div>
                            </div>
                        </div>

                        <div class="form-row row-fluid">
                            <div class="row-fluid span12">
                                <label class="form-label span3" for="normal">Cat Slug</label>
                                <div class="span8">
                                    <input type="text" name="categorySlug" value="<?php echo isset($this->category['category_slug']) ? $this->category['category_slug'] : ''?>" />
                                </div>
                            </div>
                        </div>
                    </div>
					
					<hr class="line" />
					
					<div class="form-row row-fluid">
						<div class="span12">
							<div class="row-fluid">
								<label class="form-label span3" for="normal">Chuyên mục cha</label>
								<div class="span8">
									<select name="parentId" class="nostyle">
										<option value="" selected="selected">- Không có cat cha -</option>
										<?php echo $this->treeOption;?>
									</select>
								</div>
							</div>
						</div>
					</div>
					
					<hr class="line" />
					
					<div class="form-row row-fluid">
						<div class="span12">
							<div class="row-fluid">
								<label class="form-label span3" for="normal">Mẫu</label>
                                <div class="span8">
                                    <input name="categoryTemplate" value="<?php echo isset($this->category['category_template']) ? $this->category['category_template'] : ''?>" />
                                </div>
							</div>
						</div>
					</div>
					
					<hr class="line" />
					
					<div class="form-row row-fluid">
						<div class="span12">
							<div class="row-fluid">
								<label class="form-label span3" for="normal">Thứ tự trên menu</label>
                                <div class="span8">
                                    <input name="categoryHeaderOrder" value="<?php echo isset($this->category['category_header_order']) ? $this->category['category_header_order'] : ''?>" />
                                </div>
							</div>
						</div>
					</div>

                    <hr class="line" />

                    <div class="form-row row-fluid">
                        <div class="span12">
                            <div class="row-fluid">
                                <label class="form-label span3" for="normal">Thứ tự trên trang</label>
                                <div class="span8">
                                    <input name="categoryHomeOrder" value="<?php echo isset($this->category['category_home_order']) ? $this->category['category_home_order'] : ''?>" />
                                </div>
                            </div>
                        </div>
                    </div>
					
					<hr class="line" />
					
					<div class="form-row row-fluid">
						<div class="span12">
							<div class="row-fluid">
								<label class="form-label span3" for="normal">Thứ tự footer</label>
                                <div class="span8">
                                    <input name="categoryFooterOrder" value="<?php echo isset($this->category['category_footer_order']) ? $this->category['category_footer_order'] : ''?>" />
                                </div>
							</div>
						</div>
					</div>

                    <hr class="line" />

                    <div class="form-row row-fluid">
                        <div class="span12">
                            <div class="row-fluid">
                                <label class="form-label span3" for="normal">Thứ tự trong CMS</label>
                                <div class="span8">
                                    <input name="categoryCmsOrder" value="<?php echo isset($this->category['category_cms_order']) ? $this->category['category_cms_order'] : ''?>" />
                                </div>
                            </div>
                        </div>
                    </div>
					<hr class="line" />
					
					<div class="form-row row-fluid">
						<div class="span12">
							<div class="row-fluid">
								<label class="form-label span3" for="normal">Trạng thái</label>
								<div class="span8">
									<select name="categoryStatus">
										<option <?php echo isset($this->category['category_status']) && $this->category['category_status'] == 1 ? 'selected="selected"' : '';?> value="1">Hiện</option>
										<option <?php echo isset($this->category['category_status']) && $this->category['category_status'] == 0 ? 'selected="selected"' : '';?> value="0">Ẩn</option>
									</select>
								</div>
							</div>
						</div>
					</div>

                    <hr class="line" />

					<div class="form-row row-fluid">
						<div class="span12">
							<div class="row-fluid">
								<input type="hidden" name="action" value="" />
								<?php if (isset($this->category['category_id'])) : ?>
								<button class="btn btn-info btn-large" onclick="category.action.value='update';">Cập nhật</button>
								<?php else:?>
								<button class="btn btn-info btn-large" onclick="category.action.value='add';">Thêm chuyên mục</button>
								<?php endif;?>
							</div>
						</div>
					</div>
				</div><!--content-->
			</div><!--box-->
		</div><!-- cot trai -->
		
		<div class="span4">
            <div class="box">
				<div class="title">
					<h4>
						<span class="icon16 cut-icon-comment"></span>
						<span>Danh sách chuyên mục</span>
					</h4>
					<a href="#" class="minimize">Thu nh�?</a>
				</div>
				<div class="content">
					<div class="form-row row-fluid" style="height: 460px; overflow:scroll;">
						<?php echo $this->treeView?>
					</div>
				</div><!-- content -->
			</div><!-- box -->
		</div><!-- cot phai -->
	</form>
</div>
