	<!-- Le javascript ================================================== -->
    <script type="text/javascript" src="public/js/bootstrap/bootstrap.js"></script>
    <script type="text/javascript" src="public/js/jquery.cookie.js"></script>
    <script type="text/javascript" src="public/js/jquery.mousewheel.js"></script>

    <!-- Load plugins -->
    <script type="text/javascript" src="public/plugins/qtip/jquery.qtip.min.js"></script>
    <script type="text/javascript" src="public/plugins/knob/jquery.knob.js"></script>
    <script type="text/javascript" src="public/plugins/fullcalendar/fullcalendar.min.js"></script>
    <script type="text/javascript" src="public/plugins/prettify/prettify.js"></script>
    <script type="text/javascript" src="public/plugins/ibutton/jquery.ibutton.min.js"></script>
    <script type="text/javascript" src="public/plugins/uniform/jquery.uniform.min.js"></script>
    <script type="text/javascript" src="public/plugins/stepper/ui.stepper.js"></script>
    <script type="text/javascript" src="public/plugins/timeentry/jquery.timeentry.min.js"></script>
    <script type="text/javascript" src="public/plugins/select/select2.js"></script>
    <script type="text/javascript" src="public/plugins/dualselect/jquery.dualListBox-1.3.min.js"></script>
    <script type="text/javascript" src="public/plugins/validate/jquery.validate.min.js"></script>
    <script type="text/javascript" src="public/plugins/animated-progress-bar/jquery.progressbar.js"></script>
    <script type="text/javascript" src="public/plugins/lazy-load/jquery.lazyload.min.js"></script>
    <script type="text/javascript" src="public/plugins/jpages/jPages.min.js"></script>
    <script type="text/javascript" src="public/plugins/smartWizzard/jquery.smartWizard-2.0.min.js"></script>
    <script type="text/javascript" src="public/plugins/ios-fix/ios-orientationchange-fix.js"></script>
    <!-- Important Place before main.js  -->

    <script type="text/javascript" src="public/js/jquery-ui.min.js"></script>
    <script type="text/javascript" src="public/plugins/touch-punch/jquery.ui.touch-punch.min.js"></script>
    <script type="text/javascript" src="public/js/main.js"></script>
	<script type="text/javascript" src="public/js/common.js?v=1"></script>
    </body>
</html>