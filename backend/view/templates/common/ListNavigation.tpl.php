<div class="centerContent">
    <ul class="bigBtnIcon">
        <li>
            <a href="/admin/list-news-pending" title="Bài do BTV gửi chưa được duyệt" class="tipB">
                <span class="txt">Duyệt bài</span>
            </a>
        </li>
        <li>
            <a href="/admin/edit-news">
                <span class="txt">Viết bài mới</span>
            </a>
        </li>
        <li>
            <a href="/admin/list-news-publish" title="Bài đã được đăng" class="pattern tipB">
                <span class="txt">Bài đã duyệt</span>
            </a>
        </li>
        <li>
            <a href="/admin/list-news-timer" title="Bài hẹn giờ" class="tipB">
                <span class="txt">Bài hẹn giờ</span>
            </a>
        </li>
        <li>
            <a href="/admin/set-position" title="Vị trí nổi bật" class="tipB">
                <span class="txt">Vị trí nổi bật</span>
            </a>
        </li>
    </ul>
</div>
<div class="clearfix"></div>