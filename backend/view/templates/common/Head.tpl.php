<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns:fb="http://ogp.me/ns/fb#" itemscope="itemscope" itemtype="http://schema.org/WebPage">
<head>
    <base href="<?php echo Context::getInstance()->getFront()->getRequest()->getDomain();?>/" />
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <link rel="Shortcut Icon" href="public/images/favicon.png" type="image/x-icon" />
    <meta name="robots" content="noodp,index,follow" />
    <meta name="google-site-verification" content="" />
    <meta name="AUTHOR" content="Tule Nguyễn" />
    <meta name="COPYRIGHT" content="Tule Nguyễn" />
    <meta http-equiv="Cache-control" content="Public" />
    <meta http-equiv="Expires" content="300" />
    <meta http-equiv="X-UA-Compatible" content="IE=9; IE=8;IE=EmulateIE8; IE=EmulateIE7" />
    <meta name="google-site-verification" content="iUWZDjdetBk77BkTsTClpYteJ3-YYwLCtsdYTXE8x_I" />
    <title><?php echo Context::getInstance()->getFront()->getLayout()->getPageTitle();?></title>
    <meta name="description" content="<?php echo Context::getInstance()->getFront()->getLayout()->getPageDescription();?>" />
    <meta name="keywords" content="<?php echo Context::getInstance()->getFront()->getLayout()->getPageKeywords()?>" />
    <meta name="new_keywords" content="<?php echo Context::getInstance()->getFront()->getLayout()->getPageKeywords()?>" />

    <link href="public/css/bootstrap/bootstrap.css" rel="stylesheet" type="text/css" />
    <link href="public/css/bootstrap/bootstrap-responsive.min.css" rel="stylesheet" type="text/css" />
    <link href="public/css/supr-theme/jquery.ui.supr.css" rel="stylesheet" type="text/css" />
    <link href="public/css/icons.css" rel="stylesheet" type="text/css" />

    <link href="public/plugins/qtip/jquery.qtip.css" rel="stylesheet" type="text/css" />
    <link href="public/plugins/fullcalendar/fullcalendar.css" rel="stylesheet" type="text/css" />
    <link href="public/plugins/jpages/jPages.css" rel="stylesheet" type="text/css" />
    <link href="public/plugins/prettify/prettify.css" type="text/css" rel="stylesheet" />
    <link href="public/plugins/inputlimiter/jquery.inputlimiter.css" type="text/css" rel="stylesheet" />
    <link href="public/plugins/ibutton/jquery.ibutton.css" type="text/css" rel="stylesheet" />
    <link href="public/plugins/uniform/uniform.default.css" type="text/css" rel="stylesheet" />
    <link href="public/plugins/color-picker/color-picker.css" type="text/css" rel="stylesheet" />
    <link href="public/plugins/select/select2.css" type="text/css" rel="stylesheet" />
    <link href="public/plugins/validate/validate.css" type="text/css" rel="stylesheet" />
    <link href="public/plugins/pnotify/jquery.pnotify.default.css" type="text/css" rel="stylesheet" />
    <link href="public/plugins/pretty-photo/prettyPhoto.css" type="text/css" rel="stylesheet" />
    <link href="public/plugins/smartWizzard/smart_wizard.css" type="text/css" rel="stylesheet" />
    <link href="public/plugins/dataTables/jquery.dataTables.css" type="text/css" rel="stylesheet" />
    <link href="public/plugins/elfinder/elfinder.css" type="text/css" rel="stylesheet" />
    <link href="public/plugins/plupload/jquery.ui.plupload/css/jquery.ui.plupload.css" type="text/css" rel="stylesheet" />

    <link href="public/css/main.css?v=1" rel="stylesheet" type="text/css" />

    <script type="text/javascript" src="public/js/jquery.js"></script>

    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

    <script type="text/javascript">
        //adding load class to body and hide page
        document.documentElement.className += 'loadstate';
    </script>
</head>
<body>