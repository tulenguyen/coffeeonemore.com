<!--Responsive navigation button-->  
<div class="resBtn">
    <a href="#"><span class="icon16 icomoon-icon-arrow-right white-3 black"></span></a>
</div>

<!--Sidebar collapse button-->
<div class="collapseBtn">
    <a href="#" class="tipR" title="Ẩn menu"><span class="icon12 minia-icon-layout"></span></a>
</div>

<!--Sidebar background-->
<div id="sidebarbg"></div>
<!--Sidebar content-->
<div id="sidebar">
    <div class="sidenav">
        <div class="sidebar-widget" style="margin: -1px 0 0 0;">
            <h5 class="title" style="margin-bottom:0"><?php echo isset($this->user['role_name']) ? $this->user['role_name'] : '' ?></h5>
        </div><!-- End .sidenav-widget -->

        <div class="mainnav">
            <input type="hidden" id="listRoute" value="<?php echo isset($this->listRoute) ? $this->listRoute : '' ?>" />
            <ul>
                <li>
                    <a href="#"><span class="icon16 icomoon-icon-arrow-right white-3 black"></span>Quản lý tin</a>
                    <ul class="sub">
                        <li><a href="/admin/edit-news"><span class="icon16 icomoon-icon-arrow-right white-3 black"></span>Viết bài mới</a></li>
                        <li><a href="/admin/list-news-unapproved"><span class="icon16 icomoon-icon-arrow-right white-3 black"></span>Kho chờ duyệt</a></li>
                        <li><a href="/admin/list-news-pending"><span class="icon16 icomoon-icon-arrow-right white-3 black"></span>Bài chờ xuất bản</a></li>
                        <li><a href="/admin/list-news-publish"><span class="icon16 icomoon-icon-arrow-right white-3 black"></span>Kho dữ liệu</a></li>
                        <li><a href="/admin/list-news-store"><span class="icon16 icomoon-icon-arrow-right white"></span>Lưu trữ cá nhân <span></span></a></li>
                        <li><a href="/admin/list-news-delete"><span class="icon16 icomoon-icon-arrow-right white-3 black"></span>Bài tạm xóa</a></li>
                    </ul>
                </li>
                
                 <li>
                    <a href="#"><span class="icon16 icomoon-icon-arrow-right white-3 black"></span>Quản lý Sản phẩm</a>
                    <ul class="sub">
                        <li><a href="/admin/edit-product"><span class="icon16 icomoon-icon-arrow-right white-3 black"></span>Tạo sản phẩm mới</a></li>
                        <li><a href="/admin/list-product-pending"><span class="icon16 icomoon-icon-arrow-right white-3 black"></span>Kho chờ duyệt</a></li>
                        <li><a href="/admin/list-product-publish"><span class="icon16 icomoon-icon-arrow-right white-3 black"></span>Kho xuất bản</a></li>
                    </ul>
                </li>
              
                
                <li>
                    <a href="#"><span class="icon16 icomoon-icon-arrow-right white-3 black"></span>Quản lý Chuyên mục</a>
                    <ul class="sub">
                        <li><a href="/admin/edit-product-type"><span class="icon16 icomoon-icon-arrow-right white-3 black"></span>Chuyên mục Sản phẩm</a></li>
                        <li><a href="/admin/edit-category"><span class="icon16 icomoon-icon-arrow-right white-3 black"></span>Chuyên mục Tin Tức</a></li>
                    </ul>
                </li>
                <li>
                    <a href="#"><span class="icon16 icomoon-icon-arrow-right white-3 black"></span>Quản lý thành viên</a>
                    <ul class="sub">
                        <li><a href="/admin/edit-user"><span class="icon16 icomoon-icon-arrow-right white-3 black"></span>Tạo thành viên</a></li>
                        <li><a href="/admin/list-user"><span class="icon16 icomoon-icon-arrow-right white-3 black"></span><u>Danh sách thành viên</u></a></li>
                    </ul>
                </li>
                <?php if (isset($this->user['role_id']) && $this->user['role_id'] == 1): ?>
                    <li>
                        <a href="#"><span class="icon16 icomoon-icon-arrow-right white-3 black"></span>Quản lý phân quyền</a>
                        <ul class="sub">
                            <li><a href="/admin/edit-role"><span class="icon16 icomoon-icon-arrow-right white-3 black"></span>Quản lý role</a></li>
                            <li><a href="/admin/edit-route"><span class="icon16 icomoon-icon-arrow-right white-3 black"></span>Quản lý route</a></li>
                        </ul>
                    </li>

                <?php endif; ?>

                <li>
                    <a href="#"><span class="icon16 icomoon-icon-arrow-right white-3 white"></span>Vị trí, Dòng sự kiện</a>
                    <ul class="sub">
                        <li><a href="/admin/set-event"><span class="icon16 icomoon-icon-arrow-right white"></span>Quản lý dòng sự kiện</a></li>
                        <li><a href="/admin/edit-event"><span class="icon16 icomoon-icon-arrow-right white"></span>Danh sách dòng sự kiện</a></li>
                    </ul>
                </li>
            </ul>
        </div>
    </div><!-- End sidenav -->
</div><!-- End #sidebar -->