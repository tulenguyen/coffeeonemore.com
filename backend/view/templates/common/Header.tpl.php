<div id="header">
	<div class="navbar">
		<div class="navbar-inner">
			<div class="container-fluid">
				<?php if (isset($this->user)): ?>
				<div class="nav-no-collapse">
					<ul class="nav">
                        <li>
                            <a href="/admin/crawler-news" class="tipB" title="Crawler tin">
                                <strong>Crawler tin</strong>
                            </a>
                        </li>
                        <li>
                            <a href="/admin/edit-news" class="tipB" title="Viết bài mới">
                                <strong>Viết bài mới</strong>
                            </a>
                        </li>
                        <?php if (isset($this->user['role_id']) && $this->user['role_id'] <= 3):?>
                        <li>
                            <a href="/admin/list-user" class="tipB" title="Danh sách thành viên">
                                <strong>Danh sách thành viên</strong>
                            </a>
                        </li>
                        <li>
                            <a href="/admin/edit-user" class="tipB" title="Thêm thành viên">
                                <strong>Thêm thành viên</strong>
                            </a>
                        </li>
                        <?php endif;?>
					</ul>
					<ul class="nav pull-right usernav">
						<li>
							<a href="/admin/edit-profile" class="dropdown-toggle avatar tipB" title="Tài khoản" data-toggle="dropdown">
								<img src="<?php echo isset($this->user['user_avatar']) && !empty($this->user['user_avatar']) ? $this->user['user_avatar'] : '/public/images/noavatar.png'?>" alt="" class="image" />
								<span class="txt"><?php echo isset($this->user['user_fullname']) ? $this->user['user_fullname'] : ''?></span>
								<b class="caret"></b>
							</a>
						</li>
						<li>
							<a href="/thoat" class="tipB" title="Đăng xuất"><span class="icon16 icomoon-icon-exit"></span>Đăng xuất</a>
						</li>
					</ul>
				</div><!-- /.nav-collapse -->
				<?php endif;?>
			</div>
		</div><!-- /navbar-inner -->
	</div><!-- /navbar -->
</div><!-- End #header -->
