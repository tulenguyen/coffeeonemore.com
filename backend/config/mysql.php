<?php

return [
    'local' => [
        'dbhost'        => 'localhost',
        'dbuser'        => 'coffeeonemore_u',
        'dbpassword'    => '0o9i8u7y6t',
        'dbname'        => 'coffeeonemore_db',
        'dbport'        => '3306'
    ],
    'server' => [
        'dbhost'        => '127.0.0.1',
        'dbuser'        => 'coffeeonemore_u',
        'dbpassword'    => '0o9i8u7y6t',
        'dbname'        => 'coffeeonemore_db',
        'dbport'        => '3306'
    ]
];
