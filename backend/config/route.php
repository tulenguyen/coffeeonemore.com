<?php
return array('/'																		=> 'Login',
             'ajax' 																	=> 'Ajax',
		     'captcha' 																	=> 'Captcha',
			 'default' 																	=> 'NotFound',
			 'dang-nhap'																=> 'Login',
			 'dang-ky'																	=> 'Register',
			 'thoat'																	=> 'Logout',
             'upload-manager'															=> 'UploadManager',
			 'admin(?:/<module:[\w\d-]+>)?(?:/<id:\d+>)?'								=> 'Admin',
			 'admin(?:/<module:[\w\d-]+>)?(?:/page-<page:\d+>)?'						=> 'Admin',
             '<slug:[\w\d-]+>-<id:\d+>.html'		 			                        => 'News'
		);