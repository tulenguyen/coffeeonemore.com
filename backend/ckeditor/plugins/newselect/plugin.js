/**
 * Basic sample plugin inserting newselecteviation elements into CKEditor editing area.
 *
 * Created out of the CKEditor Plugin SDK:
 * http://docs.ckeditor.com/#!/guide/plugin_sdk_sample_1
 */

// Register the plugin within the editor.
CKEDITOR.plugins.add( 'newselect', {

	// Register the icons.
	icons: 'newselect',

	// The plugin initialization logic goes inside this method.
	init: function( editor ) {

		// Define an editor command that opens our dialog.
		editor.addCommand( 'newselect', new CKEDITOR.dialogCommand( 'newselectDialog' ) );

		// Create a toolbar button that executes the above command.
		editor.ui.addButton( 'newselect', {
			// The text part of the button (if available) and tooptip.
			label: 'Chọn tin',
			// The command to execute on click.
			command: 'newselect',
			// The button placement in the toolbar (toolbar group name).
			toolbar: 'insert'
		});

		// Register our dialog file. this.path is the plugin folder path.
		CKEDITOR.dialog.add( 'newselectDialog', this.path + 'dialogs/newselect.js' );
	}
});

