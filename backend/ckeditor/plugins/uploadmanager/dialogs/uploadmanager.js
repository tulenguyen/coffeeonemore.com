/**
 * The uploadmanager dialog definition.
 *
 * Created out of the CKEditor Plugin SDK:
 * http://docs.ckeditor.com/#!/guide/plugin_sdk_sample_1
 */

CKEDITOR.dialog.add('uploadmanagerDialog', function (editor) {
    return {
        title: 'Quản lý file',
        minWidth: 900,
        minHeight: 500,
        contents: [
            {
                id: 'tab-pic',
                label: 'Quản lý ảnh',
                elements: [
                    {
                        type: 'html',
                        html: '<div class="imagemanager"></div>'
                    }
                ]
            },
            {
                id: 'tab-video',
                label: 'Quản lý Video',
                elements: [
                    {
                        type: 'html',
                        html: '<div class="videomanager"></div>'
                    }
                ]
            }
        ],
        onLoad: function () {
            $('.imagemanager').load('ckeditor/plugins/uploadmanager/html/photo.html', function () {
               
            });

            $('.videomanager').load('ckeditor/plugins/uploadmanager/html/video.html', function () {
            });
        },
        onShow: function () {
            $('input[type=checkbox]').attr('checked', false);
            $('.list_photo').find('.photo_active').removeClass('photo_active');
        },
        onOk: function () {
            if (this._.currentTabId == 'tab-pic') {
            	var html = '';
                $('.imagemanager input[type=checkbox]').each(function () {
                    if ($(this).is(':checked')) {
                        var obj = $(this).parents('li');
                        var imgurl = obj.find('img:first').attr('src');
                        var title = obj.find('h3:first').text();
                        html = '<p style="text-align:center" class="photo"><img src="' + imgurl + '" alt="' + title + '" title="' + title + '" /></p>' + html;
                    }
                });
                editor.insertHtml(html);
            } else {
            	var html = '';
                $('.videomanager input[type=checkbox]').each(function () {
                    if ($(this).is(':checked')) {
                        html = $(this).parents('li').find('textarea').val()+'<br/>' + html;
                    }
                });
                editor.insertHtml(html);
            }
        }
    };
});
