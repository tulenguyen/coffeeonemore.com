<?php

class Front  extends Controller{

	public function init()
	{
		$layoutPath	=  'view/layouts/';
		$uri  		=  $this->getRequest()->getUrl();
		$layoutName =  Context::getInstance()->getRoute()->parseUrl($uri);
		$this->registerLayout($layoutName. 'Layout', $layoutPath);
		$output 	= $this->_layout->load();
		//$output		= preg_replace('#(?:\s|&nbsp;)+#is', ' ',  $output); 
		
		$this->getResponse()->setBody($output);
		$this->getResponse()->sendResponse();
		
		//$keyCache =  Context::getInstance()->getKeyCache($uri); 
		//Helper::getInstance()->getMemcachedConnection()->set($keyCache,  $output);
	}
	
}
?>
