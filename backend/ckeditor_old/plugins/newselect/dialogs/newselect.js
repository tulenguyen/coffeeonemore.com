/**
 * The newselect dialog definition.
 *
 * Created out of the CKEditor Plugin SDK:
 * http://docs.ckeditor.com/#!/guide/plugin_sdk_sample_1
 */

CKEDITOR.dialog.add('newselectDialog', function (editor) {
    return {
        title: 'Chọn tin liên quan - nên đọc',
        minWidth: 900,
        minHeight: 500,
        contents: [
            {
                id: 'tinnendoc2',
                label: 'Tin nên đọc  ngang',
                elements: [
                    {
                        type: 'html',
                        html: '<div id="tab3"></div>'
                    }
                ]
            },
            {
                id: 'tinnendoc1',
                label: 'Tin nên đọc  dọc',
                elements: [
                    {
                        type: 'html',
                        html: '<div id="tab2"></div>'
                    }
                ]
            },
            {
                id: 'tinlienquan',
                label: 'Tin liên quan',
                elements: [
                    {
                        type: 'html',
                        html: '<div id="tab1"></div>'
                    }
                ]
            }
        ],
        onLoad: function () {
            $('#tab3').load('ckeditor/plugins/newselect/html/news_suggest_2.html');
            $('#tab2').load('ckeditor/plugins/newselect/html/news_suggest_1.html');
        	$('#tab1').load('ckeditor/plugins/newselect/html/news_relation.html');
        },
        onShow: function () {
        },
        onOk: function () {
            if (this._.currentTabId == 'tinlienquan') {
                if ($('.box_news_relation ul').find('li').length > 0) {
                    editor.insertHtml($('.box_news_relation:eq(0)').parent().html() + '<div class="clear"></div>');
                }
            } else if (this._.currentTabId == 'tinnendoc1') {
            	if ($('.box_news_suggest1 ul').find('li').length > 0) {
                    editor.insertHtml($('.box_news_suggest1:eq(0)').parent().html());
                }
            } else if (this._.currentTabId == 'tinnendoc2'){
            	if ($('.box_news_suggest2 ul').find('li').length > 0) {
                    editor.insertHtml($('.box_news_suggest2:eq(0)').parent().html() + '<div class="clear"></div>');
                }
            }
        }
    };
});