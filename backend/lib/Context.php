<?php

class Context{

	protected static $_instance;
	protected $_front; // controler
	protected $_basePath;
	protected $_route;
	protected $_cachedParams;
	protected $_session;
	
	public static function getInstance(){
		if (false === isset(self::$_instance)){
			self::$_instance = new self();
		}
		return self::$_instance;
	}
	
	public function getFront(){
		return $this->_front;
	}
	
	public function getRoute(){
		return $this->_route;
	}
	
	public function setBasePath($basePath){
		$this->_basePath = $basePath;
		return $this;
	}
	
	public function getSession(){
		if (!isset($this->_session)){
			$this->_session =  new Session();
			$this->_session->init();
		}
		return $this->_session;
	}
	
	public function getBasePath(){
		return $this->_basePath;
	}
	
	public function addCachedParam($key, $val)
	{
		$this->_cachedParams[$key] = $val;
	}
	
	public function getCachedParam($key)
	{
		if (isset($this->_cachedParams[$key])){
			return $this->_cachedParams[$key];
		}
		return null;
	}
	
	public function removeCachedParam($key = null)
	{
		if ($key !== null){
			unset($this->_cachedParams[$key]);
		}else{
			unset($this->_cachedParams);
		}
	}
	
	public function haveCache($uri)
	{
		return false; stripos($uri, '.nocache');
	}
	
	public function getKeyCache($uri)
	{
		return preg_replace('#\.\w+|[^\w\d]+#is', '', $uri);
	}
	
	public function load($paths)
	{
		if (is_array($paths)){
			foreach ($paths as $path){
				include $this->_basePath. $path;				
			}
		}else
			include $this->_basePath. $paths;
	}
	
	public function setup($mode = true)
	{
		error_reporting(E_ALL);
        ini_set('error_log', $this->_basePath.'/log/error.'.date('Y-m-d').'.log');
        ini_set('log_errors', $mode);
        ini_set('track_errors', $mode);
        ini_set('display_errors', $mode);
        ini_set('display_startup_errors', $mode);
        return $this;
	}
	
	public function start()
	{	
		$this->load( array('lib/Helper.php',
						   'lib/cache/MemCache.php',
						   'lib/Request.php') );
		
		$request 	=  new Request();
		$url 		=  $request->getFullUrl();
		
		// cache all page
		if ($this->haveCache($url)){
			$keyCache  =  $this->getKeyCache($url);	
			if (false !== ($output = Helper::getInstance()->getMemcachedConnection()->get($keyCache))){
				echo $output; return;
			}
		}
		
		$this->load( array( 'lib/database/Mysql.php',
							'lib/cache/MemCached.php',
							'lib/Response.php',
							'lib/Session.php',
							'lib/Controller.php',
							'lib/Auth.php',
							'lib/Acl.php',
							'lib/View.php',
							'lib/Layout.php',
							'lib/Transfer.php',
							'lib/Route.php',
							'lib/Util.php',
							'controls/Front.php',
							'models/Model.php') );
		
		$this->_route = new Route();
		$this->_front = new Front($request, new Response(), $this->_basePath);   unset($request, $cName);
		$this->_front->init();
				
	}
	
	public function execute()
	{
		$this->start();
	}

}

?>
